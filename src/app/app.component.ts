import { Component,Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import {AuthenticationService} from './shared/services/authentication.service'
import { CustomerService } from './shared/services/customer.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wm-oaf';
  username: any;
  val: any;
  @Output() sessionOut: EventEmitter<any> = new EventEmitter();
  constructor(
   public router: Router,
   private auth : AuthenticationService,
   private customerservice: CustomerService
    ){
      
   
  }

  isHomeRoute() {
    return this.router.url === '/';
  }

  ngOnInit() {
   if(localStorage.getItem('ACCESS_TOKEN')){
const atoken = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
this.val = atoken.AccessToken;
this.username = atoken.UserAttributes.username;
if(this.username == 'undefined' || this.username == ''){
  console.log('dd')
  localStorage.clear();
      localStorage.removeItem('timer');
      localStorage.removeItem('ACCESS_TOKEN');
      localStorage.removeItem('Attribute');
      localStorage.removeItem('Token');
}
}
 setInterval(() => {
    this.autologout(); 
    
  }, 10000);
  
}
  
  ngOnDestroy() {
 
}
  autologout(){
    const timer = JSON.parse(localStorage.getItem('timer'));
    
   
   
    if ((Date.now() > timer) && timer && this.val) {
      let p = {
      'username': this.username
    }
      localStorage.clear();
      localStorage.removeItem('timer');
      localStorage.removeItem('ACCESS_TOKEN');
      localStorage.removeItem('Attribute');
      localStorage.removeItem('Token');

      Swal.fire({
        icon: 'info',
        title: 'Oops...',
        text: 'Your session has expired, please log in again to continue',
        footer: ''
      })

      this.auth.logout(p).subscribe(res =>{
      this.router.navigate(['/']);
      })

    }
  }

}
