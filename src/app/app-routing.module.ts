import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './home/login/login.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { AuthGuard } from './shared/guard/guard.module';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component'
import { AddCustomerComponent } from './pages/customer/add-customer/add-customer.component';
import { ManageUserComponent } from './pages/manage-user/manage-user.component';


const routes: Routes = [

/*{
  path: '',
  component: LoginComponent,
  children: [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  ]
},*/
/*{
  path: 'login',
  component: LoginComponent,
},
{
    path : 'dashboard',
    component: DashboardComponent,
  canActivate: [AuthGuard]
},
{
  path : 'edit-profile',
  component: EditProfileComponent,
  canActivate: [AuthGuard]
},
{
  path : 'add-customer',
  component: AddCustomerComponent,
  canActivate: [AuthGuard]
},
{
  path : 'manage-user',
  component: ManageUserComponent,
  canActivate: [AuthGuard]
  
},*/

{ path: '', loadChildren: () => import('./oaf/oaf.module').then(m => m.OafModule) },

{path: '**', redirectTo: '/login'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }