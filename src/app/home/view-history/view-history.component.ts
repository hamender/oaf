import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Output, Input, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { ProvisionService } from '../../shared/services/provision.service';
import { ResourceService } from '../../shared/services/resource.service';
import Swal from 'sweetalert2';
import { UtilService } from '../../shared/services/util.service';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { PopupHistoryComponent } from '../../home/popup-history/popup-history.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { faEye,faEllipsisV } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-view-history',
  templateUrl: './view-history.component.html',
  styleUrls: ['./view-history.component.scss']
})
export class ViewHistoryComponent implements OnInit {
  reqst: any;
   faEye = faEye;
  faEllipsisV = faEllipsisV;
  reqArray = []
  loadergif = '../../../assets/images/turn.gif'
  showload: boolean = true
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
   data:any;
   status: any;
  constructor(
  	private proservice: ProvisionService,
     public utilService: UtilService,
    private resourceservice: ResourceService,
    private modalService: NgbModal,) { 
      this.getAllhistoryRec();


      const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'View History' }]
    this.utilService.changeBreadcrumb(breadcrumb);
  	}


  ngOnInit(): void {

    this.dtOptions = {
        initComplete: function(settings, json) {
          $('.dt-button').detach().appendTo('.download-btns ul');
          $('body').find('.dataTables_scroll').css('border-bottom', '0px');
          $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
          $('body').find('.dataTables_filter input').css({
            "border-width": "0px",
            "border-bottom": "1px solid #b1b8bb",
            "outline": "none",
            "width": "150px",
            "margin-bottom": "0px",
            "margin-right": "0px !important",
            "margin-left": "0px !important"
          }),
          $('body').find('.download-btns .dt-button').css({
          'background' :'none',
          'padding': '0.02rem 0.8rem',
          'margin': '0px',
          'border': '0px',
          'font-size': '11px',
          'line-height': '20px',
          'width': '100%',
          'display':'block',
          'text-align': 'left'
        })
          },
          dom: 'lBfrtip',
        buttons: [
        {
          extend: 'pdfHtml5',
          title : 'User History',
          text : 'Download - PDF',
          orientation: 'landscape',
          pageSize: 'A4',
          titleAttr : 'PDF',
          width:'100%'
     
        },
        {
          extend: 'csv',
          title : 'User History',
          text : 'Download - CSV'
        },
        {
          extend: 'excel',
          title : 'User History',
          text : 'Download - EXCEL'
        }           
        ],
          scrollCollapse: true,
          paging:true,
          select:true,
          bFilter: true, 
          bInfo: true,
          ordering: true,
          lengthMenu: [10, 25, 50,100,500,1000,2000],
          
      }
  }



  getAllhistoryRec(noUpdate?){
    let p ={
      'records':'all'
    }
    this.proservice.provisionlist(p).subscribe(res => {
      console.log(res)
      this.showload = false;

      let revArray;
      if(res.Records.length>0){  
        revArray = res.Records.reverse();
      }
      else{
         revArray = res.Records;
      }
      var i = res.Records.length+1;
      for(let items of revArray){
        i--
        var rid = 'Request Id';
        this.reqArray.push({id:items[rid],len:i,itm:items})
      }
      if (noUpdate) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
          
        })
      } 
      else {
        this.dtTrigger.next();
      }
     // console.log(this.reqArray)
    });
  }

  viewStatus(id){
    const breadcrumb = [{ name: 'Home', link: '/',type :'serv'}, { name: 'View History' },{ name: id } ]
    this.utilService.changeBreadcrumb(breadcrumb);
   const ngmodalRef = this.modalService.open(PopupHistoryComponent, {
            size: 'lg',
            backdrop: 'static',
            windowClass: 'customhistory'
          });
          ngmodalRef.componentInstance.id = id;
          ngmodalRef.componentInstance.loc = 'header';
          ngmodalRef.componentInstance.closeviewpopup.subscribe((rdata1) => {
          if(rdata1 == 'close'){
            const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'View History' }]
            this.utilService.changeBreadcrumb(breadcrumb);
          }
          })
   
  }
}
