import { Component, OnInit, ViewChild  } from '@angular/core';
import { PlatformServiceService } from '../../shared/services/platform-service.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { UtilService } from '../../shared/services/util.service';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  
import { faEllipsisV,faBell,faCog,faWrench,faCloud } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  datad=[]
  datadynamic=[]
  columnNamesdynamic=[]
  faEllipsisV=faEllipsisV
  faBell=faBell
  faCog=faCog
  faWrench=faWrench
  faCloud=faCloud
  cplatform:any;
  cservice:any;
  cresource:any;
  cinstance:any;
  Trequest:any;
  Tdelete:any;
  Tcreate:any;
  Tupdate:any;
  fullDataArray= []
  lineMonthlyArray= []
  lineMonthArray= []
  lineMonthArray1= []
  selectedIntC:string='monthly';
  selectedIns:string='monthly';
  a1= []
  SingleInst:any;
  loadergif = '../../../assets/images/turn.gif'
  showload: boolean = true
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    nav: true,
    navText: [ '<i class="fa-chevron-left"></i>', '<i class="fa-chevron-right></i>"' ],
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      },
      1024 : { items : 3}
    }
  }

  titled = '';
  title = '';
   typed = 'PieChart';
   type = 'ColumnChart';
   type_line = 'LineChart';
   servArray = []
   servArray1 = []
   servArray2 = []

   showchart:boolean= false
   showchart_line:boolean= false
   dataC = [];
  data_line = []
   columnNamesd = [{'string':'Browser'},{'number' : 'Percentage'}];

   columnNames = [
   {label: "platform", type: "string"},
   ];
   columnNames_line = [
   {label: 'Month', type: 'string'},
   ];
  
   optionsd = { 
   titleTextStyle: {
        fontSize: 10, 
        bold: 600, 
    },
    pieHole: 0.6,
    sliceVisibilityThreshold:0,
    pieSliceText: 'none',
    
    legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 8, alignment: 'center'}},  
    hAxis: {
        title: 'Platform'
      },
       isStacked: true
   };
   options = {
        legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
        isStacked: true,
      };
    options_line = {
      legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
      isStacked: true,
    };
    widthd = 200;  
   heightd = 200;  
   width = 450;  
   height = 200;  
  width22 = 250;  
   height22 = 180;
  constructor(private platformservice: PlatformServiceService,
    public utilService: UtilService) { 

    const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'Dashboard' }]
    this.utilService.changeBreadcrumb(breadcrumb);

    
    this.platformservice.summary1().subscribe(res=>{
      this.showload = false;
     
      this.cplatform = res.Platforms.length;
      this.datad=[]
      let serv = 0
      let resCnt = 0  
      let instCnt = 0 

      for(let item of res.Platforms){
        this.datadynamic = []
        this.SingleInst = 0
        serv += item.Services.length;
        resCnt += item.Count;

       
        this.servArray2.push(item.Platform)
        
         this.servArray[item.Platform] = new Array();
          for( let i of item.Services){ 
           this.servArray[item.Platform].push({platform:item.Platform,Service:i.Service,count:i.Count})
          let CurrentServiceArray=[]
          let CurrentInstanceArray=[] 
          instCnt += i.Count;
          this.SingleInst += i.Count;
          CurrentServiceArray.push(i.Service)
          CurrentInstanceArray.push(i.Count)
          this.servArray1.push(i.Service)
          this.columnNames.push({label:i.Service,type: 'number'})

          this.datadynamic.push(CurrentServiceArray.concat(CurrentInstanceArray));
        
        }
   
      let str = [];
      let str1 = [];
      str.push(item.Platform)
      str1.push(item.Count)
      this.datad.push(str.concat(str1));
      this.fullDataArray.push({'platform':item.Platform,'servicecount':item.Services.length,'instanceCount':this.SingleInst,'colName':this.columnNamesdynamic,'loopData':this.datadynamic})
      }
       let a1=[]
        
        for(let k=0;k<res.Platforms.length;k++){
        let p = []
        var aa = this.pushArray(serv)
   
        p.push(res.Platforms[k].Platform)
        for(let h=0;h<serv;h++){

           if(this.servArray[this.servArray2[k]][h] && this.servArray[this.servArray2[k]][h] != 'undefined'){

             var indx = this.servArray1.indexOf(this.servArray[this.servArray2[k]][h].Service)            
             var str = this.servArray[this.servArray2[k]][h].count;

             if (indx !== -1) {
        
                aa[indx] = this.servArray[this.servArray2[k]][h].count;
              }
                 

           }
           

        }

        this.dataC.push(p.concat(aa))        
      }

      this.cservice = serv;
      this.cresource = resCnt;
      this.cinstance = instCnt;
      this.showchart= true
   
    /*  console.log(  this.data)
      console.log(  this.columnNames)*/
      
    })
    this.platformservice.dashboardrequestCount().subscribe(res=>{
      console.log(res)
      if(res.StatusCode == 200){
        this.Tcreate = res.Result.TotalCreates
        this.Tdelete = res.Result.TotalDeletes
        this.Trequest = res.Result.TotalRequests
        this.Tupdate = res.Result.TotalUpdates
      }
    })
    this.getInstanceCount(this.selectedIns)

  }


  titleI = '';
   typeI = 'ColumnChart';
   dataI = [
     ["Jan-20", 1050,1000,250,1050,520],
      ["Feb-20", 1100,560,260,1500,200],
      ["Mar-20", 1500,1000,250,2700,290],
      ["Apr-20", 1600,1100,600,2900,1400],
      ["May-20", 2000,1200,400,3400,1200],
      ["Jun-20", 2100,1400,500,3500,1500],

   ];
   columnNamesI = ['Month', 'Transit Gateway Peering','Mail Relay Endpoint','LDAPS Endpoint','APIGEE','Hardened Images Library'];
   optionsI = {legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
        isStacked: true,};
   widthI = 380;  
   heightI = 175;

    titleInt = '';
   typeInt = 'ColumnChart';
   dataInt = [
     ["Jan-20", 1050,1000,250,1050,520,200],
      ["Feb-20", 1100,560,260,1500,200,300],
      ["Mar-20", 1500,1000,250,2700,290,400],
      ["Apr-20", 1600,1100,600,2900,1400,500],
      ["May-20", 2000,1200,400,3400,1200,200],
      ["Jun-20", 2100,1400,500,3500,1500,250],

   ];
   //columnNamesInt = ['Month','G Suite Group','G Suite Group Membership','GCP IAM Roles','GCP IAM Building','GCP Folder','Hardened Images Library' ];
   columnNamesInt = [{label: 'Month', type: 'string'},
      {label: 'G Suite Group', type: 'number'},
      {label: 'G Suite Group Membership', type: 'number'},
      {label: 'GCP IAM Roles', type: 'number'},
      {label: 'GCP IAM Buildins', type: 'number'},
      {label: 'GCP Folder', type: 'number'},
      {label: 'Hardened Images Library', type: 'number'}];
   optionsInt = {};
   widthInt = 350;
   heightInt = 300;
  
    title1 = '';
   type1 = 'ColumnChart';
   data1 = [
      ["Jan-20", 200],
      ["Feb-20", 410],
      ["Mar-20", 950],
      ["Apr-20", 1000],
      ["May-20", 1150],
      ["Jun-20", 1300],

   ];
   columnNames1 = ['Month','Data'];
   options1 = {};
   width1 = 350;
   height1 = 300;
 
  ngOnInit(): void {
  }

  pushArray(serv){
    let a2=[]
    for(let h=0;h<serv;h++){
        a2.push(0)
      }
      return a2;
  }
  selectedC(platform){ 

  }
  selectedLineGraph(){ 
    this.getInstanceCount(this.selectedIns)
  }

getInstanceCount(type){
  let params = {
      "StartDate": "2020-05-01",
      "EndDate": "2020-06-30",
      "Type": type
    }

    this.platformservice.getInstanceCount(params).subscribe(res=>{

     //let line_length = res.Result.length;
      for(let item of res.Result){
      this.columnNames_line.push({ label: item.PlatformName, type: 'number'})
      this.lineMonthArray.push(item.PlatformName)
       let monthArray=[]
       let monthInsArray=[]
       let line_length = item.Instances;
       this.lineMonthlyArray[item.PlatformName] = new Array();
        for(let i of item.Instances){
          this.lineMonthlyArray[item.PlatformName].push({'month':i.Month,'inst':i.Instances})
          if(!this.lineMonthArray1.includes(i.Month)){
          this.lineMonthArray1.push(i.Month)
          }
        }
      }
    
      for(let l=0;l<this.lineMonthArray1.length;l++){
        let aa=[]
        let p1 = []
        p1.push(this.lineMonthArray1[l])

        for(let k=0; k<res.Result.length; k++){
          if(this.lineMonthlyArray[this.lineMonthArray[k]][l].month == this.lineMonthArray1[l]){
            aa.push(this.lineMonthlyArray[this.lineMonthArray[k]][l].inst)
          }
        }
        this.data_line.push(p1.concat(aa)) 
        this.showchart_line = true
      }
    })
}


  generatePDF(id){
    console.log(id)
    var data = document.getElementById(id);  
    html2canvas(data).then(canvas => {  
      // Few necessary settingo ptions  
      var imgWidth = 200;   
      var pageHeight = 200;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('oaf.pdf'); // Generated PDF   
    }); 
    
  }
  generateJPG(id){
    var container = document.getElementById(id); 
      html2canvas(container).then(function(canvas) {
                var link = document.createElement("a");
                document.body.appendChild(link);
                link.download = "oaf.png";
                link.href = canvas.toDataURL("image/png");
                link.target = '_blank';
                link.click();
            });
  }


}
