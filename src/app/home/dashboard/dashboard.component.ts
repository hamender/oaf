import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,TemplateRef  } from '@angular/core';
import { AddPlatformComponent } from '../../pages/platform/add-platform/add-platform.component';
import { AddServiceComponent } from '../../pages/services/add-service/add-service.component';
import { AddResourceComponent } from '../../pages/resource/add-resource/add-resource.component';
import { AddInstanceComponent } from '../../pages/instance/add-instance/add-instance.component';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PlatformServiceService } from '../../shared/services/platform-service.service';
import { CustomerserviceService } from '../../shared/services/customerservice.service';
import { AuthenticationService } from '../../shared/services/authentication.service'
import { EditPlatformComponent } from  '../../pages/platform/edit-platform/edit-platform.component';
import { EditServiceComponent } from '../../pages/services/edit-service/edit-service.component';
import { UtilService } from '../../shared/services/util.service';
import { ResourceService } from '../../shared/services/resource.service';
import { ProvisionService } from '../../shared/services/provision.service';
import {ShContextMenuModule} from 'ng2-right-click-menu';
import { PlateformComponent } from '../../pages/platform/plateform/plateform.component';
import { EditProvisionComponent } from '../../pages/provision/edit-provision/edit-provision.component'
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import 'datatables.net';
import { Pipe, PipeTransform } from '@angular/core';
import { of } from 'rxjs';
import { Router } from "@angular/router";
import { map, delay } from 'rxjs/operators';
import { WithLoadingPipe } from '../../shared/customPipes/order-by.pipe';
import { trigger, transition, style, animate, query, stagger, state, keyframes } from "@angular/animations";
declare var $: any;
//import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { faEye, faPencilAlt, faCaretDown,faPlus, faEllipsisV,faBell,faCog,faWrench,faCloud,faTable,faChartBar} from '@fortawesome/free-solid-svg-icons';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  
})
export class DashboardComponent implements OnInit, AfterViewInit {
  faEye = faEye;
  faPencilAlt = faPencilAlt;
  faCaretDown = faCaretDown;
  faEllipsisV = faEllipsisV;
  faTable = faTable;
  faPlus = faPlus;
  faChartBar = faChartBar;
  obj$= of(1).pipe(delay(1500))
  closeResult: string;
  selectedVal:string='';
  selectedValService:string='';
  platformList: any;
  serviceList: any;
  passPlatValue: any;
  PlatformTable: boolean = false;
  ServiceTable: boolean = false;
  tableplat: boolean = false;
  backbtn: boolean = false;
  loader:boolean= true;
  pdrop:boolean = false
  showload: boolean = true
  sdrop:boolean
  dataArray= [];
  list: any;
  slist: any;
  resHeading= [];
  columns: { name: string; position: number,tableCls: string }[] = [];
  selectedColumn: number[];
  servHeading= [];
  servValue= [];
  serList= [];
  resData= [];
  nevershow: boolean = false;
  ccounter:number=0;

  commaSep: boolean = false;
  plist= [];
  tableData= [];
  indx:any
  globalslabel:any
  globalplabel:any
  showAddButton: boolean =  true
  customize: boolean = true;
  initialbread:boolean = true;
  message:string;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  dtTrigger1 = new Subject();
  dtable: boolean = false;
  globalName: boolean = true;
  globalN: boolean = true;
  collectValue= []
  globall : any;
  viewName: boolean = true;
  hidestandard: boolean = true;
  loadergif = '../../../assets/images/turn.gif'
  subscription:any;
  isDtInitialized:boolean = false;
  settings = {}

  //source: LocalDataSource;
  data = [];
 
 /* ------------------ Chart ------*/
 datad=[]
  datadynamic=[]
  columnNamesdynamic=[]
  chartview: boolean = true
  faBell=faBell
  faCog=faCog
  faWrench=faWrench
  faCloud=faCloud
  cplatform:any;
  cservice:any;
  cresource:any;
  cinstance:any;
  Trequest:any;
  Tdelete:any;
  Tcreate:any;
  Tupdate:any;
  fullDataArray= []
  lineMonthlyArray= []
  lineMonthArray= []
  lineMonthArray1= []
  selectedIntC = []
  selectedIns:string='monthly';
  a1= []
  SingleInst:any;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    nav: true,
    navText: [ '<i class="fa-chevron-left"></i>', '<i class="fa-chevron-right></i>"' ],
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      },
      1024 : { items : 3}
    }
  }

  titled = '';
  title = '';
   typed = 'PieChart';
   type = 'ColumnChart';
   type_line = 'LineChart';
   servArray = []
   servArray1 = []
   servArray2 = []

   showchart:boolean= false
   showchart_line:boolean= false
   dataC = [];
  data_line = [ 
 
   ]
   columnNamesd = [{'string':'Browser'},{'number' : 'Percentage'}];

   columnNames = [
   {label: "platform", type: "string"},
   ];
   columnNames_line = [
   {label: 'Month', type: 'string'},

   ];
  
   optionsd = { 
   titleTextStyle: {
        fontSize: 10, 
        bold: 600, 
    },
    pieHole: 0.6,
    sliceVisibilityThreshold:0,
    pieSliceText: 'none',
    
    legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 8, alignment: 'center'}},  
    hAxis: {
        title: 'Platform'
      },
       isStacked: true
   };
   options = {
        legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
        isStacked: true,
        chartArea: { 'width': '100%', height: '100%', top: '30%', left: '10%', right: '3%', bottom: '15%'}
      };
    options_line = {
      legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
      isStacked: true,
    };
    widthd = 210;  
    heightd = 210;
    widthdd = 260;  
   heightdd = 260;   
   width = 550;  
   height = 245;  
  width22 = 350;  
   height22 = 245;
   titleI = '';
   typeI = 'ColumnChart';
   dataI = [
/*     ["Jan-20", 1050,1000,250,1050,520],
      ["Feb-20", 1100,560,260,1500,200],
      ["Mar-20", 1500,1000,250,2700,290],
      ["Apr-20", 1600,1100,600,2900,1400],
      ["May-20", 2000,1200,400,3400,1200],
      ["Jun-20", 2100,1400,500,3500,1500],*/

   ];
   sliderColumn=[]
   sliderColumnData=[]
   sliderColumnData1=[]
   sliderMonth=[]
   sliderplatfomr=[]
   columnNamesI = [];
   optionsI = {legend: {position: 'bottom' , maxLines: 2, width: 100, textStyle: {color: '#000', fontSize: 9, alignment: 'center'}},  
        isStacked: true,};
   widthI = 400;  
   heightI = 175;

    titleInt = '';
   typeInt = 'ColumnChart';
   dataInt = [
     ["Jan-20", 1050,1000,250,1050,520,200],
      ["Feb-20", 1100,560,260,1500,200,300],
      ["Mar-20", 1500,1000,250,2700,290,400],
      ["Apr-20", 1600,1100,600,2900,1400,500],
      ["May-20", 2000,1200,400,3400,1200,200],
      ["Jun-20", 2100,1400,500,3500,1500,250],

   ];
   //columnNamesInt = ['Month','G Suite Group','G Suite Group Membership','GCP IAM Roles','GCP IAM Building','GCP Folder','Hardened Images Library' ];
   columnNamesInt = [{label: 'Month', type: 'string'},
      {label: 'G Suite Group', type: 'number'},
      {label: 'G Suite Group Membership', type: 'number'},
      {label: 'GCP IAM Roles', type: 'number'},
      {label: 'GCP IAM Buildins', type: 'number'},
      {label: 'GCP Folder', type: 'number'},
      {label: 'Hardened Images Library', type: 'number'}];
   optionsInt = {};
   widthInt = 350;
   heightInt = 300;
  
    title1 = '';
   type1 = 'ColumnChart';
   data1 = [
      ["Jan-20", 200],
      ["Feb-20", 410],
      ["Mar-20", 950],
      ["Apr-20", 1000],
      ["May-20", 1150],
      ["Jun-20", 1300],

   ];
   columnNames1 = ['Month','Data'];
   options1 = {};
   width1 = 350;
   height1 = 300; 


  constructor(private modalService: NgbModal,
    private platformservice: PlatformServiceService,
    private customerservice: CustomerserviceService,
    public auth: AuthenticationService,
    public utilService: UtilService,
    private resourceservice: ResourceService,
    private provisionservice: ProvisionService,
    private router: Router,

   ) { 

     const timer1 = JSON.parse(localStorage.getItem('timer'));
     if ((Date.now() > timer1) && timer1) {
      localStorage.clear();
      this.router.navigate(['/']);
     }

     
   // if(this.initialbread){
    const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'Dashboard' }]
    this.utilService.changeBreadcrumb(breadcrumb);
   // }
    

 
    this.utilService.currentMessage.subscribe(message => {

      if(message == 'table1' && this.selectedVal){
         // $('#table1').DataTable().clear().destroy();
          //$('#table2').DataTable().clear().destroy();
          $('#table3').DataTable().clear().destroy();
          let platform = this.selectedVal.split('__')
          const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: platform[0] }]
          this.utilService.changeBreadcrumb(breadcrumb);
        this.selected();
         this.ServiceTable = false;
     
      }
     
    })
    this.getSliderinstances('monthly')
  
  }


getPlatformList(){
 // $('#table1').DataTable().clear().destroy();
  //$('#table2').DataTable().clear().destroy();
  $('#table3').DataTable().clear().destroy();
  this.tableData = [] ;
  this.plist = [];
  this.tableplat = true;
    
  this.platformservice.summary1().subscribe(res1=>{
   // console.log(res1)
    this.showload = false
    if(res1.StatusCode == 200){
      this.cplatform = res1.Platforms.length;
      this.datad=[]
      let serv = 0
      let resCnt = 0  
      let instCnt = 0 
      for(let item of res1.Platforms){
        this.datadynamic = []
        this.SingleInst = 0
        serv += item.Services.length;
        resCnt += item.Count;

       
        this.servArray2.push(item.Platform)
        
         this.servArray[item.Platform] = new Array();
          for( let i of item.Services){ 
           this.servArray[item.Platform].push({platform:item.Platform,Service:i.Service,count:i.Count})
          let CurrentServiceArray=[]
          let CurrentInstanceArray=[] 
          instCnt += i.Count;
          this.SingleInst += i.Count;
          CurrentServiceArray.push(i.Service)
          CurrentInstanceArray.push(i.Count)
          this.servArray1.push(i.Service)
          this.columnNames.push({label:i.Service,type: 'number'})

          this.datadynamic.push(CurrentServiceArray.concat(CurrentInstanceArray));
        
        }
   
      let str = [];
      let str1 = [];
      str.push(item.Platform)
      str1.push(item.Count)
      this.datad.push(str.concat(str1));
      this.fullDataArray.push({'platform':item.Platform,'platformindex':item.Index_type,'servicecount':item.Services.length,'instanceCount':this.SingleInst,'colName':this.columnNamesdynamic,'loopData':this.datadynamic})

      this.plist.push(item)
      }
      let a1=[]
        
        for(let k=0;k<res1.Platforms.length;k++){
        let p = []
        var aa = this.pushArray(serv)
   
        p.push(res1.Platforms[k].Platform)
        for(let h=0;h<serv;h++){

           if(this.servArray[this.servArray2[k]][h] && this.servArray[this.servArray2[k]][h] != 'undefined'){

             var indx = this.servArray1.indexOf(this.servArray[this.servArray2[k]][h].Service)            
             var str = this.servArray[this.servArray2[k]][h].count;

             if (indx !== -1) {
        
                aa[indx] = this.servArray[this.servArray2[k]][h].count;
              }
                 

           }
           

        }

        this.dataC.push(p.concat(aa))        
      }

      this.cservice = serv;
      this.cresource = resCnt;
      this.cinstance = instCnt;
      this.showchart= true
      this.platformList = res1.Platforms;
      this.tableData = this.plist;
      setTimeout(()=>{
      this.dtTableLoad('table3','A4')

      },1000);
      
    }

  })
  this.platformservice.dashboardrequestCount().subscribe(res=>{
      console.log(res)
      if(res.StatusCode == 200){
        this.Tcreate = res.Result.TotalCreates
        this.Tdelete = res.Result.TotalDeletes
        this.Trequest = res.Result.TotalRequests
        this.Tupdate = res.Result.TotalUpdates
      }
    })
    this.getInstanceCount(this.selectedIns)
    
  
}

sortArray(arr:any){
  return arr.sort((a, b) => (a.Service > b.Service) ? 1 : -1)

}


onClick(event, type){
  let spl = event.data.split('__')
    if(type == 'edit'){  
      const ngmodalRef2 = this.modalService.open(PlateformComponent, {
            size: 'md',
            backdrop: 'static'
          });
          ngmodalRef2.componentInstance.platId = spl[0];
          ngmodalRef2.componentInstance.passEntry.subscribe((rdata) => {
          if(rdata.statusCode == 200){
            this.tableplat = false;
            this.getPlatformList();
          }
          })
          ngmodalRef2.componentInstance.dplatEntry.subscribe((rdata1) => {
          if(rdata1.statusCode == 200){
            this.tableplat = false;
            this.getPlatformList();

          }
          })
    }
    if(type == 'view'){  
      const ngmodalRef2 = this.modalService.open(PlateformComponent, {
            size: 'md',
            backdrop: 'static'
          });
          ngmodalRef2.componentInstance.viewId = spl[0];
    }
    /*if(type == 'del'){  
      this.delPlatform(spl[0])
    }*/
}

onClick1(event,type){
  let spl = event.data.split('__')
  if(type == 'edit'){  
    const ngmodalRef4 = this.modalService.open(EditServiceComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    ngmodalRef4.componentInstance.servId = spl[0];
    ngmodalRef4.componentInstance.platId = spl[1];
    ngmodalRef4.componentInstance.eService.subscribe((rdata) => {
    if(rdata.statusCode == 200){
      this.tableplat = false;
      this.getPlatformList();
    }
    })
    ngmodalRef4.componentInstance.dservEntry.subscribe((rdata1) => {
    if(rdata1.statusCode == 200){
      this.tableplat = false;
      this.getPlatformList();
      
    }
    })

  }
  if(type == 'view'){  
    const ngmodalRef4 = this.modalService.open(EditServiceComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    ngmodalRef4.componentInstance.viewId = spl[0];
    ngmodalRef4.componentInstance.viewpId = spl[1];
  }
  /*if(type == 'del'){  
    this.delService(spl[0], spl[1])
  }*/

}

openPlatform() {
  const ngmodalRef = this.modalService.open(AddPlatformComponent, {
    size: 'lg',
    backdrop: 'static'
  });
  ngmodalRef.componentInstance.passEntry1.subscribe((rdata) => {
  if(rdata.statusCode == 200){
    this.tableplat = false;
      this.getPlatformList();
  } this.showload = true;
  })
}

openService(){
  const ngmodalRef2 = this.modalService.open(AddServiceComponent, {
    size: 'lg',
    backdrop: 'static'
  });
    ngmodalRef2.componentInstance.aService.subscribe((rdata) => {
    //console.log(rdata)
  if(rdata.statusCode == 200){
    if(this.selectedVal){
    //$('#table1').DataTable().clear().destroy();
    this.selected();
    }
    else if(this.selectedValService){
      this.selectedservice();
    }
    else{
      this.tableplat = false;
      this.getPlatformList();
      this.showload = true;
    }
  }
  })
}

resouce(){
  const ngmodalRef31 = this.modalService.open(AddResourceComponent, {
    size: 'lg',
    backdrop: 'static',
    windowClass: 'custom-class'
  });
 // console.log(ngmodalRef31)
  ngmodalRef31.componentInstance.aResource.subscribe((res) => {
   // console.log(res)
  if(res.StatusCode == 200){
   //$('#table1').DataTable().clear().destroy();
   if(this.selectedVal){
    this.selected();

   }
   else{
    this.tableplat = false;
    this.getPlatformList();
    this.showload = true;
   }
  }
  })
}

instance(){
  const ngmodalRef3 = this.modalService.open(AddInstanceComponent, {
    size: 'lg',
    backdrop: 'static'
  });
   ngmodalRef3.componentInstance.aInstance.subscribe((rdata) => {
   // console.log(rdata)
  if(rdata.StatusCode == 200){
    if(this.selectedVal){
      this.selected();
    }
    else if(this.selectedValService){
     // //$('#table2').DataTable().clear().destroy();
      this.selectedservice();
    }
    else{
      this.tableplat = false;
       this.getPlatformList();
       this.showload = true;
    }
    
  }
  })
}

ngOnInit(): void {
   //this.source = new LocalDataSource();
  if(localStorage.getItem('Attribute')){
      let attribute = JSON.parse(localStorage.getItem('Attribute'));
      this.globall = attribute.groups[0];
      if(this.globall == 'Administrator'){
       this.globalName = false;
       this.globalN = true;
       this.viewName = true;
       this.showAddButton = true;
       this.hidestandard = false;
          }else if(this.globall == 'Standard'){
             this.globalName = false;
             this.globalN = false;             
             this.viewName = false;
             this.hidestandard = false;
             this.showAddButton = false;
          }else{
             this.globalName = true;
             this.viewName =true;
             this.hidestandard = true;
             this.globalN = true;
             this.showAddButton = true;
             

          }
        }
 /* setTimeout(()=>{
  //this.loader = false;
    this.dtTableLoad('table3','A4')
   },5000);*/
   //
   setTimeout(()=>{
      this.getPlatformList();
    },100);
    if(localStorage.getItem('Attribute')){
      let attribute = JSON.parse(localStorage.getItem('Attribute'));
      //console.log(attribute.groups[0])

    }
    this.dtOptions = {
      initComplete: function(settings, json) {
        console.log('init completed');

        //$('body').find('.dataTables_scroll').css('border-bottom', '0px');
        //$('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
        $('.dt-button').detach().appendTo('.download-btns ul');
        $('body').find('.dataTables_filter input').css({
          "border-width": "0px",
          "border-bottom": "1px solid #b1b8bb",
          "outline": "none",
          "width": "150px",
          "margin-bottom": "0px",
          "margin-right": "0px !important",
          "margin-left": "0px !important"
        }),
        $('body').find('.download-btns .dt-button').css({
          'background' :'none',
          'padding': '0.02rem 0.8rem',
          'margin': '0px',
          'border': '0px',
          'font-size': '11px',
          'line-height': '20px',
          'width': '100%',
          'display':'block',
          'text-align': 'left'
        })
       
        },
        
        dom: 'lBfrtip',
        buttons: [
        {
          extend: 'pdfHtml5',
          title : 'ddd',
          text : 'Download - PDF',
          orientation: 'landscape',
          pageSize: 'A4',
          titleAttr : 'PDF',
          width:'100%',
          customize: function(doc) {
           doc.styles.table = {
             alignment: 'center'
           }
         }  
        },
        {
          extend: 'csv',
          title : 'ddd',
          text : 'Download - CSV'
        },
        {
          extend: 'excel',
          title : 'ddd',
          text : 'Download - EXCEL'
        }           
        ],
        scrollCollapse: true,
         scrollX: true,
         //scrollY: 600,
        paging:true,
        select:true,
        bFilter: true, 
        bInfo: true,
        ordering: true,
        lengthMenu: [10, 25, 50,100,500,1000,2000]
        };
}

selected(event?,indx?){
  if(event || indx){
    this.selectedVal = event+'__'+indx;
  }
  this.resData= [];
  this.resHeading= []


  this.showload = true;
  this.globalplabel = ''
  //$('#table1').DataTable().clear().destroy();
  //$('#table2').DataTable().clear().destroy();
  $('#table3').DataTable().clear().destroy();
 
  let platt = this.selectedVal;
 // this.pdrop = false
  this.tableplat = false;
  let spl = platt.split('__');
  this.globalplabel = this.selectedVal;
  //this.stelectedPlatform = spl[0];

 let params = {
      'platform': spl[0]
    }
    
    this.PlatformTable = false;
    this.dataArray= [];
    
    this.columns = []
    this.selectedValService= '';

    this.selectedColumn = [];
    const breadcrumb1 = [{ name: 'Home', link: '/',type :'plat'},{ name: spl[0] }]
    this.utilService.changeBreadcrumb(breadcrumb1);

     this.resourceservice.list(params).subscribe(res => {
      //console.log(res)
      this.showload = false
    if(res.statusCode == 200){
      this.pdrop = false;
      
      this.ServiceTable = false;
      this.customize = false;
      let indexArray = []
      let ii=0;
      let countArray= []
      var loadCounter = 0;
      var len = res.Metadata.length;

      for(let item of res.Metadata){

        
          let selctindx = []
          let newArray ={}
          let newArray1 = []
         
        for(let i of Object.keys(item)){
          ii++;
          if(!this.resHeading.includes(i)){  
          this.resHeading.push(i);
          newArray[i]= new Object()
          newArray[i]['title'] = i
          newArray[i]['filter']= false
          
          //newArray1.push(i)

           this.settings['columns'] = newArray;
          this.columns.push({name: i, position: ii,tableCls:'table1'})
          selctindx.push(ii)
          this.selectedColumn = selctindx

          }
        }
      }
      this.settings['hideSubHeader'] = true;
      //console.log(this.settings)
       let newArray2 ={};
      for(let item of res.Metadata){
        let itype = item[spl[1]];
        let resData1= [];
       
        for(let v of this.resHeading){

          if(item[v]){
          if(Array.isArray(item[v])){
             let servArray= [];
              for(let v1 of item[v]){
                servArray.push(v1.service_name+' ('+v1.count+')')
              }
              resData1.push(servArray.toString().split(',').join(', '))
              newArray2[v]= servArray.toString().split(',').join(', ')
           }
           else{
             resData1.push(item[v])
              newArray2[v] = item[v]
           }
         }else{
           newArray2[v] = '--'
          resData1.push('--')
         }
        
        // console.log(newArray2)
        
        }
         this.data.push(newArray2)
        

      
           this.resData.push({'rec':resData1,'id':itype,'plat':spl[0],'index':spl[1]})
           let lsize = 'A4'
          if(this.resHeading.length>8){
            lsize = 'A0'
          }
          else{
             lsize = 'A4'
          }
          loadCounter++;
      
    
         if(loadCounter === len){
         this.PlatformTable = true;
          }
    
    
      }
        this.ccounter++;  

    }
    else{
      this.showload = false
       Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.message,
          showConfirmButton: false,
          timer: 1500
        })

       if(this.pdrop == true){ 
       this.tableplat = true;
       
       this.pdrop = false
       setTimeout(()=>{
          this.dtTableLoad('table3','A4')
          this.PlatformTable = false;
        },500);
       }
       this.ServiceTable = false;
       this.customize = true;
    }
  })
  this.ServInstance(spl[0])

    
}
  counter(i: number) {
    return new Array(i);
  }

  platResource(plat,index?){

     
  }
  ServInstance(plat){
   // console.log(plat)
   // $('#table1').DataTable().clear().destroy();
    //$('#table2').DataTable().clear().destroy();
    $('#table3').DataTable().clear().destroy();
    let params = {
      'platform': plat
    }
    this.customerservice.getServiceListbyID(params).subscribe(res=>{
    this.serviceList = res.Services;
    });
  }

  selectedservice(event?,platt?,index?){
    this.pdrop= false;
    this.ServiceTable = false;
    this.showload = true;
     if(event){
      this.selectedValService = event+'__'+index;
      this.pdrop= true;
    }
    //console.log(this.selectedValService)
    this.globalslabel= '';
   // $('#table1').DataTable().clear().destroy();
    //$('#table2').DataTable().clear().destroy();
    $('#table3').DataTable().clear().destroy();
    this.ServiceTable = false;

     if(platt){
      this.selectedVal = platt;
     }
      let plat = this.selectedVal;
      let spl = plat.split('__');
      this.globalslabel = this.selectedValService;
      let servSplit = this.selectedValService.split('__')

      //this.selectServ = servSplit[0];
      //this.getListService(servSplit[0], spl[0],servSplit[1])
       let params = {
        'platform': spl[0],
        'service': servSplit[0],
      }
         this.selectedColumn = [];
         this.columns = [];
         this.servHeading= []
          const breadcrumb =  [{ name: 'Home', link: '/',type :'serv'},{ name: spl[0], link:'plat' }, { name: servSplit[0] }]
          this.utilService.changeBreadcrumb(breadcrumb);
        this.provisionservice.getProvisionList(params).subscribe(res=>{
        this.PlatformTable = false;
        this.showload = false;
        this.customize = false;
          if(res.statusCode == 200){
            this.pdrop == false;
            this.servValue= [];
            var loadCounter = 0;
            var len = res.Metadata.length;
            for(let item of res.Metadata){
              let iii = 0;
              //this.selectedColumn = [];
              //this.columns = [];
             
              let selctindx = []
              for(let i of Object.keys(item)){
                iii++
              if(!this.servHeading.includes(i)){  
              this.servHeading.push(i);
              this.columns.push({name: i, position: iii,tableCls:'table2'})
              selctindx.push(iii)
              this.selectedColumn = selctindx
              }
              }
              }
              for(let item of res.Metadata){
              let sArray= []
              //console.log(this.servHeading)
               for(let v of this.servHeading){
                
                if(item[v]){
                sArray.push(item[v])
               }else{
 
                sArray.push('--')
               }
              }

              this.indx = item[servSplit[1]];
            this.servValue.push({'val':sArray,'plat':spl[0],'serv':servSplit[0],'index':this.indx})
            let lsize = 'A4'
          if(this.servHeading.length>8){
            lsize = 'A0'
          }
          else{
             lsize = 'A4'
          }
          loadCounter++;
           /* setTimeout(()=>{
              $('#table2').DataTable().destroy();
              //this.loader = false;
              this.dtTableLoad('table2',lsize)
              this.ServiceTable = true;
             },100);*/
             if(loadCounter === len){
         this.ServiceTable = true;
        /* setTimeout(()=>{
           this.dtTrigger1.next();
      
         },2000);*/
      
         

      /*setTimeout(()=>{
        this.showload = false
              $('#table1').DataTable().destroy();
              //this.loader = false;
              this.dtTableLoad('table1',lsize)
              this.PlatformTable = true;
             },1000);*/
    }
            }
          }
          else{
            Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.message,
          showConfirmButton: false,
          timer: 1500
        })
       //this.selectServ = '';
      //this.stelectedPlatform = '';
      //this.anyService = false;
      //this.anyPlat = false;
      
       this.customize = true;
       if( this.pdrop == true){
       this.tableplat = true;
       this.ServiceTable = false;
       setTimeout(()=>{
        this.dtTableLoad('table3','A4')
       },100);
       }
       if( this.pdrop == false){
        this.tableplat = false;
       this.ServiceTable = false;
       /*setTimeout(()=>{
        this.dtTableLoad('table2')
       },500);*/
       }

          }
        })
      //this.anyService = true;

  }

platformSelection(event,index){
  //console.log(event)
 // console.log(index)
  this.globalplabel = ''
  //this.showload = true;
  this.pdrop = true;
  //$('#table1').DataTable().clear().destroy();
  //$('#table2').DataTable().clear().destroy();
  $('#table3').DataTable().clear().destroy();
   //this.stelectedPlatform = event;
  this.globalplabel = event+'__'+index;
  //this.selectedVal = event;
  this.selected(event,index);
  this.tableplat = false;
 // this.selected(this.globalplabel);
}

  serviceSelection(event,plat,index,indexp){
    
    //this.showload = true;
    //this.pdrop = true
   // $('#table1').DataTable().clear().destroy();
    //$('#table2').DataTable().clear().destroy();
    $('#table3').DataTable().clear().destroy();
       this.globalplabel = plat+'__'+indexp;
       this.globalslabel = event+'__'+index;

     this.ServInstance(plat)
     this.selectedservice(event,this.globalplabel,index);
      this.tableplat = false;
      //this.selected(this.globalplabel)
      
  }


  getListService(serv,plat,index){
    
  }
  backBtn(){
     this.tableplat = true;
     this.PlatformTable = false;
     this.backbtn = false;
  }

   checkValue(values, position,tableClass) {
    this.collectValue.push({'val':values,'pos':position})
    /*const index = this.selectedColumn.findIndex(column => column === position);
    if (index >= 0) {
      this.selectedColumn.splice(index, 1);
    } else {
      this.selectedColumn.push(position);
    }
    const table = $('.table1').DataTable();
    const column = table.column(position);
    column.visible(!column.visible());*/
    
  }
  submitCHange(values){
   // console.log(values)
    if(values){ 
      for(let val of values){

      const index = this.selectedColumn.findIndex(column => column === val.pos);
      if (index >= 0) {
        this.selectedColumn.splice(index, 1);
      } else {
        this.selectedColumn.push(val.pos);
      }
      const table = $('.table1').DataTable();
      const column = table.column(val.pos);
      column.visible(!column.visible());
      }
    }
     this.collectValue=[] 
  }

open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



  editPlatform(index,plat,indextype){

    const ngmodalRef3 = this.modalService.open(EditPlatformComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef3.componentInstance.platId = plat;
    ngmodalRef3.componentInstance.resId = index;
    ngmodalRef3.componentInstance.index = indextype;
    ngmodalRef3.componentInstance.passEntry2.subscribe((rdata) => {
          if(rdata.StatusCode == 200){
            this.selected();
          }
          })
    ngmodalRef3.componentInstance.delRes.subscribe((rdata1) => {
     // console.log(rdata1)
          if(rdata1.StatusCode == 200){
            this.selected();
          }
          })

  }
  viewPlatform(index,plat,indextype){
    const ngmodalRef3 = this.modalService.open(EditPlatformComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef3.componentInstance.viewId = plat;
    ngmodalRef3.componentInstance.viewresId = index;
    ngmodalRef3.componentInstance.index = indextype;
  }

  editProvision(plat,serv,index){
   
    const ngmodalRef4 = this.modalService.open(EditProvisionComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    ngmodalRef4.componentInstance.plat = this.globalplabel;
    ngmodalRef4.componentInstance.serv = this.globalslabel;
    ngmodalRef4.componentInstance.sid = index;
    ngmodalRef4.componentInstance.editP.subscribe((rdata1) => {
    if(rdata1.StatusCode == 200){
      this.selectedservice();
    }
    })
    ngmodalRef4.componentInstance.dprovision.subscribe((rdata1) => {
    if(rdata1.StatusCode == 200){
      this.selectedservice();
    }
    })
  }
  
  viewProvision(plat,serv,index){
    const ngmodalRef5 = this.modalService.open(EditProvisionComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    ngmodalRef5.componentInstance.vserv = this.globalslabel;
    ngmodalRef5.componentInstance.vplat =  this.globalplabel;
    ngmodalRef5.componentInstance.sid = index;
    
  }
  dtTableLoad(tableId='',pageS = 'A4'){
   
    var pagetitle = this.selectedVal+' '+this.selectedValService;
    let ptitle = ''
    if(this.selectedVal == '' && this.selectedValService == ''){
      ptitle = 'All Platform & Services Data'
    }
    if(this.selectedVal && this.selectedValService== ''){
      ptitle = this.selectedVal.split('__')[0]
    }
    if(this.selectedValService && this.selectedVal== ''){
      ptitle = 'All Services'
    }
    if(this.selectedValService && this.selectedVal){
      ptitle = this.selectedVal.split('__')[0] + ' >> ' +this.selectedValService.split('__')[0]
    }

    let dbOption: any = {};
    dbOption = {
      initComplete: function(settings, json) {
        console.log('init completed');

        //$('body').find('.dataTables_scroll').css('border-bottom', '0px');
        //$('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
        $('.dt-button').detach().appendTo('.download-btns ul');
        $('body').find('.dataTables_filter input').css({
          "border-width": "0px",
          "border-bottom": "1px solid #b1b8bb",
          "outline": "none",
          "width": "150px",
          "margin-bottom": "0px",
          "margin-right": "0px !important",
          "margin-left": "0px !important"
        }),
        $('body').find('.download-btns .dt-button').css({
          'background' :'none',
          'padding': '0.02rem 0.8rem',
          'margin': '0px',
          'border': '0px',
          'font-size': '11px',
          'line-height': '20px',
          'width': '100%',
          'display':'block',
          'text-align': 'left'
        })
       
        },
        
        dom: 'lBfrtip',
        buttons: [
        {
          extend: 'pdfHtml5',
          title : function() {
                  return ptitle;
              },
          text : 'Download - PDF',
          orientation: 'landscape',
          pageSize: pageS,
          titleAttr : 'PDF',
          width:'100%',
          customize: function(doc) {
     doc.styles.table = {
       alignment: 'center'
     }
   }  
        },
        {
          extend: 'csv',
          title : function() {
                  return ptitle;
              },
          text : 'Download - CSV'
        },
        {
          extend: 'excel',
          title : function() {
                  return ptitle;
              },
          text : 'Download - EXCEL'
        }           
        ],
        scrollCollapse: true,
         scrollX: true,
         //scrollY: 600,
        paging:true,
        select:true,
        bFilter: true, 
        bInfo: true,
        ordering: true,
        lengthMenu: [10, 25, 50,100,500,1000,2000]
        };
        $('#'+tableId).DataTable(dbOption);
    }

    ngAfterViewInit() {
      
    }
    pushArray(serv){
    let a2=[]
    for(let h=0;h<serv;h++){
        a2.push(0)
      }
      return a2;
  }
  selectedC(i, platform){     
    console.log(this.selectedIntC[i])
    this.getSliderinstances(this.selectedIntC[i])   
  }
  selectedLineGraph(){ 
    this.getInstanceCount(this.selectedIns)
  }

getInstanceCount(type){
  let params = {
      "StartDate": "2020-05-01",
      "EndDate": "2020-06-30",
      "Type": type
    }

    this.platformservice.getInstanceCount(params).subscribe(res=>{

     //let line_length = res.Result.length;
      for(let item of res.Result){
      this.columnNames_line.push({ label: item.PlatformName, type: 'number'})
      this.lineMonthArray.push(item.PlatformName)
       let monthArray=[]
       let monthInsArray=[]
       let line_length = item.Instances;
       this.lineMonthlyArray[item.PlatformName] = new Array();
        for(let i of item.Instances){
          this.lineMonthlyArray[item.PlatformName].push({'month':i.Month,'inst':i.Instances})
          if(!this.lineMonthArray1.includes(i.Month)){
          this.lineMonthArray1.push(i.Month)
          }
        }
      }
    
      for(let l=0;l<this.lineMonthArray1.length;l++){
        let aa=[]
        let p1 = []
        p1.push(this.lineMonthArray1[l])

        for(let k=0; k<res.Result.length; k++){
          if(this.lineMonthlyArray[this.lineMonthArray[k]][l].month == this.lineMonthArray1[l]){
            aa.push(this.lineMonthlyArray[this.lineMonthArray[k]][l].inst)
          }
        }
        this.data_line.push(p1.concat(aa)) 
        this.showchart_line = true
      }
    })
}

getSliderinstances(type){
  let params = {
      "StartDate": "2020-05-01",
      "EndDate": "2020-06-30",
      "Type": type
    }
    this.platformservice.getInstanceSlider(params).subscribe(res=>{
      console.log(res)
      if(res.StatusCode == 200 && res.Result){
        for(let item of res.Result){
          //console.log(item.PlatformName)
          this.sliderplatfomr.push(item.PlatformName);
          let cnt1 = item.Instances.length;
          this.sliderColumnData[item.PlatformName] = new Array()
          for(let i of item.Instances){
          let b=[]
            if(!this.sliderMonth.includes(i.Month)){
              this.sliderMonth.push(i.Month)
            }
            this.sliderColumnData[item.PlatformName][i.Month] = new Array()

            if(Object.keys(i.Services).length>0){

              let b1=[]
              for (const [key, value] of Object.entries(i.Services)) {
                if(!this.columnNamesI.includes(key)){
                  this.columnNamesI.push(key)
               
                }
               this.sliderColumnData[item.PlatformName][i.Month][key]= new Array()
              this.sliderColumnData[item.PlatformName][i.Month][key].push(value)

              }

            }
            
          }
        }

       let ab = this.pushArray(this.columnNamesI.length);
     
       for(let a=0;a<this.sliderMonth.length;a++){
         let bb = []
         bb.push(this.sliderMonth[a])
          for(let l=0;l<res.Result.length;l++){
            let aaa = []
            for(let k =0;k<this.columnNamesI.length;k++){
              if(this.sliderColumnData[this.sliderplatfomr[l]][this.sliderMonth[a]][this.columnNamesI[k]]){
               let len = this.sliderColumnData[this.sliderplatfomr[l]][this.sliderMonth[a]][this.columnNamesI[k]].length;
              
               for(let h =0;h<len;h++){
                 aaa.push(this.sliderColumnData[this.sliderplatfomr[l]][this.sliderMonth[a]][this.columnNamesI[k]][h])
               }
              
              }
              else{
                 aaa.push(0)
              }
            }
            this.dataI.push(bb.concat(aaa))
            console.log(this.dataI)
          }
       }
     
        
      }
      console.log( this.sliderColumnData)
      console.log( this.columnNamesI)
     /* console.log(this.sliderColumn)
      console.log( this.sliderColumnData)
      console.log( this.sliderMonth)*/
    })
}

  generatePDF(id){
    console.log(id)
    var data = document.getElementById(id);  
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 100;   
      var pageHeight = 100;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      let pdf = new jspdf('p', 'mm', 'a6'); // A5 size page of PDF  
      var position = 0;  
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
      pdf.save('oaf.pdf'); // Generated PDF   
    }); 
    
  }
  generateJPG(id){
    var container = document.getElementById(id); 
      html2canvas(container).then(function(canvas) {
                var link = document.createElement("a");
                document.body.appendChild(link);
                link.download = "oaf.png";
                link.href = canvas.toDataURL("image/png");
                link.target = '_blank';
                link.click();
            });
  }
  ChangeView(state){
    if(state=='graph'){
      this.chartview= true
    }
    if(state=='table'){
          this.chartview= false
        }
  }
  editplatformCharts(pname){    
    //this.chartview = false;    
  const ngmodalRef2 = this.modalService.open(PlateformComponent, {    
        size: 'md',    
        backdrop: 'static'    
      });    
      ngmodalRef2.componentInstance.platId = pname;    
  }    
  viewplatformCharts(pname){    
    //this.chartview = false;    
    const ngmodalRef2 = this.modalService.open(PlateformComponent, {    
            size: 'md',    
            backdrop: 'static'    
          });    
          ngmodalRef2.componentInstance.viewId = pname;    
  }
}
