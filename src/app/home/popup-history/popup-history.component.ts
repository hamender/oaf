import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Output, Input, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProvisionService } from '../../shared/services/provision.service';
import { ResourceService } from '../../shared/services/resource.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-popup-history',
  templateUrl: './popup-history.component.html',
  styleUrls: ['./popup-history.component.scss']
})
export class PopupHistoryComponent implements OnInit {

  reqst: any;
   @Input() id: any;
   @Input() rqid: any;
   @Input() pid: any;

   @Input() loc: any;
   @Output() closeviewpopup: EventEmitter<any> = new EventEmitter();
   data:any;
   status: any;
   loadergif = '../../../assets/images/turn.gif'
   showload: boolean = true
  constructor(public activeModal: NgbActiveModal,
  	private proservice: ProvisionService,
    private resourceservice: ResourceService) { 
  	}


  ngOnInit() {

    if(this.id && this.loc && this.loc == 'header'){
       let param = {
      'Id': this.id
    }
    console.log(param)
    this.proservice.getSingleHistory(param).subscribe(res => {
      console.log(res)
      this.showload = false;
      let arr = [];
      for(let item of res.Record){
         console.log(item)
         var rid = 'Request Id';
         var ridVal = item[rid]
         arr.push({'itms':item,'rid':ridVal})
         this.status = item['Status'];
      }
      console.log(arr)
      this.data = arr;
    })
    }

    if(this.pid && this.loc && this.loc == 'provision'){
       let param = {
      'Id': this.pid
    }
    this.proservice.getSingleHistory(param).subscribe(res => {
      this.showload = false;
      console.log(res)
      let arr = [];
      for(let item of res.Record){
         console.log(item)
         var rid = 'Request Id';
         var ridVal = item[rid]
         arr.push({'itms':item,'rid':ridVal})
         this.status = item['Status'];
      }
      this.data = arr;
    })
    }
    
    if(this.rqid && this.loc && this.loc == 'resource'){
       let param = {
      'Id': this.rqid
    }
     this.proservice.getSingleHistory(param).subscribe(res => {
      this.showload = false;
      console.log(res)
      let arr = [];
      for(let item of res.Record){
         console.log(item)
         var rid = 'Request Id';
         var ridVal = item[rid]
         arr.push({'itms':item,'rid':ridVal})
         this.status = item['Status'];
      }
      this.data = arr;
    })
    }
  }

closeform(){
      this.activeModal.close();
      this.closeviewpopup.emit('close');
  }
}
