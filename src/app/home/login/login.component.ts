import { Component, OnInit, ElementRef, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { CustomValidators } from '../../shared/validator/custom-validators';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  href = 'https://app.oaf-x.com';
  closeResult: string;
  loginForm: FormGroup;
  codeForm: FormGroup;
  forgotForm: FormGroup;
  resetPass: FormGroup;
  resetUserPass: FormGroup;
  forgotUser: FormGroup;
  fcontrol: boolean = false;
  step:number = 1;
  show:boolean = false;
  code:boolean = false;
  PasswordMatch: boolean ;
  PasswordMatch1: boolean ;
  usrname : any;
  phone: any;
  uemail: any;
  sess: any;
  mfaError:boolean = false;
  userError:boolean = false;
  userError1:boolean = false;
  passConfirmationMsg:boolean = false;
  userConfirmationMsg:boolean = false;
  imagePath = '../../../assets/images/myra-logo.png';
  legalImgPath = '../../../assets/images/blue-logo.png';
  //gifpath = '../../../assets/images/marketing-gif.gif';
gifpath = '../../../assets/images/animated-video.mp4';



  constructor(private authservice: AuthenticationService,
    private fb: FormBuilder,
    private router: Router,
    private modalService: NgbModal) {
     
    if (this.authservice.isLoggedIn()) {
      this.router.navigate(["dashboard"]);
    }

    }

  ngOnInit(): void {
 
    
    this.loginForm = this.fb.group({
      username: ['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['',[Validators.required]],
      //('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/')
    })
    this.forgotUser = this.fb.group({
      uemail: ['',[Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      pnumber: ['',[Validators.required,Validators.pattern('^\\+[1-9]\\d{1,14}$')]],
      
    })
    this.codeForm = this.fb.group({
      mfcode: ['',[Validators.required,Validators.minLength(6),Validators.pattern('^[0-9]*$')]]
    })
    this.forgotForm = this.fb.group({
      uname: ['',[Validators.required]]
    })
    this.resetPass = this.fb.group({
      newpass: ['',[Validators.required,Validators.minLength(14),CustomValidators.strong]],
      cnfnewpass:['',[Validators.required]],

    })
    this.resetUserPass = this.fb.group({
      newpass: ['',[Validators.required,Validators.minLength(14),CustomValidators.strong]],
      cnfnewpass:['',[Validators.required]],
      code: ['',[Validators.required]],

    })
    if(localStorage.getItem('Attribute')){
    let userAttr = JSON.parse(localStorage.getItem('Attribute'));
    this.phone = userAttr.phone_number;
    this.uemail = userAttr.email; 
    }
    if(localStorage.getItem('Username')){
    let uname = localStorage.getItem('Username');
    this.usrname = uname;
    }
    if(localStorage.getItem('Session')){  
    let session = localStorage.getItem('Session');
    this.sess = session;
    }
    
  }

     get logF() {
        return this.loginForm.controls;
      }

     get forgetF() {
        return this.forgotForm.controls;
      }
    
     get resetF() {
        return this.resetUserPass.controls;
      }
      get forgetU() {
        return this.forgotUser.controls;
      }
     
     get PassF() {
        return this.resetPass.controls;
      }

     get oneF() {
        return this.resetPass.controls;
      }

       get oneF1() {
        return this.codeForm.controls;
      }
      

  submitLogin(loginForm){
    if (this.loginForm.invalid) {
        }else{
    const uname = this.loginForm.get("username").value;
    const pass = this.loginForm.get("password").value;
    let params = {
      'username': uname,
      'password': pass
      }
    
    this.authservice.signin(params).subscribe(res => {
      //console.log(res)
         

      if(res.statusCode == 200){
        if(res.data.hasOwnProperty('ChallengeName')){
          if(res.data.ChallengeName == 'NEW_PASSWORD_REQUIRED'){
            this.step = 4;
            localStorage.setItem(
                "Session", res.data.Session
              );
      /*localStorage.setItem(
                "Attribute", res.data.ChallengeParameters.userAttributes
              );*/
      localStorage.setItem(
                "Username", res.data.ChallengeParameters.USER_ID_FOR_SRP
              );
          }
          if(res.data.ChallengeName == 'SMS_MFA'){
           // console.log(res.data.Session)
            this.step = 5;
             localStorage.setItem(
                "Session", res.data.Session
              );
             this.sess = res.data.Session;

          }
        }
        else{
          //console.log(res.data)
        localStorage.setItem("Token",JSON.stringify(res.data ));
        const time_to_login = Date.now() + res.data.ExpiresIn*1000;
        
        localStorage.setItem('timer', JSON.stringify(time_to_login));
        localStorage.setItem("Attribute",JSON.stringify(res.data.UserAttributes ));
        this.router.navigate(["dashboard"]);

        }
      
     
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.message,
          showConfirmButton: false,
          timer: 1500
        })
       
      }
      
    });
  }
  }
  submitCode(codeForm){
    if (this.codeForm.invalid) {
      this.codeForm.controls['mfcode'].markAsTouched();

        }else{
    const uname = this.loginForm.get("username").value;
    //console.log(this.sess)
    if(localStorage.getItem('Session')){  
    let session = localStorage.getItem('Session');
    console.log(session)
    this.sess = session;
    }
    let params = {
      'username': uname,
      'mfa_code': this.codeForm.value.mfcode,
      'session': this.sess
      }
      this.authservice.mfa(params).subscribe(res => {
      //console.log(res)
      if(res.statusCode == 200){
        localStorage.setItem("Token",JSON.stringify(res.data ));
        const time_to_login = Date.now() + res.data.ExpiresIn*1000;
        localStorage.setItem('timer', JSON.stringify(time_to_login));
        localStorage.setItem("Attribute",JSON.stringify(res.data.UserAttributes ));
        this.router.navigate(["dashboard"]);
      }
    })
  }
}
  SubmitPass(resetPass){
    if (this.resetPass.invalid) {
        }else{
         // console.log(resetPass)
          if(localStorage.getItem('Username')){
          let uname = localStorage.getItem('Username');
          this.usrname = uname;
         // console.log(this.usrname)
          }
           if(localStorage.getItem('Session')){  
          let session = localStorage.getItem('Session');
          this.sess = session;
          }
          if(this.PasswordMatch == true){
            let npass=  this.resetPass.value.newpass;
              let params = {
              'username': this.usrname,
              'password': npass,
              'session': this.sess
              }
              this.authservice.resetPassword(params).subscribe(res => {
                if(res.statusCode== 200){
                  if(res.data.ChallengeName == 'SMS_MFA'){
                    this.step = 5;
                     localStorage.setItem(
                        "Session", res.data.Session
                      );
                  }
                  else{  
                 // console.log(res)
                  //this.step=1
                  const time_to_login = Date.now() + res.data.ExpiresIn*1000;
                 // console.log(time_to_login)
                  localStorage.setItem('timer', JSON.stringify(time_to_login));
                  localStorage.setItem("Token",JSON.stringify(res.data ));
                  localStorage.setItem("Attribute",JSON.stringify(res.data.UserAttributes ));
                  this.router.navigate(["dashboard"]);
                  }
                }
              })
              
          }
  
  }
}


  ResetUserPass(resetUserPass){

    if (this.resetUserPass.invalid) {
        }else{
     if(this.PasswordMatch1 == true){
      let nuser=  this.usrname;
        let params = {
        'username': nuser,
        'password': this.resetUserPass.value.newpass,
        'mfa_code': this.resetUserPass.value.code,
        }

        this.authservice.resetUserPassword(params).subscribe(res => {

          if(res.message == "Invalid Verification code"){
           this.mfaError = true;
          }
   else{
           this.mfaError = false;

        if(res.statusCode ==200){
          Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Password Successfully Changed.',
          showConfirmButton: false,
          timer: 1500
        })
          this.step=1
        }}
        })
      }
     }
  }
  CheckPassword(event) {
   // console.log('d')
    let confirmPass = event.target.value;
    let passWrd = this.resetPass.value.newpass;

    if (passWrd == confirmPass) {
      this.PasswordMatch = true;
    } else {
      this.PasswordMatch = false;
    }
  }
  CheckPassword1(event) {
//console.log('dd')
    let confirmPass = event.target.value;
    let passWrd = this.resetUserPass.value.newpass;

    if (passWrd == confirmPass) {
      this.PasswordMatch1 = true;
    } else {
      this.PasswordMatch1 = false;
    }
  }

  forgotPassword(){
    this.step = 2;
  }
  logIn(){
    this.step = 1;
    this.forgotUser.reset()
    this.forgotForm.reset()
  }

  forgotuser(){
    this.step = 6;
    this.loginForm.reset();

  }
  submitForgotUser(forgotUser){
    if(this.forgotUser.invalid){
          this.forgotUser.controls['uemail'].markAsTouched()
          this.forgotUser.controls['pnumber'].markAsTouched()
    }else{
    let u_email = this.forgotUser.value.uemail;
    let p_number = this.forgotUser.value.pnumber.toString();;
    let p ={
      'email':u_email,
      'phone_number':p_number
    }
    this.authservice.forgottonusername(p).subscribe(res=>{
      //console.log(res)
      if(res.statusCode == 200){
         this.userConfirmationMsg = true;
         this.userError1 = false;
        /*Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Username sent on email/phone number',
          showConfirmButton: false,
          timer: 1500
        })*/
         this.step = 1;
         this.forgotUser.reset()
      }
      else{
       this.userError1 = true;
         /*Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.data.Error,
          showConfirmButton: false,
          timer: 1500
        })*/
      }
    })
  }
  }
  submitForgot(forgotForm){

     if (this.forgotForm.invalid) {
      this.forgotForm.controls['uname'].markAsTouched()
        }
        else{
    const uname = this.forgotForm.get("uname").value;
    let params = {
      'username': uname
    }
    this.authservice.forgotPassword(params).subscribe(res => {

      if(res.statusCode==200){
         this.userError = false;
         this.passConfirmationMsg = true;
        this.step = 3;
        this.fcontrol= false;
        this.usrname = uname;
      }
      else{
         this.userError = true;
         this.passConfirmationMsg = false;
         /*Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.message,
          showConfirmButton: false,
          timer: 1500
        })*/
      }
    });
  }
}


showForm(content){
    if(this.authservice.isLoggedIn()){
      //console.log(this.authservice.isLoggedIn())
    }
    else{
      //console.log(this.authservice.isLoggedIn())
      this.open(content);
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
