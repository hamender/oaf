import { Directive, ElementRef } from '@angular/core';
import { faEye } from '@fortawesome/free-solid-svg-icons';

@Directive({
  selector: '[appAppPassword]'
})
export class AppPasswordDirective {
 private _shown = false;
 faEye = faEye;
  constructor(private el: ElementRef) { 
this.setup();
  }
toggle(span: HTMLElement) {
    this._shown = !this._shown;
    if (this._shown) {
      this.el.nativeElement.setAttribute('type', 'text');
      span.innerHTML = '<span class=""><fa-icon [icon]="faEye"></fa-icon></span>';
    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      span.innerHTML = '<span class=""><fa-icon [icon]="faEye"></fa-icon></span>';
    }
  }
setup() {
    const parent = this.el.nativeElement.parentNode;
    const span = document.createElement('span');
    span.innerHTML = `<span class=""><fa-icon [icon]="faEye"></fa-icon></span>`;
    span.addEventListener('click', (event) => {
      this.toggle(span);
    });
    parent.appendChild(span);
  }
}
