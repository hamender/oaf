import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerserviceService {

  url: string;
  IDToken: any;
  IDToken1: any;
  AcToken: any;
  constructor (private http: HttpClient,
    private auth: AuthenticationService) { 
    this.url = environment.url;
    this.IDToken = this.auth.getToken();
    this.IDToken1 = this.auth.getToken1();
    this.AcToken = this.auth.getAccToken();
  }

    create(data: any): Observable<any> {
     
      let idt = this.auth.getToken1();
      if(idt){
      return this.http.post<Blob>(this.url+'service', data, { 
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            
            'Accept': '*/*',

          }),
          
        });
      }
    }
    list():Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
      return this.http.get<Blob>(this.url+'service', { //  <-----  notice the null  *****
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            
            'Accept': '*/*',
          }),
          
        });
      }

  }

    update(params: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
            
            'Accept': '*/*',
              });

     return this.http.put(this.url+'service', params, { headers: finalHeaders});  
        
        
      }
  }
getservicebyID(params: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization':idt,
            
            'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'service',
          { headers: finalHeaders, params }
        );
      }
  }

    getServiceListbyID(params: any): Observable<any> {
     let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
            
            'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'service/list',
          { headers: finalHeaders, params }
        );
      }
  }


    deleteService(data: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        const options = {
        headers: new HttpHeaders({
        'Authorization': idt,
            
            'Accept': '*/*',
        }),
        body: data,
        }
        return this.http.delete( this.url+'service', options);
      }
  }
}
