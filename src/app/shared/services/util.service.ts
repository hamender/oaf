import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
	private breadcrumbSource = new BehaviorSubject([]);
  currentBreadcrumb = this.breadcrumbSource.asObservable();
 
   private messageSource = new BehaviorSubject('false');
  	currentMessage = this.messageSource.asObservable();

  constructor() {


   }

   changeBreadcrumb(breadcrumb: Breadcrump[]) {
    this.breadcrumbSource.next(breadcrumb);
  }
  
  changeMessage(message: string) {
    this.messageSource.next(message)
  }
}
export class Breadcrump {
  name?: string;
  link?: string;
}