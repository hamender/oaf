  import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  url: string;
  IDToken: any;
  IDToken1: any;
  AcToken: any;
  constructor(private http: HttpClient,
  	private auth: AuthenticationService) {
    this.url = environment.url;
    this.IDToken = this.auth.getToken();
    this.IDToken1 = this.auth.getToken1();
    this.AcToken = this.auth.getAccToken();
	}

	create(data: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
      
      return this.http.post(this.url+'resource', data, { //  <-----  notice the null  *****
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            
            'Accept': '*/*',

          }),
          
        });
      }
  	}

  	update(data: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
      return this.http.put<Blob>(this.url+'resource', data, { //  <-----  notice the null  *****
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            
            'Accept': '*/*',

          }),
          
        });
      }
  	}

  /*	deleteResource(params: any):Observable<any> {
  	return this.http.delete(this.url+'resource/', params)
  	}*/
    deleteResource(data: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        const options = {
        headers: new HttpHeaders({
            'Authorization': idt,
            
            'Accept': '*/*',
        }),
        body: data,
        }
        return this.http.delete( this.url+'resource', options);
      }
  }
  	

  	getResource(params: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'resource',
          { headers: finalHeaders, params }
        );
      }
  	}

  	list(params: any): Observable<any> {
      let idt = this.auth.getToken1();
      if( idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'resource/list',
          { headers: finalHeaders, params }
        );
      }
  	}

    getHistorybyID(params: any): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'provisionhistory/byresourceid',
          { headers: finalHeaders, params }
        );
      }
    }
}
