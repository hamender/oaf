import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  url: string;
  IDToken: any;
  IDToken1: any;
  AcToken: any;
  constructor (private http: HttpClient,
  	private auth:AuthenticationService) { 
    this.url = environment.url;
    this.IDToken = this.auth.getToken();
    this.IDToken1 = this.auth.getToken1();
    this.AcToken = this.auth.getAccToken();
	}

	create(data: any): Observable<any> {

      //let token = this.auth.getToken();
 let idt = this.auth.getToken1();
      if(idt){
      return this.http.post<Blob>(this.url+'user', data, { 
      	headers: new HttpHeaders(
          {
            'Authorization': idt,
            
            'Accept': '*/*',

          }),
          
        });
      }
  }
 
  list():Observable<any> {
    let idt = this.auth.getToken1();
      if(idt){
      return this.http.get<Blob>(this.url+'user/list', {
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            'Accept': '*/*',
          }),  
        });
      }
  }

  getUserbyname(params: any): Observable<any> {
     let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'user',
          { headers: finalHeaders, params }
        );
      }
  }
  updateUser(params: any): Observable<any> {
       let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });

     return this.http.put(this.url+'user', params, { headers: finalHeaders});  
        
        
      }
  }
  updateCurrentUser(params: any): Observable<any> {
       let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });

     return this.http.put(this.url+'currentuser', params, { headers: finalHeaders});  
        
        
      }
  }
  getCurrentUser(): Observable<any> {
    let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'currentuser',
          { headers: finalHeaders }
        );
      }
  }
  deleteUser(data: any): Observable<any> {
     let idt = this.auth.getToken1();
      if(idt){
        const options = {
        headers: new HttpHeaders({
        'Authorization': idt,
        'Accept': '*/*',
        }),
        body: data,
        }
        return this.http.delete( this.url+'user', options);
      }
  }
  
 
     loginHistory(): Observable<any> {
      let idt = this.auth.getToken1();
      if(idt){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': idt,
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'loginhistory',
          { headers: finalHeaders }
        );
      }
  }
  changepassword(params: any): Observable<any>{
        let idt = this.auth.getToken1();
      if(idt){
      return this.http.post<Blob>(this.url+'changepassword', params, { 
        headers: new HttpHeaders(
          {
            'Authorization': idt,
            'Accept': '*/*',

          }),
          
        });
      }
    
  }

 
}
