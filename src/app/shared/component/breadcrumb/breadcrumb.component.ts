import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/util.service';
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

   breadcrumb = [];
     message:string;

  constructor(private utilService: UtilService) { }

  ngOnInit() {
    this.utilService.currentBreadcrumb
      .subscribe(breadcrumb => {
        this.breadcrumb = breadcrumb;
      });
  }

  goback() {
    this.utilService.changeMessage('table1')
  }
 
}
