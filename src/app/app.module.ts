import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './shared/guard/guard.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './home/login/login.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { AddPlatformComponent } from './pages/platform/add-platform/add-platform.component';
import { EditPlatformComponent } from './pages/platform/edit-platform/edit-platform.component';
import { AddServiceComponent } from './pages/services/add-service/add-service.component';
import { EditServiceComponent } from './pages/services/edit-service/edit-service.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { AddCustomerComponent } from './pages/customer/add-customer/add-customer.component';
import { AddProvisionComponent } from './pages/provision/add-provision/add-provision.component';
import { PlateformComponent } from './pages/platform/plateform/plateform.component';
import { AddInstanceComponent } from './pages/instance/add-instance/add-instance.component';
import { AddResourceComponent } from './pages/resource/add-resource/add-resource.component';
import { ManageUserComponent } from './pages/manage-user/manage-user.component';
//import { ToastrModule } from 'ngx-toastr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditUserComponent } from './pages/manage/edit-user/edit-user.component';
import { BreadcrumbComponent } from './shared/component/breadcrumb/breadcrumb.component';
import { UserIdleModule } from 'angular-user-idle';
import { AppPasswordDirective } from './shared/directive/app-password.directive';
import { DatePipe } from '@angular/common';
import { ShContextMenuModule } from 'ng2-right-click-menu';
import { EditProvisionComponent } from './pages/provision/edit-provision/edit-provision.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AddConnectorComponent } from './pages/connector/add-connector/add-connector.component';
import { ManageConnectorComponent } from './pages/connector/manage-connector/manage-connector.component';
import { EditConnectorComponent } from './pages/connector/edit-connector/edit-connector.component';
import { ApiDocumentationComponent } from './pages/api-documentation/api-documentation.component';
import { ConnectorDocComponent } from './pages/api-documentation/connector-doc/connector-doc.component';
import { PlatformDocComponent } from './pages/api-documentation/platform-doc/platform-doc.component';
import { ResourceDocComponent } from './pages/api-documentation/resource-doc/resource-doc.component';
import { ServiceDocComponent } from './pages/api-documentation/service-doc/service-doc.component';
import { InstanceDocComponent } from './pages/api-documentation/instance-doc/instance-doc.component';
import { ViewHistoryComponent } from './home/view-history/view-history.component';
import { PopupHistoryComponent } from './home/popup-history/popup-history.component';
import { UserGuideComponent } from './pages/user-guide/user-guide.component';
import { ManageOrganisationComponent } from './pages/organisation/manage-organisation/manage-organisation.component';
import { ChartComponent } from './home/chart/chart.component';
/*import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';*/
import { GoogleChartsModule } from 'angular-google-charts';
//import {NgxPaginationModule} from 'ngx-pagination';
//import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    AddPlatformComponent,
    EditPlatformComponent,
    AddServiceComponent,
    EditServiceComponent,
    HeaderComponent,
    FooterComponent,
    EditProfileComponent,
    AddCustomerComponent,
    AddProvisionComponent,
    PlateformComponent,
    AddInstanceComponent,
    AddResourceComponent,
    ManageUserComponent,
    EditUserComponent,
    BreadcrumbComponent,
    AppPasswordDirective,
    EditProvisionComponent,
    AddConnectorComponent,
    ManageConnectorComponent,
    EditConnectorComponent,
    ApiDocumentationComponent,
    ConnectorDocComponent,
    PlatformDocComponent,
    ResourceDocComponent,
    ServiceDocComponent,
    InstanceDocComponent,
    ViewHistoryComponent,
    PopupHistoryComponent,
    UserGuideComponent,
    ManageOrganisationComponent,
    ChartComponent,
    
 
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FontAwesomeModule,
    //ToastrModule.forRoot() // ToastrModule added, 
    BrowserAnimationsModule,
    ShContextMenuModule,
    UserIdleModule.forRoot({idle: 3600, timeout: 1800, ping: 870}),
    DataTablesModule,
     NgMultiSelectDropDownModule.forRoot(),
     GoogleChartsModule,
     CarouselModule ,
     //NgxPaginationModule,
     //Ng2SearchPipeModule
    /* DropDownListModule,
     ButtonModule*/
  ],
  providers: [
  AuthGuard,DatePipe,

 ],
  bootstrap: [AppComponent]
})
export class AppModule { }