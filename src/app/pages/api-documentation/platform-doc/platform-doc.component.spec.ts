import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformDocComponent } from './platform-doc.component';

describe('PlatformDocComponent', () => {
  let component: PlatformDocComponent;
  let fixture: ComponentFixture<PlatformDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
