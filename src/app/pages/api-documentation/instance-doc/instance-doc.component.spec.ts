import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanceDocComponent } from './instance-doc.component';

describe('InstanceDocComponent', () => {
  let component: InstanceDocComponent;
  let fixture: ComponentFixture<InstanceDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstanceDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstanceDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
