import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
​import { faArrowRight } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-instance-doc',
  templateUrl: './instance-doc.component.html',
  styleUrls: ['./instance-doc.component.scss']
})
export class InstanceDocComponent implements OnInit {
  getF: FormGroup;
  delIn: FormGroup;
  PostInst: FormGroup;
  PutInst: FormGroup;
faArrowRight = faArrowRight;
  
  getInstance: boolean = false;
  createInstance: boolean =false;
  delInstance: boolean = false;
  updateIns: boolean =false;
  getInt: boolean = false;
  DInst: boolean = false;
  crInstance: boolean = false;
  upInstance: boolean = false;
  GetI:any;
  GetI1: any;
  dInst: any;
  dInst1: any;
  CRIns: any;
  CRIns1: any;
  UPIns:any;
  UPIns1:any;
  constructor( private fb: FormBuilder) { }

  ngOnInit(): void {
     this.getF = this.fb.group({
      plat: ['', [Validators.required]],
      sev: ['', [Validators.required]],
      sev_id: ['', [Validators.required]],
    });

     this.delIn = this.fb.group({
      plat: ['', [Validators.required]],
      sev: ['', [Validators.required]],
      plat_id: ['', [Validators.required]],
      sev_id: ['',[Validators.required]]
    });
      this.PostInst = this.fb.group({
      plat: ['', [Validators.required]],
      sev: ['', [Validators.required]],
      payload: ['', [Validators.required]],
    });
        this.PutInst = this.fb.group({
      plat: ['', [Validators.required]],
      sev: ['', [Validators.required]],
      payload: ['', [Validators.required]],
    });
  }

get submitG() {
    return this.getF.controls;
  }

get DelI(){
  return this.delIn.controls;
}

get PostI(){
  return this.PostInst.controls;
}

get PutI(){
  return this.PutInst.controls;
}
  resourceclick(event){
 if(event== 1){
    this.getInstance = true;
  }
  if(event== 2){
    this.createInstance = true;
  }
  if(event== 3){
    this.delInstance = true;
  }
  if(event== 4){
    this.updateIns = true;
  }
}

cancelclick(event){
	if(event== 1){
  this.getInstance = false;
  }
  if(event== 2){
    this.createInstance = false;
  }
  if(event== 3){
    this.delInstance =false;
  }
  if(event== 4){
    this.updateIns = false;
  }

}

/*------------Sumit Forms----------*/

SubmitInstance(getF){
   if (this.getF.invalid) {
      this.getF.controls['plat'].markAsTouched();
      this.getF.controls['sev'].markAsTouched();
      this.getF.controls['sev_id'].markAsTouched();


      this.getInt= false;
        }else{
    this.getInt = true;
    var u1 = ''
  var u2 = ''
  var u3 = ''

    if(this.getF.value){ 
    u1 = 'plat='+this.getF.value.plat
    u2 = '&sev='+this.getF.value.sev
    u3 = '&sev_id='+this.getF.value.sev_id

    }
    this.GetI = 'curl -X GET "https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+' -H "accept: application/json"'
    this.GetI1 = 'https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+''
  }

}
CreInstance(PostInst){
      if (this.PostInst.invalid) {
      this.PostInst.controls['plat'].markAsTouched();
      this.PostInst.controls['sev'].markAsTouched();
      this.PostInst.controls['payload'].markAsTouched();
      this.crInstance= false;
        }else{
    this.crInstance = true;
    var u1 = ''
  var u2 = ''
  var u3 = ''

    if(this.PostInst.value){ 
    u1 = 'plat='+this.PostInst.value.plat
    u2 = '&sev='+this.PostInst.value.sev
    u3 = '&payload='+this.PostInst.value.payload
    }
    this.CRIns = 'curl -X GET "https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+' -H "accept: application/json"'
    this.CRIns1 = 'https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+''
  }
}

DeleteInstance(delIn){
  if (this.delIn.invalid) {
      this.delIn.controls['plat'].markAsTouched();
      this.delIn.controls['sev'].markAsTouched();
      this.delIn.controls['plat_id'].markAsTouched();
      this.delIn.controls['sev_id'].markAsTouched();

      this.DInst= false;
        }else{
    this.DInst = true;
    var u1 = ''
  var u2 = ''
  var u3 = ''
  var u4 = ''

    if(this.delIn.value){ 
    u1 = 'plat='+this.delIn.value.plat
    u2 = '&sev='+this.delIn.value.sev
    u3 = '&plat_id='+this.delIn.value.plat_id
    u4 = '&sev_id='+this.delIn.value.sev_id

    }
    this.dInst = 'curl -X GET "https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+''+u4+' -H "accept: application/json"'
    this.dInst1 = 'https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+''+u4+''
  }
}
UpdInstance(PutInst){
  if (this.PutInst.invalid) {
      this.PutInst.controls['plat'].markAsTouched();
      this.PutInst.controls['sev'].markAsTouched();
      this.PutInst.controls['payload'].markAsTouched();
      this.upInstance= false;
        }else{
    this.upInstance = true;
    var u1 = ''
  var u2 = ''
  var u3 = ''

    if(this.PutInst.value){ 
    u1 = 'plat='+this.PutInst.value.plat
    u2 = '&sev='+this.PutInst.value.sev
    u3 = '&payload='+this.PutInst.value.payload
    }
    this.UPIns = 'curl -X GET "https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+' -H "accept: application/json"'
    this.UPIns1 = 'https://api.oaf-x.com/user/?'+u1+''+u2+''+u3+''
  }
}

}
