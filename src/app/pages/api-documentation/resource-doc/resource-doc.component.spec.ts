import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceDocComponent } from './resource-doc.component';

describe('ResourceDocComponent', () => {
  let component: ResourceDocComponent;
  let fixture: ComponentFixture<ResourceDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourceDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
