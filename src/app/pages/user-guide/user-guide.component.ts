import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { UtilService } from '../../shared/services/util.service';
import { Router } from '@angular/router';
import { NgbPanelChangeEvent, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
​import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-user-guide',
  templateUrl: './user-guide.component.html',
  styleUrls: ['./user-guide.component.scss']
})
export class UserGuideComponent implements OnInit {
faArrowRight = faArrowRight;
    @ViewChild('acc') accordion: NgbAccordion;
  model = {};
  currentStep = 1;
  step:Number;
  constructor(public utilService: UtilService,
  	private router: Router,
    public myElement: ElementRef) {
  	 const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'User Guide' }]
    this.utilService.changeBreadcrumb(breadcrumb);
     }
​
  ngOnInit(): void {
  }
  goToLink(val){
    console.log(val)
    $(document).ready(function(){
      $('.umanage button').click();

    })
}
 /*    clickNum(step) {
      console.log(step)
    this.currentStep = this.currentStep < step ? step : this.currentStep;
    setTimeout(() => this.accordion.toggle('toggle-' + step), 0);

  }*/
  
/* beforeChange($event: NgbPanelChangeEvent) {
    const availStep = 'toggle-' + this.currentStep;
    if ($event.panelId > availStep) {
      $event.preventDefault();
    }
  }; */

  }
