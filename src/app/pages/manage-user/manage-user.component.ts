import { UtilService } from '../../shared/services/util.service';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray} from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { EditUserComponent } from '../../pages/manage/edit-user/edit-user.component'
import Swal from 'sweetalert2';
import { CustomerService } from '../../shared/services/customer.service';
import { AddCustomerComponent } from '../../pages/customer/add-customer/add-customer.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router,NavigationEnd } from '@angular/router';
import { trigger, transition, style, animate, query, stagger, state, keyframes } from "@angular/animations";
import { faEye, faPencilAlt, faPlus} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss'],
  animations: [
    trigger("listAnimation1", [
      transition("* => *", [
        // each time the binding value changes
        query(
          ":leave",
          [stagger(100, [animate("0.5s", style({ opacity: 0 }))])],
          { optional: true }
        ),
        query(
          ":enter",
          [
            style({ opacity: 0 }),
            stagger(100, [animate("0.5s", style({ opacity: 1 }))])
          ],
          { optional: true }
        )
      ])
    ]),
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1, 'overflow-x': 'hidden'}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    ),
    trigger('slideIn', [
      state('*', style({ 'overflow-y': 'hidden' })),
      state('void', style({ 'overflow-y': 'hidden' })),
      transition('* => void', [
        style({ height: '*' }),
        animate(250, style({ height: 0 }))
      ]),
      transition('void => *', [
        style({ height: '0' }),
        animate(250, style({ height: '*' }))
      ])
    ])
  ]
})
export class ManageUserComponent implements OnInit  {
  userList: any;
   faEye = faEye;
   faPlus = faPlus;
  faPencilAlt = faPencilAlt;
  showload:boolean= true;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  show:boolean = false;
  loadergif = '../../../assets/images/turn.gif'
   checkboxes: boolean[];
   selectedRow: Number;
   groups: any;
   reqArray = [];
   check: any;
  constructor(private router: Router,
    private modalService: NgbModal,
    private customerservice: CustomerService,
    public utilService: UtilService,
   ) { 

    const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'Manage User' }]
    this.utilService.changeBreadcrumb(breadcrumb);

        this.router.events.subscribe((e) => {
       if (e instanceof NavigationEnd) {
          
       }
    });
      
    
  }
   ngOnInit() {
   
    setTimeout(() =>{
      this.getList();
    },2000)
      
        this.dtOptions = {
        initComplete: function(settings, json) {
          $('body').find('.dataTables_scroll').css('border-bottom', '0px');
          $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
          $('body').find('.dataTables_filter input').css({
            "border-width": "0px",
            "border-bottom": "1px solid #b1b8bb",
            "outline": "none",
            "width": "150px",
            "margin-bottom": "0px",
            "margin-right": "0px !important",
            "margin-left": "0px !important"
        } )
          },
          scrollCollapse: true,
          paging:true,
          select:true,
          bFilter: true, 
          bInfo: true,
          ordering: true,
          lengthMenu: [10, 25, 50,100,500,1000,2000],
          
      }
    }
 

   
    viewUser(index){

    const ngmodalRef = this.modalService.open(EditUserComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef.componentInstance.viewId = index;
    /*ngmodalRef.componentInstance.passEntry3.subscribe((receivedEntry) => {
    console.log(receivedEntry);
    })*/
    }
    editUser(index){
      const ngmodalRef = this.modalService.open(EditUserComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef.componentInstance.username = index;
    ngmodalRef.componentInstance.passEntry4.subscribe((rdata) => {
      if(rdata.statusCode == 200){
        this.getList('noUpdate');
      }
    })
    ngmodalRef.componentInstance.dEntry.subscribe((rdata1) => {
      if(rdata1.statusCode == 200){
        this.getList('noUpdate');
      }
    })
    }
   
    addCustomer(){
      const ngmodalRef = this.modalService.open(AddCustomerComponent, {
      size: 'lg',
      backdrop: 'static'
    });
      ngmodalRef.componentInstance.aCustomer.subscribe((rdata) => {

  if(rdata.statusCode == 200){
    
     this.getList('noUpdate');
  }
  })
    //this.router.navigate(['//add-customer'])
  }


    /*ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }*/
 getList(noUpdate?){
      this.customerservice.list().subscribe(res=>{
        console.log(res)
        this.showload = false
        if(res.statusCode == 200){
         
          this.userList = res.data.Users
        this.show = true
         this.checkboxes = new Array(this.userList.length);
    this.checkboxes.fill(false);
         if (noUpdate) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
          
        })
      } 
      else {
        this.dtTrigger.next();
      }
         
        }
      })
    }

/*  bulkdelUser(){
    var allSelected = this.checkboxes.every(checkbox => checkbox === true);
console.log(allSelected)
    if (allSelected) {
      alert("At least one row should be present.")
      return;
    }
    for (let i = this.checkboxes.length-1; i >= 0; i--) {
      // If selected, then delete that row.
      if (this.checkboxes[i]) {
        console.log(this.checkboxes[i])
        this.groups.splice(i, 1);
      }
    }
    // Remove entries from checkboxes array.
    this.checkboxes = this.checkboxes.filter(checkbox => checkbox === false);
  }*/
/*bulkdelUser(){
 this.check=  this.reqArray;
 console.log(this.check[0])
 for(let i=0; i>=this.check[i].length; i++){
  console.log(true)
  console.log(this.check)
  console.log(this.check[i])
 }
}
  
   multipleDel(username, i){
    console.log(username)
    console.log(i)
    this.reqArray.push({username})
    console.log(this.reqArray)
   }*/

}
