import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Input, Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { CustomerService } from '../../../shared/services/customer.service';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  userForm: FormGroup
  @Input() username: any;
  @Input() viewId: any;

  @Output() passEntry4: EventEmitter<any> = new EventEmitter();
  @Output() dEntry: EventEmitter<any> = new EventEmitter();
  showload:boolean= true;
  loadergif = '../../../assets/images/turn.gif'
  userData: any;
  readonly: boolean =false;
  delbtnDisable: boolean =true;
  userRoles=[];
  parm={};
  uname: any;
  constructor(public activeModal: NgbActiveModal,
          private fb: FormBuilder,
          private customerservice: CustomerService,
          public auth: AuthenticationService) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      userName: [{value:'',disabled:true},[Validators.required]],
      name: ['',[Validators.required]],
      userEmail: [{value:'',disabled:true},[Validators.required,Validators.email]],
      userPhone: ['',[Validators.required,Validators.pattern('^\\+[1-9]\\d{1,14}$')]],
      status: ['',[Validators.required]],
      type: ['',[Validators.required]],
      callurl: [''],
    });

    if (this.auth.isAdmin()) {
      this.userRoles = [{ name: 'Super-Admin', value: 'Super-Admin' },
                        { name: 'Administrator', value: 'Administrator' },
                        { name: 'Standard', value: 'Standard' }]
    } else {
      this.userRoles = [{ name: 'Administrator', value: 'Administrator' },
                        { name: 'Standard', value: 'Standard' }];
    }

    if(this.viewId){
      this.getUserDetail(this.viewId)
      this.readonly= true;
    }
    if(this.username){
       this.getUserDetail(this.username)
    }
    
  }

  closeform(){
      this.activeModal.close();
  }

  get userFormF() { 
    return this.userForm.controls; 
  }
  submitForm(userForm){
    console.log(userForm)
    let arr=[
    {'Name':'phone_number','Value':this.userForm.value.userPhone},
    {'Name':'callback_url','Value':this.userForm.value.callurl},
    {'Name':'name','Value':this.userForm.value.name}
    ];

    this.parm['username']= this.uname;
    this.parm['user_group']= this.userForm.value.type;
    this.parm['attributes']= arr;

    console.log(this.parm)
 
    this.customerservice.updateUser(this.parm).subscribe(res => {
      console.log(res)
      if(res.statusCode == 200){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Updated!!',
          showConfirmButton: false,
          timer: 1500
        })
        this.activeModal.close();

      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.data.Errors,
          showConfirmButton: false,
          timer: 1500
        })
      }
        this.passEntry4.emit(res);
    })
  }
  updateForm(data){
    console.log(data)
    this.uname = data.username;
     this.userForm.patchValue({userName: data.username})
     this.userForm.patchValue({name: data.name})
     this.userForm.patchValue({userEmail: data.email})
     this.userForm.patchValue({userPhone: data.phone_number})
     this.userForm.patchValue({status: data.status})
     this.userForm.patchValue({type: data.groups[0] ? data.groups[0] : ""})
     this.userForm.patchValue({callurl: data.callback_url ? data.callback_url : ""})
     this.delbtnDisable = false;
  }
  getUserDetail(val){
    let p={
      'username':val
    }
    this.customerservice.getUserbyname(p).subscribe(res=>{
      this.showload = false
        if(res.statusCode==200){
          this.userData = res.data.UserProfile;
          this.updateForm(this.userData)
        }  
      })
  }
  delUser(uname){
    console.log(uname)
    let p = {
      'username':uname
    }
    Swal.fire({
  title: 'Are you sure you want to delete?',
  text: "This action cannot be undone",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#0181AC',
  cancelButtonColor: '#d5d5d5',
  confirmButtonText: 'Delete'
}).then((result) => {
  if (result.value) {
    this.customerservice.deleteUser(p).subscribe(res=> {
      if(res.statusCode == 200){  
      Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'User Deleted!!',
          showConfirmButton: false,
          timer: 1500
        })
      this.activeModal.close();
        this.dEntry.emit(res);
      }
    })
    
  }
})
  }

}
