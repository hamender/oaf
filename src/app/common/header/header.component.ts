import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from "@angular/router";
import { UserIdleService } from 'angular-user-idle';
import { CustomerService } from '../../shared/services/customer.service'
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
//import { ViewHistoryComponent } from '../../home/view-history/view-history.component';
import { PopupHistoryComponent } from '../../home/popup-history/popup-history.component';
import { ProvisionService } from '../../shared/services/provision.service';
import { faEllipsisV, faBell } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  uName: any;
  Name: any;
  faEllipsisV = faEllipsisV;
  faBell = faBell;
  texttt: boolean = false;
  isAdmin:boolean;
  imagePath = '../../../assets/images/myra-logo.png';
  globalName: boolean = true;
  globall : any;
  showbell: boolean = false;
 // reqArray = []
  reqArray: Array<any> = []
  searchResult: Array<any> = [];
  searchInput: String = '';
  content:any[]=new Array();
  counter:number;
  constructor(public authservice: AuthenticationService,
    //private activeModal: NgbActiveModal,
    private router: Router,
    private userIdle: UserIdleService,
    public customerservice: CustomerService,
    private modalService: NgbModal,
    private proservice: ProvisionService
    ) {
    let p ={
      'records':'all'
    }
     this.counter=0;
    this.proservice.provisionlist(p).subscribe(res => {
      var i = res.Records.length+1;
      for(let items of res.Records){
        i--
        var rid = 'Request Id';
        this.reqArray.push({id:items[rid],len:i})
        this.searchResult = this.reqArray
      }
 
     // console.log(this.reqArray)
    });

    if(localStorage.getItem('Attribute')){
      let attribute = JSON.parse(localStorage.getItem('Attribute'));
     //console.log(attribute)
     this.globall = attribute.groups[0];
     this.Name = attribute.name;
      if(this.globall == 'Administrator'){
       this.globalName = false;
          }
          else if(this.globall == 'Standard'){
             this.globalName = false;
          }
          else{
             this.globalName = true;

          }
        }
    }

     



  ngOnInit(): void {
    
    
      this.userIdle.startWatching();
    this.logoutAcc();

    this.userIdle.onTimerStart().subscribe(count => console.log(count));
    
    this.userIdle.onTimeout().subscribe(() => console.log('Time is up!'));
    if(this.authservice.isAdmin()){
      this.isAdmin = true
    }
    else{
      this.isAdmin = false
    }

  }

/*  getData(){
    console.log(this.counter + 'dat size'+this.reqArray.length)

    for(let i=this.counter+1;i<this.reqArray.length;i++)
    {
    this.content.push(this.reqArray[i]);
    if(i%10==0) break;
    }
    this.counter+=10;

  }*/

  

  fetchSeries(value){
    if(value === '') {
      return this.searchResult = []
    }
    this.searchResult = this.reqArray.filter(function(series) {
      return series.id.toLowerCase().startsWith(value.target.value.toLowerCase())
    })
}

  getSingleHistroynyID(id){
    //console.log(id)
    const ngmodalRef = this.modalService.open(PopupHistoryComponent, {
            size: 'lg',
            backdrop: 'static',
            windowClass: 'customhistory'
          });
          ngmodalRef.componentInstance.id = id;
          ngmodalRef.componentInstance.loc = 'header';
   
  }
  logout(){

     
  let params = {"username": this.uName}
     localStorage.clear();
      localStorage.removeItem('timer');
      localStorage.removeItem('ACCESS_TOKEN');
      localStorage.removeItem('Attribute');
      localStorage.removeItem('Token');
    this.authservice.logout(params).subscribe(res=>{
      //console.log(res)

     this.router.navigate(['/']);
/*     if(res.errorMessage  == "'username'"){
      console.log(true)
     setInterval(function(){
      $('.close').trigger('click');

   }, 1000)
   }else{
    console.log(false)
   }*/
    })
    
  }
   logoutAcc(){
 
    this.userIdle.onTimerStart().subscribe(count => {
           //console.log('no')
            if(count == 45){
             // console.log('yes')
              this.logout();
            }
          });
  }
   /*closeform(){   
    this.activeModal.close();
  }*/
  mUser(){
    this.router.navigate(['//manage-user'])
  }
  
  viewMore(){
     this.router.navigate(['//view-history'])
  }

  mConnector(){
    
    this.router.navigate(['//manage-connector'])
  }
  
   editprofile(){
    this.router.navigate(['//manage-profile'])
  }
  
  LogoClick(){
    this.router.navigate(['/'])
  }

  userGuide(){
    this.router.navigate(['//user-guide'])
  }
  apiDoc(){
    this.router.navigate(['//api-documentation'])
  }

  mOrgan(){
    this.router.navigate(['//manage-organisation'])
  }

}
