import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,TemplateRef  } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { AuthenticationService } from '../../shared/services/authentication.service'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
	href = 'https://app.oaf-x.com';
	closeResult: string;
	imagePath = '../../../assets/images/blue-logo.png';
	
  constructor(private modalService: NgbModal,
  	public auth: AuthenticationService) { }

  ngOnInit(): void {
  }

 showForm(content){
    if(this.auth.isLoggedIn()){
      console.log(this.auth.isLoggedIn())
      this.open(content);
    }
    else{
      console.log(this.auth.isLoggedIn())
      
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
