import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let CustomerService = /** @class */ (() => {
    let CustomerService = class CustomerService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            //let token = this.auth.getToken();
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'user', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'user/list', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        getUserbyname(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'user', { headers: finalHeaders, params });
            }
        }
        updateUser(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'user', params, { headers: finalHeaders });
            }
        }
        updateCurrentUser(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'currentuser', params, { headers: finalHeaders });
            }
        }
        getCurrentUser() {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'currentuser', { headers: finalHeaders });
            }
        }
        deleteUser(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'user', options);
            }
        }
        loginHistory() {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'loginhistory', { headers: finalHeaders });
            }
        }
        changepassword(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'changepassword', params, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
    };
    CustomerService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CustomerService);
    return CustomerService;
})();
export { CustomerService };
//# sourceMappingURL=customer.service.js.map