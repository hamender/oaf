import { __decorate } from "tslib";
import { Injectable } from "@angular/core";
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
let AppHttpInterceptor = /** @class */ (() => {
    let AppHttpInterceptor = class AppHttpInterceptor {
        constructor(router, http) {
            this.router = router;
            this.http = http;
            this.headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Token': localStorage.getItem("Token")
            });
        }
        intercept(req, next) {
            console.log("intercepted request ... ");
            // Clone the request to add the new header.
            const authReq = req.clone({ headers: req.headers.set("Token", localStorage.getItem("Token")) });
            console.log("Sending request with new header now ...");
            //send the newly created request
            return next.handle(authReq)
                .catch(err => {
                // onError
                console.log(err);
                if (err instanceof HttpErrorResponse) {
                    console.log(err.status);
                    console.log(err.statusText);
                    if (err.status === 0) {
                        localStorage.clear();
                        window.location.href = "/login";
                    }
                }
                return Observable.throw(err);
            });
        }
    };
    AppHttpInterceptor = __decorate([
        Injectable()
    ], AppHttpInterceptor);
    return AppHttpInterceptor;
})();
export { AppHttpInterceptor };
//# sourceMappingURL=AppHttpInterceptor.js.map