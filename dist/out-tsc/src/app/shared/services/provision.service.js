import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ProvisionService = /** @class */ (() => {
    let ProvisionService = class ProvisionService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'provision', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        getProvision(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provision', { headers: finalHeaders, params });
            }
        }
        deleteProvision(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'provision', options);
            }
        }
        getProvisionList(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provision/list', { headers: finalHeaders, params });
            }
        }
        update(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'provision', params, { headers: finalHeaders });
            }
        }
        provisionlist(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provisionhistory/list', { headers: finalHeaders, params });
            }
        }
        getSingleHistory(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provisionhistory', { headers: finalHeaders, params });
            }
        }
        getHistorybyID(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provisionhistory/byinstanceid', { headers: finalHeaders, params });
            }
        }
    };
    ProvisionService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ProvisionService);
    return ProvisionService;
})();
export { ProvisionService };
//# sourceMappingURL=provision.service.js.map