import { __decorate, __param } from "tslib";
import { Injectable, Inject } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
let AuthenticationService = /** @class */ (() => {
    let AuthenticationService = class AuthenticationService {
        constructor(document, http) {
            this.document = document;
            this.http = http;
            this.url = environment.url;
        }
        signin(user) {
            return this.http.post(this.url + 'signin', user).pipe(map((auth) => {
                localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
                return auth;
            }));
        }
        getHostname() {
            return this.document.location.hostname;
        }
        isAdmin() {
            if (localStorage.getItem('ACCESS_TOKEN')) {
                let Attribute = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
                let grp = Attribute.UserAttributes.groups;
                if (grp.includes('Super-Admin')) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        isLoggedIn() {
            if (localStorage.getItem('Token')) {
                const token = localStorage.getItem('Token');
                return token ? true : false;
            }
            else {
                return false;
            }
        }
        /* public logout() {
           localStorage.clear();
         }*/
        logout(params) {
            return this.http.post(this.url + 'signout', params);
        }
        resetPassword(params) {
            return this.http.post(this.url + 'resetpassword', params).pipe(map((auth) => {
                localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
                return auth;
            }));
        }
        resetUserPassword(params) {
            return this.http.put(this.url + 'resetpassword', params);
        }
        forgotPassword(param) {
            return this.http.post(this.url + 'forgotpassword', param);
        }
        forgottonusername(param) {
            return this.http.post(this.url + 'forgotusername', param);
        }
        mfa(params) {
            return this.http.post(this.url + 'mfa', params).pipe(map((auth) => {
                localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
                return auth;
            }));
        }
        getToken() {
            if (localStorage.getItem('Token')) {
                const token = JSON.parse(localStorage.getItem('Token'));
                return token.IdToken;
            }
            else {
                return false;
            }
        }
        getToken1() {
            if (localStorage.getItem('ACCESS_TOKEN')) {
                const token1 = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
                return token1.IdToken;
            }
            else {
                return false;
            }
        }
        getAccToken() {
            if (localStorage.getItem('Token')) {
                const token = JSON.parse(localStorage.getItem('Token'));
                return token.AccessToken;
            }
            else {
                return false;
            }
        }
    };
    AuthenticationService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(DOCUMENT))
    ], AuthenticationService);
    return AuthenticationService;
})();
export { AuthenticationService };
//# sourceMappingURL=authentication.service.js.map