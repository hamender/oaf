import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let CustomerserviceService = /** @class */ (() => {
    let CustomerserviceService = class CustomerserviceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'service', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'service', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'service', params, { headers: finalHeaders });
            }
        }
        getservicebyID(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'service', { headers: finalHeaders, params });
            }
        }
        getServiceListbyID(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'service/list', { headers: finalHeaders, params });
            }
        }
        deleteService(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'service', options);
            }
        }
    };
    CustomerserviceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CustomerserviceService);
    return CustomerserviceService;
})();
export { CustomerserviceService };
//# sourceMappingURL=customerservice.service.js.map