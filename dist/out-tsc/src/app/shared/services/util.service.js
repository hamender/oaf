import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
let UtilService = /** @class */ (() => {
    let UtilService = class UtilService {
        constructor() {
            this.breadcrumbSource = new BehaviorSubject([]);
            this.currentBreadcrumb = this.breadcrumbSource.asObservable();
            this.messageSource = new BehaviorSubject('false');
            this.currentMessage = this.messageSource.asObservable();
        }
        changeBreadcrumb(breadcrumb) {
            this.breadcrumbSource.next(breadcrumb);
        }
        changeMessage(message) {
            this.messageSource.next(message);
        }
    };
    UtilService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], UtilService);
    return UtilService;
})();
export { UtilService };
export class Breadcrump {
}
//# sourceMappingURL=util.service.js.map