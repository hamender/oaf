import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let PlatformServiceService = /** @class */ (() => {
    let PlatformServiceService = class PlatformServiceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'platform', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'platform', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        summary1() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'summaryreport', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'platform', params, { headers: finalHeaders });
            }
        }
        getPlatformbyID(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'platform', { headers: finalHeaders, params });
            }
        }
        deletePlatform(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'platform', options);
            }
        }
        dashboardrequestCount() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'dashboard/getrequestcount', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        getInstanceCount(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'dashboard/getinstancecount', { headers: finalHeaders, params });
            }
        }
        getInstanceSlider(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'dashboard/getinstanceperservice', { headers: finalHeaders, params });
            }
        }
    };
    PlatformServiceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PlatformServiceService);
    return PlatformServiceService;
})();
export { PlatformServiceService };
//# sourceMappingURL=platform-service.service.js.map