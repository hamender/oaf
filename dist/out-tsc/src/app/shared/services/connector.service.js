import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ConnectorService = /** @class */ (() => {
    let ConnectorService = class ConnectorService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'connector', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'connector', params, { headers: finalHeaders });
            }
        }
        get(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'connector', { headers: finalHeaders, params });
            }
        }
        deleteConnector(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'connector', options);
            }
        }
        list() {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.get(this.url + 'connector/list', {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
    };
    ConnectorService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ConnectorService);
    return ConnectorService;
})();
export { ConnectorService };
//# sourceMappingURL=connector.service.js.map