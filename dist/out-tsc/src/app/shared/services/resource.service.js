import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ResourceService = /** @class */ (() => {
    let ResourceService = class ResourceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.post(this.url + 'resource', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                return this.http.put(this.url + 'resource', data, {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        /*	deleteResource(params: any):Observable<any> {
          return this.http.delete(this.url+'resource/', params)
          }*/
        deleteResource(data) {
            let idt = this.auth.getToken1();
            if (idt) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': idt,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'resource', options);
            }
        }
        getResource(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'resource', { headers: finalHeaders, params });
            }
        }
        list(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'resource/list', { headers: finalHeaders, params });
            }
        }
        getHistorybyID(params) {
            let idt = this.auth.getToken1();
            if (idt) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': idt,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provisionhistory/byresourceid', { headers: finalHeaders, params });
            }
        }
    };
    ResourceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ResourceService);
    return ResourceService;
})();
export { ResourceService };
//# sourceMappingURL=resource.service.js.map