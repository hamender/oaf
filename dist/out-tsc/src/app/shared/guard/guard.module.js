import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
let AuthGuard = /** @class */ (() => {
    let AuthGuard = class AuthGuard {
        constructor(router, authService) {
            this.router = router;
            this.authService = authService;
        }
        canActivate(route, state) {
            if (this.authService.isLoggedIn()) {
                return of(this.authService.isLoggedIn());
            }
            return of(this.router.createUrlTree(['/auth'], { queryParams: { return: state.url } }));
        }
    };
    AuthGuard = __decorate([
        Injectable()
    ], AuthGuard);
    return AuthGuard;
})();
export { AuthGuard };
//# sourceMappingURL=guard.module.js.map