import { __decorate } from "tslib";
import { Pipe } from '@angular/core';
import { isObservable, of } from 'rxjs';
import { map, startWith, catchError } from 'rxjs/operators';
let WithLoadingPipe = /** @class */ (() => {
    let WithLoadingPipe = class WithLoadingPipe {
        transform(val) {
            return isObservable(val)
                ? val.pipe(map((value) => ({ loading: false, value })), startWith({ loading: true }), catchError(error => of({ loading: false, error })))
                : val;
        }
    };
    WithLoadingPipe = __decorate([
        Pipe({
            name: 'withLoading',
        })
    ], WithLoadingPipe);
    return WithLoadingPipe;
})();
export { WithLoadingPipe };
//# sourceMappingURL=order-by.pipe.js.map