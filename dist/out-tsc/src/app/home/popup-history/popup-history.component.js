import { __decorate } from "tslib";
import { Component, Output, Input, EventEmitter } from '@angular/core';
let PopupHistoryComponent = /** @class */ (() => {
    let PopupHistoryComponent = class PopupHistoryComponent {
        constructor(activeModal, proservice, resourceservice) {
            this.activeModal = activeModal;
            this.proservice = proservice;
            this.resourceservice = resourceservice;
            this.closeviewpopup = new EventEmitter();
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = true;
        }
        ngOnInit() {
            if (this.id && this.loc && this.loc == 'header') {
                let param = {
                    'Id': this.id
                };
                console.log(param);
                this.proservice.getSingleHistory(param).subscribe(res => {
                    console.log(res);
                    this.showload = false;
                    let arr = [];
                    for (let item of res.Record) {
                        console.log(item);
                        var rid = 'Request Id';
                        var ridVal = item[rid];
                        arr.push({ 'itms': item, 'rid': ridVal });
                        this.status = item['Status'];
                    }
                    console.log(arr);
                    this.data = arr;
                });
            }
            if (this.pid && this.loc && this.loc == 'provision') {
                let param = {
                    'Id': this.pid
                };
                this.proservice.getSingleHistory(param).subscribe(res => {
                    this.showload = false;
                    console.log(res);
                    let arr = [];
                    for (let item of res.Record) {
                        console.log(item);
                        var rid = 'Request Id';
                        var ridVal = item[rid];
                        arr.push({ 'itms': item, 'rid': ridVal });
                        this.status = item['Status'];
                    }
                    this.data = arr;
                });
            }
            if (this.rqid && this.loc && this.loc == 'resource') {
                let param = {
                    'Id': this.rqid
                };
                this.proservice.getSingleHistory(param).subscribe(res => {
                    this.showload = false;
                    console.log(res);
                    let arr = [];
                    for (let item of res.Record) {
                        console.log(item);
                        var rid = 'Request Id';
                        var ridVal = item[rid];
                        arr.push({ 'itms': item, 'rid': ridVal });
                        this.status = item['Status'];
                    }
                    this.data = arr;
                });
            }
        }
        closeform() {
            this.activeModal.close();
            this.closeviewpopup.emit('close');
        }
    };
    __decorate([
        Input()
    ], PopupHistoryComponent.prototype, "id", void 0);
    __decorate([
        Input()
    ], PopupHistoryComponent.prototype, "rqid", void 0);
    __decorate([
        Input()
    ], PopupHistoryComponent.prototype, "pid", void 0);
    __decorate([
        Input()
    ], PopupHistoryComponent.prototype, "loc", void 0);
    __decorate([
        Output()
    ], PopupHistoryComponent.prototype, "closeviewpopup", void 0);
    PopupHistoryComponent = __decorate([
        Component({
            selector: 'app-popup-history',
            templateUrl: './popup-history.component.html',
            styleUrls: ['./popup-history.component.scss']
        })
    ], PopupHistoryComponent);
    return PopupHistoryComponent;
})();
export { PopupHistoryComponent };
//# sourceMappingURL=popup-history.component.js.map