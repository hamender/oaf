import { __decorate } from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { PopupHistoryComponent } from '../../../home/popup-history/popup-history.component';
import { trigger, transition, style, animate, query, stagger, state } from "@angular/animations";
let EditPlatformComponent = /** @class */ (() => {
    let EditPlatformComponent = class EditPlatformComponent {
        constructor(activeModal, fb, platformservice, resourceservice, elem, modalService) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.platformservice = platformservice;
            this.resourceservice = resourceservice;
            this.elem = elem;
            this.modalService = modalService;
            this.passEntry2 = new EventEmitter();
            this.delRes = new EventEmitter();
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = true;
            this.typeArr = [];
            this.dropArray = [];
            this.arr = [];
            this.show = false;
            this.getAll = [];
            this.compArray = [];
            this.param = {};
            this.parms = {};
            this.delbtnDisable = true;
            this.globalName = true;
            this.showerrmsg = false;
            this.indextypevalue = '';
        }
        ngOnInit() {
            if (localStorage.getItem('Attribute')) {
                let attribute = JSON.parse(localStorage.getItem('Attribute'));
                this.globall = attribute.groups[0];
                if (this.globall == 'Administrator') {
                    this.globalName = false;
                }
                else if (this.globall == 'Standard') {
                    this.globalName = false;
                }
                else {
                    this.globalName = true;
                }
            }
            this.userForm = this.fb.group({
                platform: ['', [Validators.required]],
            });
            if (this.platId && this.resId) {
                this.getPlatform(this.platId, this.resId, this.index);
            }
            if (this.viewId) {
                this.getPlatform(this.viewId, this.viewresId, this.index);
            }
            this.arr = [
                { 'ftype': 'Support Email', 'type': 'email', 'exacttype': 'Support Email*' },
                { 'ftype': 'field-a', 'type': 'text', 'exacttype': 'Alphabets only' },
                { 'ftype': 'field-d', 'type': 'date', 'exacttype': 'Date' },
                { 'ftype': 'field-dt', 'type': 'datetime', 'exacttype': 'Date and Time' },
                { 'ftype': 'field-e', 'type': 'email', 'exacttype': 'Email' },
                { 'ftype': 'field-me', 'type': 'text', 'exacttype': 'Emails (comma separated)' },
                { 'ftype': 'field-m', 'type': 'text', 'exacttype': 'Mixed String' },
                { 'ftype': 'field-mm', 'type': 'text', 'exacttype': 'Mixed Strings (comma separated)' },
                { 'ftype': 'field-x', 'type': 'text', 'exacttype': 'Mixed Encrypted' },
                { 'ftype': 'field-n', 'type': 'text', 'exacttype': 'Numbers only' },
                { 'ftype': 'field-s', 'type': 'selector', 'exacttype': 'Select' },
                { 'ftype': 'field-ms', 'type': 'text', 'exacttype': 'Selects (comma separated)' },
                { 'ftype': 'field-sn', 'type': 'text', 'exacttype': 'Select or Add new' },
            ];
        }
        updateForm(data) {
            this.userForm.patchValue({ platform: data });
            this.delbtnDisable = false;
        }
        getPlatform(value, resVal, index) {
            let p = {
                'platform': value,
                'id': resVal
            };
            this.indextypevalue = index;
            this.resourceservice.getResource(p).subscribe(res => {
                console.log(res);
                this.showload = false;
                if (res.statusCode == 200) {
                    this.updateForm(value);
                    let headArray = [];
                    this.itype = res.Metadata[index];
                    this.platname = value;
                    console.log(this.itype);
                    for (let i of Object.keys(res.Metadata)) {
                        if (i != 'services') {
                            headArray.push(i);
                        }
                    }
                    let valArray = [];
                    for (let val of Object.values(res.Metadata)) {
                        if (!Array.isArray(val)) {
                            valArray.push(val);
                        }
                    }
                    var result = valArray.reduce(function (result, field, index) {
                        result[headArray[index]] = field;
                        return result;
                    }, {});
                    this.compArray = result;
                }
            });
        }
        gettype(a) {
            for (let ar of this.arr) {
                if (a === ar.ftype) {
                    return ar.type;
                }
            }
        }
        fieldChange() {
        }
        selectedDrop() {
        }
        closeform() {
            this.activeModal.close();
        }
        textValidation(event) {
            console.log(event.target.value);
            if (event.target.value == '') {
                this.showerrmsg = true;
            }
            else {
                this.showerrmsg = false;
            }
        }
        /*  getIds($event){
            let platformId = this.platformId.nativeElement.value;
            let field = this.field.nativeElement.value;
        
          }*/
        submitForm(userForm) {
            this.showload = true;
            // let updateArray= []
            let platformId = this.platformId.nativeElement.value;
            let field = this.field.nativeElement.value;
            let elements = this.elem.nativeElement.querySelectorAll('.option_input');
            elements.forEach(element => {
                this.param[element.name] = element.value;
            });
            this.param['platform'] = platformId;
            //console.log(this.param)
            this.resourceservice.update(this.param).subscribe(res => {
                //console.log(res)
                this.showload = false;
                var rid = 'Request Id';
                if (res.StatusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Your request has been validated and queued, you will received an email notification when fulfilled',
                        html: "Your request id is:" + ' ' + '<span data-id="' + res[rid] + '" class="SwalBtn1 customSwalBtn">' + res[rid] + '</span>',
                        showConfirmButton: true,
                        confirmButtonColor: '#707070',
                    });
                    this.activeModal.close();
                    var self = this;
                    $(document).on('click', '.SwalBtn1', function () {
                        //Some code 1
                        var iddd = $('.SwalBtn1').data('id');
                        //console.log(iddd)
                        self.gethostiry(iddd);
                        Swal.clickConfirm();
                    });
                }
                else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Your request could not be validated and has been cancelled',
                        showConfirmButton: true,
                        confirmButtonColor: '#707070',
                    });
                }
                this.passEntry2.emit(res);
            });
        }
        gethostiry(id) {
            console.log(id);
            const ngmodalRef = this.modalService.open(PopupHistoryComponent, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'customhistory'
            });
            ngmodalRef.componentInstance.rqid = id;
            ngmodalRef.componentInstance.loc = 'resource';
        }
        delPlatformRes(idextype, plat) {
            /* let params= {
               this.index: idextype,
               'platform':plat
             }*/
            this.parms[this.index] = idextype;
            this.parms['platform'] = plat;
            //console.log(this.parms)
            Swal.fire({
                title: 'Are you sure you want to delete?',
                text: "This action cannot be undone",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    this.resourceservice.deleteResource(this.parms).subscribe(res => {
                        var rid = 'Request_Id';
                        console.log(res[rid]);
                        if (res.StatusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Your request has been validated and queued, you will received an email notification when fulfilled',
                                html: "Your request id is:" + ' ' + '<span data-id="' + res[rid] + '" class="SwalBtn1 customSwalBtn">' + res[rid] + '</span>',
                                showConfirmButton: true,
                                confirmButtonColor: '#707070',
                            });
                            var self = this;
                            $(document).on('click', '.SwalBtn1', function () {
                                //Some code 1
                                var iddd = $('.SwalBtn1').data('id');
                                //console.log(iddd)
                                self.gethostiry(iddd);
                                Swal.clickConfirm();
                            });
                        }
                        else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Your request could not be validated and has been cancelled',
                                showConfirmButton: true,
                                confirmButtonColor: '#707070',
                            });
                        }
                        this.activeModal.close();
                        this.delRes.emit(res);
                    });
                }
            });
        }
    };
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "platId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "resId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "index", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "viewId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "viewresId", void 0);
    __decorate([
        ViewChild('platformId')
    ], EditPlatformComponent.prototype, "platformId", void 0);
    __decorate([
        ViewChild('dropdown')
    ], EditPlatformComponent.prototype, "dropdown", void 0);
    __decorate([
        ViewChild('field')
    ], EditPlatformComponent.prototype, "field", void 0);
    __decorate([
        ViewChild('field1')
    ], EditPlatformComponent.prototype, "field1", void 0);
    __decorate([
        Output()
    ], EditPlatformComponent.prototype, "passEntry2", void 0);
    __decorate([
        Output()
    ], EditPlatformComponent.prototype, "delRes", void 0);
    EditPlatformComponent = __decorate([
        Component({
            selector: 'app-edit-platform',
            templateUrl: './edit-platform.component.html',
            styleUrls: ['./edit-platform.component.scss'],
            animations: [
                trigger("listAnimation", [
                    transition("* => *", [
                        // each time the binding value changes
                        query(":leave", [stagger(100, [animate("0.5s", style({ opacity: 0 }))])], { optional: true }),
                        query(":enter", [
                            style({ opacity: 0 }),
                            stagger(100, [animate("0.5s", style({ opacity: 1 }))])
                        ], { optional: true })
                    ])
                ]),
                trigger('enterAnimation', [
                    transition(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                        animate('500ms', style({ transform: 'translateX(0)', opacity: 1, 'overflow-x': 'hidden' }))
                    ]),
                    transition(':leave', [
                        style({ transform: 'translateX(0)', opacity: 1 }),
                        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ]),
                trigger('slideIn', [
                    state('*', style({ 'overflow-y': 'hidden' })),
                    state('void', style({ 'overflow-y': 'hidden' })),
                    transition('* => void', [
                        style({ height: '*' }),
                        animate(250, style({ height: 0 }))
                    ]),
                    transition('void => *', [
                        style({ height: '0' }),
                        animate(250, style({ height: '*' }))
                    ])
                ])
            ]
        })
    ], EditPlatformComponent);
    return EditPlatformComponent;
})();
export { EditPlatformComponent };
//# sourceMappingURL=edit-platform.component.js.map