import { __decorate } from "tslib";
import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { trigger, transition, style, animate, query, stagger, state } from "@angular/animations";
let AddPlatformComponent = /** @class */ (() => {
    let AddPlatformComponent = class AddPlatformComponent {
        constructor(activeModal, fb, platformservice) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.platformservice = platformservice;
            this.passEntry1 = new EventEmitter();
            this.slcVal = '';
            this.nARray = [];
            this.loader = false;
            this.showerror = false;
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = false;
        }
        ngOnInit() {
            this.platForm = this.fb.group({
                platformName: ['', [Validators.required]],
                serviceIndex: ['', [Validators.required]],
                ServiceIndexValue: ['', [Validators.required]],
                services: this.fb.array([]),
            });
            setTimeout(() => {
                this.addserviceClick.nativeElement.click();
            }, 200);
            this.addNewAddressGroup();
        }
        closeform() {
            this.activeModal.close();
        }
        typeChange(event, i) {
            let val = i;
            if (this.nARray.includes(val)) {
                if (event.target.value != 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    const index = this.nARray.indexOf(val);
                    if (index !== -1) {
                        this.nARray.splice(index, 1);
                    }
                }
            }
            else {
                if (event.target.value == 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    this.nARray.push(val);
                    console.log(this.nARray);
                }
            }
        }
        submitplatForm(platForm) {
            if (this.platForm.invalid || this.addressGroup.invalid) {
                this.showerror = true;
            }
            else {
                this.showload = true;
                this.showerror = false;
                this.loader = true;
                let newArray = [];
                for (let arr of platForm.value.services) {
                    let newObj = {};
                    newObj['type'] = arr.serviceFields;
                    newObj['name'] = arr.ServiceFieldValue;
                    if (arr.serviceFields == 'field-s') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    if (arr.serviceFields == 'field-ms') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    if (arr.serviceFields == 'field-sn') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    newArray.push(newObj);
                }
                let params = {
                    'index': platForm.value.ServiceIndexValue,
                    'platform': platForm.value.platformName,
                    'fields': newArray
                };
                console.log(params);
                this.platformservice.create(params).subscribe(res => {
                    this.showload = false;
                    if (res.statusCode == 200) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Platform created!!',
                            showConfirmButton: true,
                        });
                        this.activeModal.close();
                    }
                    else {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: res.Message,
                            showConfirmButton: true,
                        });
                    }
                    this.loader = false;
                    this.passEntry1.emit(res);
                });
            }
        }
        addNewAddressGroup() {
            const add = this.platForm.get('services');
            add.push(this.fb.group({
                serviceFields: ['', [Validators.required]],
                ServiceFieldValue: ['', [Validators.required]],
                ServiceFieldValue1: [''],
            }));
        }
        deleteAddressGroup(index) {
            const add = this.platForm.get('services');
            add.removeAt(index);
            if (this.nARray.includes(index)) {
                const index1 = this.nARray.indexOf(index);
                if (index1 !== -1) {
                    this.nARray.splice(index1, 1);
                }
            }
        }
        get forgetF() {
            return this.platForm.controls;
        }
        get addressGroup() {
            return this.platForm.get('services');
        }
    };
    __decorate([
        ViewChild('addserviceClick')
    ], AddPlatformComponent.prototype, "addserviceClick", void 0);
    __decorate([
        Output()
    ], AddPlatformComponent.prototype, "passEntry1", void 0);
    AddPlatformComponent = __decorate([
        Component({
            selector: 'app-add-platform',
            templateUrl: './add-platform.component.html',
            styleUrls: ['./add-platform.component.scss'],
            animations: [
                trigger("listAnimation", [
                    transition("* => *", [
                        // each time the binding value changes
                        query(":leave", [stagger(100, [animate("0.5s", style({ opacity: 0 }))])], { optional: true }),
                        query(":enter", [
                            style({ opacity: 0 }),
                            stagger(100, [animate("0.5s", style({ opacity: 1 }))])
                        ], { optional: true })
                    ])
                ]),
                trigger('enterAnimation', [
                    transition(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                        animate('500ms', style({ transform: 'translateX(0)', opacity: 1, 'overflow-x': 'hidden' }))
                    ]),
                    transition(':leave', [
                        style({ transform: 'translateX(0)', opacity: 1 }),
                        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ]),
                trigger('slideIn', [
                    state('*', style({ 'overflow-y': 'hidden' })),
                    state('void', style({ 'overflow-y': 'hidden' })),
                    transition('* => void', [
                        style({ height: '*' }),
                        animate(250, style({ height: 0 }))
                    ]),
                    transition('void => *', [
                        style({ height: '0' }),
                        animate(250, style({ height: '*' }))
                    ])
                ])
            ]
        })
    ], AddPlatformComponent);
    return AddPlatformComponent;
})();
export { AddPlatformComponent };
//# sourceMappingURL=add-platform.component.js.map