import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { EditUserComponent } from '../../pages/manage/edit-user/edit-user.component';
import { AddCustomerComponent } from '../../pages/customer/add-customer/add-customer.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { NavigationEnd } from '@angular/router';
import { trigger, transition, style, animate, query, stagger, state } from "@angular/animations";
import { faEye, faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
let ManageUserComponent = /** @class */ (() => {
    let ManageUserComponent = class ManageUserComponent {
        constructor(router, modalService, customerservice, utilService) {
            this.router = router;
            this.modalService = modalService;
            this.customerservice = customerservice;
            this.utilService = utilService;
            this.faEye = faEye;
            this.faPlus = faPlus;
            this.faPencilAlt = faPencilAlt;
            this.showload = true;
            this.dtOptions = {};
            this.dtTrigger = new Subject();
            this.show = false;
            this.loadergif = '../../../assets/images/turn.gif';
            this.reqArray = [];
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Manage User' }];
            this.utilService.changeBreadcrumb(breadcrumb);
            this.router.events.subscribe((e) => {
                if (e instanceof NavigationEnd) {
                }
            });
        }
        ngOnInit() {
            setTimeout(() => {
                this.getList();
            }, 2000);
            this.dtOptions = {
                initComplete: function (settings, json) {
                    $('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    });
                },
                scrollCollapse: true,
                paging: true,
                select: true,
                bFilter: true,
                bInfo: true,
                ordering: true,
                lengthMenu: [10, 25, 50, 100, 500, 1000, 2000],
            };
        }
        viewUser(index) {
            const ngmodalRef = this.modalService.open(EditUserComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.viewId = index;
            /*ngmodalRef.componentInstance.passEntry3.subscribe((receivedEntry) => {
            console.log(receivedEntry);
            })*/
        }
        editUser(index) {
            const ngmodalRef = this.modalService.open(EditUserComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.username = index;
            ngmodalRef.componentInstance.passEntry4.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            ngmodalRef.componentInstance.dEntry.subscribe((rdata1) => {
                if (rdata1.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        addCustomer() {
            const ngmodalRef = this.modalService.open(AddCustomerComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.aCustomer.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            //this.router.navigate(['//add-customer'])
        }
        /*ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
      }*/
        getList(noUpdate) {
            this.customerservice.list().subscribe(res => {
                console.log(res);
                this.showload = false;
                if (res.statusCode == 200) {
                    this.userList = res.data.Users;
                    this.show = true;
                    this.checkboxes = new Array(this.userList.length);
                    this.checkboxes.fill(false);
                    if (noUpdate) {
                        this.dtElement.dtInstance.then((dtInstance) => {
                            dtInstance.destroy();
                            this.dtTrigger.next();
                        });
                    }
                    else {
                        this.dtTrigger.next();
                    }
                }
            });
        }
    };
    __decorate([
        ViewChild(DataTableDirective, { static: false })
    ], ManageUserComponent.prototype, "dtElement", void 0);
    ManageUserComponent = __decorate([
        Component({
            selector: 'app-manage-user',
            templateUrl: './manage-user.component.html',
            styleUrls: ['./manage-user.component.scss'],
            animations: [
                trigger("listAnimation1", [
                    transition("* => *", [
                        // each time the binding value changes
                        query(":leave", [stagger(100, [animate("0.5s", style({ opacity: 0 }))])], { optional: true }),
                        query(":enter", [
                            style({ opacity: 0 }),
                            stagger(100, [animate("0.5s", style({ opacity: 1 }))])
                        ], { optional: true })
                    ])
                ]),
                trigger('enterAnimation', [
                    transition(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                        animate('500ms', style({ transform: 'translateX(0)', opacity: 1, 'overflow-x': 'hidden' }))
                    ]),
                    transition(':leave', [
                        style({ transform: 'translateX(0)', opacity: 1 }),
                        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ]),
                trigger('slideIn', [
                    state('*', style({ 'overflow-y': 'hidden' })),
                    state('void', style({ 'overflow-y': 'hidden' })),
                    transition('* => void', [
                        style({ height: '*' }),
                        animate(250, style({ height: 0 }))
                    ]),
                    transition('void => *', [
                        style({ height: '0' }),
                        animate(250, style({ height: '*' }))
                    ])
                ])
            ]
        })
    ], ManageUserComponent);
    return ManageUserComponent;
})();
export { ManageUserComponent };
//# sourceMappingURL=manage-user.component.js.map