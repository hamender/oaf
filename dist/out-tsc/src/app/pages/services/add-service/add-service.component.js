import { __decorate } from "tslib";
import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
let AddServiceComponent = /** @class */ (() => {
    let AddServiceComponent = class AddServiceComponent {
        constructor(activeModal, fb, customerservice, platformservice) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.customerservice = customerservice;
            this.platformservice = platformservice;
            this.aService = new EventEmitter();
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = true;
            this.slcVal = '';
            this.nARray = [];
            this.showerror = false;
        }
        ngOnInit() {
            this.serviceForm = this.fb.group({
                platformName: ['', [Validators.required]],
                servicename: ['', [Validators.required]],
                serviceIndex: ['', [Validators.required]],
                serviceIndexValue: ['', [Validators.required]],
                services: this.fb.array([]),
            });
            setTimeout(() => {
                this.addserviceClick.nativeElement.click();
            }, 200);
            this.getPlatformList();
            this.addNewServiceGroup();
        }
        closeform() {
            this.activeModal.close();
        }
        typeChange(event, i) {
            let val = i;
            console.log(i);
            if (this.nARray.includes(val)) {
                if (event.target.value != 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    const index = this.nARray.indexOf(val);
                    if (index !== -1) {
                        this.nARray.splice(index, 1);
                    }
                }
            }
            else {
                if (event.target.value == 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    this.nARray.push(val);
                    console.log(this.nARray);
                }
            }
        }
        getPlatformList() {
            this.platformservice.list().subscribe(res => {
                console.log(res);
                this.showload = false;
                if (res.statusCode == 200) {
                    this.platformList = res.Metadata;
                }
            });
        }
        submitserviceForm(serviceForm) {
            if (this.serviceForm.invalid || this.addressGroup.invalid) {
                this.showerror = true;
            }
            else {
                this.showerror = false;
                this.showload = true;
                let newArray = [];
                for (let arr of serviceForm.value.services) {
                    let newObj = {};
                    newObj['type'] = arr.serviceFields;
                    newObj['name'] = arr.ServiceFieldValue;
                    if (arr.serviceFields == 'field-s') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    if (arr.serviceFields == 'field-ms') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    if (arr.serviceFields == 'field-sn') {
                        newObj['values'] = arr.ServiceFieldValue1;
                    }
                    newArray.push(newObj);
                }
                let params = {
                    index: serviceForm.value.serviceIndexValue,
                    service: serviceForm.value.servicename,
                    platform: serviceForm.value.platformName,
                    fields: newArray
                };
                this.customerservice.create(params).subscribe(res => {
                    this.showload = true;
                    this.serviceForm.reset();
                    if (res.statusCode == 200) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Service Created!!',
                            showConfirmButton: true,
                            confirmButtonColor: '#707070',
                        });
                        this.activeModal.close();
                        this.aService.emit(res);
                    }
                    else {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: res.message,
                            showConfirmButton: true,
                            confirmButtonColor: '#707070',
                        });
                    }
                });
            }
        }
        addNewServiceGroup() {
            const add = this.serviceForm.get('services');
            add.push(this.fb.group({
                serviceFields: ['', [Validators.required]],
                ServiceFieldValue: ['', [Validators.required]],
                ServiceFieldValue1: [''],
            }));
        }
        deleteServiceGroup(index) {
            const add = this.serviceForm.get('services');
            add.removeAt(index);
            if (this.nARray.includes(index)) {
                console.log('ss');
                //if(event.target.value != 'field-s'){
                const index1 = this.nARray.indexOf(index);
                if (index1 !== -1) {
                    this.nARray.splice(index1, 1);
                }
                //} 
            }
        }
        get forgetF() {
            return this.serviceForm.controls;
        }
        get addressGroup() {
            return this.serviceForm.get('services');
        }
    };
    __decorate([
        ViewChild('addserviceClick')
    ], AddServiceComponent.prototype, "addserviceClick", void 0);
    __decorate([
        Output()
    ], AddServiceComponent.prototype, "aService", void 0);
    AddServiceComponent = __decorate([
        Component({
            selector: 'app-add-service',
            templateUrl: './add-service.component.html',
            styleUrls: ['./add-service.component.scss']
        })
    ], AddServiceComponent);
    return AddServiceComponent;
})();
export { AddServiceComponent };
//# sourceMappingURL=add-service.component.js.map