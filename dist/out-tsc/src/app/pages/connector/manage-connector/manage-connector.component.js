import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { AddConnectorComponent } from '../add-connector/add-connector.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { EditConnectorComponent } from '../edit-connector/edit-connector.component';
import { trigger, transition, style, animate, query, stagger, state } from "@angular/animations";
import { faEye, faPencilAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
let ManageConnectorComponent = /** @class */ (() => {
    let ManageConnectorComponent = class ManageConnectorComponent {
        constructor(utilService, modalService, connectorservice) {
            this.utilService = utilService;
            this.modalService = modalService;
            this.connectorservice = connectorservice;
            this.faEye = faEye;
            this.faPlus = faPlus;
            this.faPencilAlt = faPencilAlt;
            this.dtOptions = {};
            this.dtTrigger = new Subject();
            this.globalName = true;
            this.showload = true;
            this.loadergif = '../../../assets/images/turn.gif';
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Manage Connector' }];
            this.utilService.changeBreadcrumb(breadcrumb);
        }
        ngOnInit() {
            if (localStorage.getItem('Attribute')) {
                let attribute = JSON.parse(localStorage.getItem('Attribute'));
                this.globall = attribute.groups[0];
                if (this.globall == 'Administrator') {
                    this.globalName = false;
                }
                else if (this.globall == 'Standard') {
                    this.globalName = false;
                }
                else {
                    this.globalName = true;
                }
            }
            this.dtOptions = {
                initComplete: function (settings, json) {
                    $('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    });
                },
                scrollCollapse: true,
                paging: true,
                select: true,
                bFilter: true,
                bInfo: true,
                ordering: true,
                lengthMenu: [10, 25, 50, 100, 500, 1000, 2000],
            };
            setTimeout(() => {
                this.getList();
            }, 2000);
        }
        addConnector() {
            const ngmodalRef = this.modalService.open(AddConnectorComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.Aconnector.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        editConnector(event) {
            const ngmodalRef = this.modalService.open(EditConnectorComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.eID = event;
            ngmodalRef.componentInstance.editCon.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            ngmodalRef.componentInstance.delCon.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        viewConnector(event) {
            const ngmodalRef = this.modalService.open(EditConnectorComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.vID = event;
        }
        getList(noUpdate) {
            this.connectorservice.list().subscribe(res => {
                this.showload = false;
                if (res.statusCode == 200) {
                    if (res.data.Items.length > 0) {
                        this.tableData = res.data.Items;
                    }
                    if (noUpdate) {
                        this.dtElement.dtInstance.then((dtInstance) => {
                            dtInstance.destroy();
                            this.dtTrigger.next();
                        });
                    }
                    else {
                        this.dtTrigger.next();
                    }
                }
            });
        }
    };
    __decorate([
        ViewChild(DataTableDirective, { static: false })
    ], ManageConnectorComponent.prototype, "dtElement", void 0);
    ManageConnectorComponent = __decorate([
        Component({
            selector: 'app-manage-connector',
            templateUrl: './manage-connector.component.html',
            styleUrls: ['./manage-connector.component.scss'],
            animations: [
                trigger("listAnimation", [
                    transition("* => *", [
                        // each time the binding value changes
                        query(":leave", [stagger(100, [animate("0.5s", style({ opacity: 0 }))])], { optional: true }),
                        query(":enter", [
                            style({ opacity: 0 }),
                            stagger(100, [animate("0.5s", style({ opacity: 1 }))])
                        ], { optional: true })
                    ])
                ]),
                trigger('enterAnimation', [
                    transition(':enter', [
                        style({ transform: 'translateX(100%)', opacity: 0 }),
                        animate('500ms', style({ transform: 'translateX(0)', opacity: 1, 'overflow-x': 'hidden' }))
                    ]),
                    transition(':leave', [
                        style({ transform: 'translateX(0)', opacity: 1 }),
                        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ]),
                trigger('slideIn', [
                    state('*', style({ 'overflow-y': 'hidden' })),
                    state('void', style({ 'overflow-y': 'hidden' })),
                    transition('* => void', [
                        style({ height: '*' }),
                        animate(250, style({ height: 0 }))
                    ]),
                    transition('void => *', [
                        style({ height: '0' }),
                        animate(250, style({ height: '*' }))
                    ])
                ])
            ]
        })
    ], ManageConnectorComponent);
    return ManageConnectorComponent;
})();
export { ManageConnectorComponent };
//# sourceMappingURL=manage-connector.component.js.map