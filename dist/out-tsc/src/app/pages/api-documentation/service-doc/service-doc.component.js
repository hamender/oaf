import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let ServiceDocComponent = /** @class */ (() => {
    let ServiceDocComponent = class ServiceDocComponent {
        constructor(fb) {
            this.fb = fb;
            this.faArrowRight = faArrowRight;
            this.getservice = false;
            this.createService = false;
            this.delService = false;
            this.updateService = false;
            this.GSev = false;
            this.DSev = false;
            this.creServ = false;
            this.updServ = false;
        }
        ngOnInit() {
            this.getSev = this.fb.group({
                plat: ['', [Validators.required]],
                sev: ['', [Validators.required]],
            });
            this.delSev = this.fb.group({
                plat: ['', [Validators.required]],
                sev: ['', [Validators.required]],
            });
            this.postSev = this.fb.group({
                index: ['', [Validators.required]],
                service: ['', [Validators.required]],
                plat: ['', [Validators.required]],
                field: ['', [Validators.required]],
            });
            this.PutSev = this.fb.group({
                index1: ['', [Validators.required]],
                service1: ['', [Validators.required]],
                plat1: ['', [Validators.required]],
                field1: ['', [Validators.required]],
            });
        }
        get getS() {
            return this.getSev.controls;
        }
        get delS() {
            return this.delSev.controls;
        }
        get postS() {
            return this.postSev.controls;
        }
        get putS() {
            return this.PutSev.controls;
        }
        servclick(event) {
            if (event == 1) {
                this.getservice = true;
            }
            if (event == 2) {
                this.createService = true;
            }
            if (event == 3) {
                this.delService = true;
            }
            if (event == 4) {
                this.updateService = true;
            }
        }
        cancelclick(event) {
            if (event == 1) {
                this.getservice = false;
            }
            if (event == 2) {
                this.createService = false;
            }
            if (event == 3) {
                this.delService = false;
            }
            if (event == 4) {
                this.updateService = false;
            }
        }
        /*------------Sumit Forms----------*/
        SubmitService(getSev) {
            if (this.getSev.invalid) {
                this.getSev.controls['plat'].markAsTouched();
                this.getSev.controls['sev'].markAsTouched();
                this.GSev = false;
            }
            else {
                this.GSev = true;
                var u1 = '';
                var u2 = '';
                if (this.getSev.value) {
                    u1 = 'plat=' + this.getSev.value.plat;
                    u2 = '&sev=' + this.getSev.value.sev;
                }
                this.GetS = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.GetS1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        CreService(postSev) {
            if (this.postSev.invalid) {
                this.postSev.controls['index'].markAsTouched();
                this.postSev.controls['service'].markAsTouched();
                this.postSev.controls['plat'].markAsTouched();
                this.postSev.controls['field'].markAsTouched();
                this.creServ = false;
            }
            else {
                this.creServ = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                if (this.postSev.value) {
                    u1 = 'index=' + this.postSev.value.index;
                    u2 = '&service=' + this.postSev.value.service;
                    u3 = '&plat=' + this.postSev.value.plat;
                    u4 = '&field=' + this.postSev.value.field;
                }
                this.Post = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + ' -H "accept: application/json"';
                this.Post1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '';
            }
        }
        DeleteService(delSev) {
            if (this.delSev.invalid) {
                this.delSev.controls['plat'].markAsTouched();
                this.delSev.controls['sev'].markAsTouched();
                this.DSev = false;
            }
            else {
                this.DSev = true;
                var u1 = '';
                var u2 = '';
                if (this.delSev.value) {
                    u1 = 'plat=' + this.delSev.value.plat;
                    u2 = '&sev=' + this.delSev.value.sev;
                }
                this.DelS = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.DelS1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        UpService(PutSev) {
            if (this.PutSev.invalid) {
                this.PutSev.controls['index1'].markAsTouched();
                this.PutSev.controls['service1'].markAsTouched();
                this.PutSev.controls['plat1'].markAsTouched();
                this.PutSev.controls['field1'].markAsTouched();
                this.updServ = false;
            }
            else {
                this.updServ = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                if (this.PutSev.value) {
                    u1 = 'index1=' + this.PutSev.value.index1;
                    u2 = '&service1=' + this.PutSev.value.service1;
                    u3 = '&plat1=' + this.PutSev.value.plat1;
                    u4 = '&field1=' + this.PutSev.value.field1;
                }
                this.Put = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + ' -H "accept: application/json"';
                this.Put1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '';
            }
        }
    };
    ServiceDocComponent = __decorate([
        Component({
            selector: 'app-service-doc',
            templateUrl: './service-doc.component.html',
            styleUrls: ['./service-doc.component.scss']
        })
    ], ServiceDocComponent);
    return ServiceDocComponent;
})();
export { ServiceDocComponent };
//# sourceMappingURL=service-doc.component.js.map