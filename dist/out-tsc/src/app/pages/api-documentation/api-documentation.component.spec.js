import { async, TestBed } from '@angular/core/testing';
import { ApiDocumentationComponent } from './api-documentation.component';
describe('ApiDocumentationComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ApiDocumentationComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ApiDocumentationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=api-documentation.component.spec.js.map