import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let ApiDocumentationComponent = /** @class */ (() => {
    let ApiDocumentationComponent = class ApiDocumentationComponent {
        constructor(utilservice, fb) {
            this.utilservice = utilservice;
            this.fb = fb;
            this.faArrowRight = faArrowRight;
            this.getprovision = false;
            this.getprovisionInt = false;
            this.getprovisionRes = false;
            this.getproList = false;
            this.getusershow = false;
            this.getusershow4 = false;
            this.createusershow = false;
            this.forgetUsername = false;
            this.changePass = false;
            this.forgetPass = false;
            this.delUser = false;
            this.updateuser = false;
            this.updateCurrentUser = false;
            this.resetUser = false;
            this.gUser = false;
            this.gPro = false;
            this.gInt = false;
            this.gRes = false;
            this.gPList = false;
            this.gUserHistry = false;
            this.ctUser = false;
            this.fUser = false;
            this.fPass = false;
            this.CPass = false;
            this.dUser = false;
            this.updateShow = false;
            this.upcurr = false;
            this.reUser = false;
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'API Documentation' }];
            this.utilservice.changeBreadcrumb(breadcrumb);
        }
        ngOnInit() {
            this.addUser = this.fb.group({
                username: ['', [Validators.required]],
            });
            this.addList = this.fb.group({
                username: ['', [Validators.required]],
            });
            this.createuser = this.fb.group({
                username: ['', [Validators.required]],
                password: ['', [Validators.required]],
                name: ['', [Validators.required]],
                mobile: ['', [Validators.required]],
                email: ['', [Validators.required]],
                mfa: ['', [Validators.required]],
                group: ['', [Validators.required]],
                callbck: [''],
            });
            this.forgetp = this.fb.group({
                email: ['', [Validators.required]],
                number: ['', [Validators.required]]
            });
            this.changeP = this.fb.group({
                pre_pass: ['', [Validators.required]],
                new_pass: ['', [Validators.required]],
                acc_token: ['', [Validators.required]],
            });
            this.f_Pass = this.fb.group({
                username: ['', [Validators.required]]
            });
            this.DelUser = this.fb.group({
                username: ['', [Validators.required]],
            });
            this.update = this.fb.group({
                username: ['', [Validators.required]],
                password: ['', [Validators.required]],
                group: ['', [Validators.required]],
                attr: [''],
            });
            this.updateCurr = this.fb.group({
                token: ['', [Validators.required]],
                attr: ['', [Validators.required]],
            });
            this.Reset = this.fb.group({
                username: ['', [Validators.required]],
                password: ['', [Validators.required]],
                mfa: ['', [Validators.required]],
            });
            this.proId = this.fb.group({
                id: ['', [Validators.required]],
            });
            this.IntId = this.fb.group({
                id: ['', [Validators.required]],
            });
            this.ResId = this.fb.group({
                id: ['', [Validators.required]],
            });
            this.ProList = this.fb.group({
                record: ['', [Validators.required]],
            });
        }
        getuserclick(event) {
            if (event == 1) {
                console.log('eeee');
                this.getusershow = true;
            }
            if (event == 4) {
                this.getusershow4 = true;
            }
            if (event == 5) {
                this.getprovision = true;
            }
            if (event == 6) {
                this.getprovisionInt = true;
            }
            if (event == 7) {
                this.getprovisionRes = true;
            }
            if (event == 8) {
                this.getproList = true;
            }
        }
        cancelclick(event) {
            if (event == 1) {
                this.getusershow = false;
            }
            if (event == 4) {
                this.getusershow4 = false;
            }
            if (event == 5) {
                this.getprovision = false;
            }
            if (event == 6) {
                this.getprovisionInt = false;
            }
            if (event == 7) {
                this.getprovisionRes = false;
            }
            if (event == 8) {
                this.getproList = false;
            }
        }
        createuserclick(event) {
            if (event == 1) {
                this.createusershow = true;
            }
            if (event == 2) {
                this.forgetUsername = true;
            }
            if (event == 3) {
                this.changePass = true;
            }
            if (event == 4) {
                this.forgetPass = true;
            }
        }
        createcancelclick(event) {
            if (event == 1) {
                this.createusershow = false;
            }
            if (event == 2) {
                this.forgetUsername = false;
            }
            if (event == 3) {
                this.changePass = false;
            }
            if (event == 4) {
                this.forgetPass = false;
            }
        }
        deluserclick() {
            this.delUser = true;
        }
        delcancelclick() {
            this.delUser = false;
        }
        updateuserclick(event) {
            if (event == 1) {
                this.updateuser = true;
            }
            if (event == 2) {
                this.updateCurrentUser = true;
            }
            if (event == 3) {
                this.resetUser = true;
            }
        }
        updatecanclclick(event) {
            if (event == 1) {
                this.updateuser = false;
            }
            if (event == 2) {
                this.updateCurrentUser = false;
            }
            if (event == 3) {
                this.resetUser = false;
            }
        }
        /*-----------------*/
        get submitF() {
            return this.addUser.controls;
        }
        get submitHt() {
            return this.addList.controls;
        }
        get submitU() {
            return this.createuser.controls;
        }
        get ForgetU() {
            return this.forgetp.controls;
        }
        get ChangeP() {
            return this.changeP.controls;
        }
        get ForgetP() {
            return this.f_Pass.controls;
        }
        get DelU() {
            return this.DelUser.controls;
        }
        get updateF() {
            return this.update.controls;
        }
        get currentup() {
            return this.updateCurr.controls;
        }
        get rstUser() {
            return this.Reset.controls;
        }
        get ProvisnId() {
            return this.proId.controls;
        }
        get InstanceId() {
            return this.IntId.controls;
        }
        get ResourceId() {
            return this.ResId.controls;
        }
        get ProvisionList() {
            return this.ProList.controls;
        }
        /********Get Submit Forms Started Here ************/
        submitFormGetUser(addUser) {
            if (this.addUser.invalid) {
                this.addUser.controls['username'].markAsTouched();
                this.gUser = false;
            }
            else {
                this.gUser = true;
                var un = '';
                if (this.addUser.value) {
                    un = 'username=' + this.addUser.value.username;
                }
                this.getShow = 'curl -X GET "https://api.oaf-x.com/user/?' + un + ' -H "accept: application/json"';
                this.getShow1 = 'https://api.oaf-x.com/user/?' + un + '';
            }
        }
        submitFormGetHistory(addList) {
            if (this.addList.invalid) {
                this.addList.controls['username'].markAsTouched();
                this.gUserHistry = false;
            }
            else {
                this.gUserHistry = true;
                var unn = '';
                if (this.addList.value) {
                    unn = 'username=' + this.addList.value.username;
                }
                this.getHistry = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.getHistry1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
        /********Get Submit Forms End Here ************/
        /*------------ User Post Submit Start Here----------*/
        submitCreateUser(createuser) {
            if (this.createuser.invalid) {
                this.createuser.controls['username'].markAsTouched();
                this.createuser.controls['password'].markAsTouched();
                this.createuser.controls['name'].markAsTouched();
                this.createuser.controls['mobile'].markAsTouched();
                this.createuser.controls['email'].markAsTouched();
                this.createuser.controls['mfa'].markAsTouched();
                this.createuser.controls['group'].markAsTouched();
                this.ctUser = false;
            }
            else {
                this.ctUser = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                var u8 = '';
                if (this.createuser.value) {
                    u1 = 'username=' + this.createuser.value.username;
                    u2 = '&password=' + this.createuser.value.password;
                    u3 = '&name=' + this.createuser.value.name;
                    u4 = '&mobile=' + this.createuser.value.mobile;
                    u5 = '&email=' + this.createuser.value.email;
                    u6 = '&mfa=' + this.createuser.value.mfa;
                    u7 = '&group=' + this.createuser.value.group;
                    u8 = '&callbck=' + this.createuser.value.callbck;
                }
                this.CreateUr = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + ' -H "accept: application/json"';
                this.CreateUr1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + '';
            }
        }
        ForgetUsername(forgetp) {
            if (this.forgetp.invalid) {
                this.forgetp.controls['email'].markAsTouched();
                this.forgetp.controls['number'].markAsTouched();
                this.fUser = false;
            }
            else {
                this.fUser = true;
                var u1 = '';
                var u2 = '';
                if (this.forgetp.value) {
                    u1 = 'email=' + this.forgetp.value.email;
                    u2 = '&number=' + this.forgetp.value.number;
                }
                this.FgetU = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.FgetU1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        ChangePassword(changeP) {
            if (this.changeP.invalid) {
                this.changeP.controls['pre_pass'].markAsTouched();
                this.changeP.controls['new_pass'].markAsTouched();
                this.changeP.controls['acc_token'].markAsTouched();
                this.CPass = false;
            }
            else {
                this.CPass = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                if (this.changeP.value) {
                    u1 = 'pre_pass=' + this.changeP.value.pre_pass;
                    u2 = '&new_pass=' + this.changeP.value.new_pass;
                    u3 = '&acc_token=' + this.changeP.value.acc_token;
                }
                this.Cpass = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + ' -H "accept: application/json"';
                this.Cpass1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '';
            }
        }
        ForgetPassword(f_Pass) {
            if (this.f_Pass.invalid) {
                this.f_Pass.controls['username'].markAsTouched();
                this.fPass = false;
            }
            else {
                this.fPass = true;
                var unn = '';
                if (this.f_Pass.value) {
                    unn = 'username=' + this.f_Pass.value.username;
                }
                this.FPass = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.FPass1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
        /*----------- User Post Submit End Here-------*/
        /*----------Delete User------------*/
        DeleteUser(DelUser) {
            if (this.DelUser.invalid) {
                this.DelUser.controls['username'].markAsTouched();
                this.dUser = false;
            }
            else {
                this.dUser = true;
                var un = '';
                if (this.DelUser.value) {
                    un = 'username=' + this.DelUser.value.username;
                }
                this.delShow = 'curl -X GET "https://api.oaf-x.com/user/?' + un + ' -H "accept: application/json"';
                this.delShow1 = 'https://api.oaf-x.com/user/?' + un + '';
            }
        }
        /*-------Update User Start Here-----*/
        UpdateUser(update) {
            if (this.update.invalid) {
                this.update.controls['username'].markAsTouched();
                //this.update.controls['password'].markAsTouched();
                this.update.controls['group'].markAsTouched();
                this.updateShow = false;
            }
            else {
                this.updateShow = true;
                var un1 = '';
                var un2 = '';
                var un3 = '';
                var un4 = '';
                if (this.update.value) {
                    un1 = '&username=' + this.update.value.username;
                    un2 = '&password=' + this.update.value.password;
                    un3 = '&group=' + this.update.value.group;
                    un4 = '&attr=' + this.update.value.attr;
                }
                this.upShow = 'curl -X GET "https://api.oaf-x.com/user/?' + un1 + '' + un2 + '' + un3 + '' + un4 + ' -H "accept: application/json"';
                this.upShow1 = 'https://api.oaf-x.com/user/?' + un1 + '' + un2 + '' + un3 + '' + un4 + '';
            }
        }
        UpdateCurrentUser(updateCurr) {
            if (this.updateCurr.invalid) {
                this.updateCurr.controls['token'].markAsTouched();
                this.updateCurr.controls['attr'].markAsTouched();
                this.upcurr = false;
            }
            else {
                this.upcurr = true;
                var u1 = '';
                var u2 = '';
                if (this.updateCurr.value) {
                    u1 = 'token=' + this.updateCurr.value.token;
                    u2 = '&attr=' + this.updateCurr.value.attr;
                }
                this.upcr = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.upcr1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        ResetCurrentPass(Reset) {
            if (this.Reset.invalid) {
                this.Reset.controls['username'].markAsTouched();
                this.Reset.controls['password'].markAsTouched();
                this.Reset.controls['mfa'].markAsTouched();
                this.reUser = false;
            }
            else {
                this.reUser = true;
                var un1 = '';
                var un2 = '';
                var un3 = '';
                if (this.Reset.value) {
                    un1 = '&username=' + this.Reset.value.username;
                    un2 = '&password=' + this.Reset.value.password;
                    un3 = '&mfa=' + this.Reset.value.mfa;
                }
                this.rUser = 'curl -X GET "https://api.oaf-x.com/user/?' + un1 + '' + un2 + '' + un3 + ' -H "accept: application/json"';
                this.rUser1 = 'https://api.oaf-x.com/user/?' + un1 + '' + un2 + '' + un3 + '';
            }
        }
        /*-------Update User Start Here-----*/
        submitProvision(proId) {
            if (this.proId.invalid) {
                this.proId.controls['id'].markAsTouched();
                this.gPro = false;
            }
            else {
                this.gPro = true;
                var unn = '';
                if (this.proId.value) {
                    unn = 'id=' + this.proId.value.id;
                }
                this.getPro = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.getPro1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
        submitProvisionInt(IntId) {
            if (this.IntId.invalid) {
                this.IntId.controls['id'].markAsTouched();
                this.gInt = false;
            }
            else {
                this.gInt = true;
                var unn = '';
                if (this.IntId.value) {
                    unn = 'id=' + this.IntId.value.id;
                }
                this.getInt = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.getInt1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
        submitProvisionRes(ResId) {
            if (this.ResId.invalid) {
                this.ResId.controls['id'].markAsTouched();
                this.gRes = false;
            }
            else {
                this.gRes = true;
                var unn = '';
                if (this.ResId.value) {
                    unn = 'id=' + this.ResId.value.id;
                }
                this.getRes = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.getRes1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
        submitProvisionList(ProList) {
            if (this.ProList.invalid) {
                this.ProList.controls['record'].markAsTouched();
                this.gPList = false;
            }
            else {
                this.gPList = true;
                var unn = '';
                if (this.ProList.value) {
                    unn = 'record=' + this.ProList.value.record;
                }
                this.getPList = 'curl -X GET "https://api.oaf-x.com/user/?' + unn + ' -H "accept: application/json"';
                this.getPList1 = 'https://api.oaf-x.com/user/?' + unn + '';
            }
        }
    };
    ApiDocumentationComponent = __decorate([
        Component({
            selector: 'app-api-documentation',
            templateUrl: './api-documentation.component.html',
            styleUrls: ['./api-documentation.component.scss']
        })
    ], ApiDocumentationComponent);
    return ApiDocumentationComponent;
})();
export { ApiDocumentationComponent };
//# sourceMappingURL=api-documentation.component.js.map