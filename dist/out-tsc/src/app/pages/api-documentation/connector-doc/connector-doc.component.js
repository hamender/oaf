import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let ConnectorDocComponent = /** @class */ (() => {
    let ConnectorDocComponent = class ConnectorDocComponent {
        constructor(fb) {
            this.fb = fb;
            this.faArrowRight = faArrowRight;
            this.getConnectr = false;
            this.createConnectr = false;
            this.delConnectr = false;
            this.updateCon = false;
            this.createConnectr_role = false;
            this.createConnectr_stack = false;
            this.createConnectr_stackset = false;
            this.createConnectr_gcp = false;
            this.createConnectr_azure = false;
            this.createConnectr_file = false;
            this.Con_Get = false;
            this.Con_Del = false;
            this.Con_AWS = false;
            this.CRole = false;
            this.updateConn = false;
            this.ConStack = false;
            this.StackSet = false;
            this.ConGCP = false;
            this.ConAzure = false;
            this.ConFile = false;
        }
        ngOnInit() {
            this.getC = this.fb.group({
                id: ['', [Validators.required]],
            });
            this.delC = this.fb.group({
                id: ['', [Validators.required]],
            });
            this.CreateCon = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                frequency: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.CreateRole = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                frequency: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.UpdateRole = this.fb.group({
                id: ['', [Validators.required]],
                description: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                frequency: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.Con_Stack = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                freq_value: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.Con_StackSet = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                freq_value: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.Con_GCP = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                freq_value: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.Con_AZURE = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                rotation: ['', [Validators.required]],
                cred_detail: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                freq_value: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
            this.Con_File = this.fb.group({
                name: ['', [Validators.required]],
                con_type: ['', [Validators.required]],
                con_file: ['', [Validators.required]],
                freq_type: ['', [Validators.required]],
                freq_value: ['', [Validators.required]],
                association: ['', [Validators.required]],
            });
        }
        get submitF() {
            return this.CreateCon.controls;
        }
        get submit_Con() {
            return this.getC.controls;
        }
        get Del_Con() {
            return this.delC.controls;
        }
        get submitR() {
            return this.CreateRole.controls;
        }
        get updatecon() {
            return this.UpdateRole.controls;
        }
        get CreateStack() {
            return this.Con_Stack.controls;
        }
        get CreateStackSet() {
            return this.Con_StackSet.controls;
        }
        get CreateGCP() {
            return this.Con_GCP.controls;
        }
        get CreateAZURE() {
            return this.Con_AZURE.controls;
        }
        get CreateFile() {
            return this.Con_File.controls;
        }
        Connectrclick(event) {
            if (event == 1) {
                this.getConnectr = true;
            }
            if (event == 2) {
                this.createConnectr = true;
            }
            if (event == 3) {
                this.delConnectr = true;
            }
            if (event == 4) {
                this.updateCon = true;
            }
            if (event == 5) {
                this.createConnectr_role = true;
            }
            if (event == 6) {
                this.createConnectr_stack = true;
            }
            if (event == 7) {
                this.createConnectr_stackset = true;
            }
            if (event == 8) {
                this.createConnectr_gcp = true;
            }
            if (event == 9) {
                this.createConnectr_azure = true;
            }
            if (event == 10) {
                this.createConnectr_file = true;
            }
        }
        cancelclick(event) {
            if (event == 1) {
                this.getConnectr = false;
            }
            if (event == 2) {
                this.createConnectr = false;
            }
            if (event == 3) {
                this.delConnectr = false;
            }
            if (event == 4) {
                this.updateCon = false;
            }
            if (event == 5) {
                this.createConnectr_role = false;
            }
            if (event == 6) {
                this.createConnectr_stack = false;
            }
            if (event == 7) {
                this.createConnectr_stackset = false;
            }
            if (event == 8) {
                this.createConnectr_gcp = false;
            }
            if (event == 9) {
                this.createConnectr_azure = false;
            }
            if (event == 10) {
                this.createConnectr_file = false;
            }
        }
        /*------------Submit Forms----------*/
        SubmitConnector(getC) {
            if (this.getC.invalid) {
                this.getC.controls['id'].markAsTouched();
                this.Con_Get = false;
            }
            else {
                this.Con_Get = true;
                var u1 = '';
                if (this.getC.value) {
                    u1 = 'id=' + this.getC.value.id;
                }
                this.CGet = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + ' -H "accept: application/json"';
                this.CGet1 = 'https://api.oaf-x.com/user/?' + u1 + '';
            }
        }
        CreateConnector(CreateCon) {
            if (this.CreateCon.invalid) {
                this.CreateCon.controls['name'].markAsTouched();
                this.CreateCon.controls['con_type'].markAsTouched();
                this.CreateCon.controls['rotation'].markAsTouched();
                this.CreateCon.controls['cred_detail'].markAsTouched();
                this.CreateCon.controls['freq_type'].markAsTouched();
                this.CreateCon.controls['frequency'].markAsTouched();
                this.CreateCon.controls['association'].markAsTouched();
                this.Con_AWS = false;
            }
            else {
                this.Con_AWS = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.CreateCon.value) {
                    u1 = 'name=' + this.CreateCon.value.name;
                    u2 = '&con_type=' + this.CreateCon.value.con_type;
                    u3 = '&rotation=' + this.CreateCon.value.rotation;
                    u4 = '&cred_detail=' + this.CreateCon.value.cred_detail;
                    u5 = '&freq_type=' + this.CreateCon.value.freq_type;
                    u6 = '&frequency=' + this.CreateCon.value.frequency;
                    u7 = '&association=' + this.CreateCon.value.association;
                }
                this.conA = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.conA1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        DeleteConnector(delC) {
            if (this.delC.invalid) {
                this.delC.controls['id'].markAsTouched();
                this.Con_Del = false;
            }
            else {
                this.Con_Del = true;
                var u1 = '';
                if (this.delC.value) {
                    u1 = 'id=' + this.delC.value.id;
                }
                this.DelC = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + ' -H "accept: application/json"';
                this.DelC1 = 'https://api.oaf-x.com/user/?' + u1 + '';
            }
        }
        UpdateConnector(UpdateRole) {
            console.log(true);
            console.log(this.UpdateRole.value);
            if (this.UpdateRole.invalid) {
                this.UpdateRole.controls['id'].markAsTouched();
                this.UpdateRole.controls['description'].markAsTouched();
                this.UpdateRole.controls['freq_type'].markAsTouched();
                this.UpdateRole.controls['frequency'].markAsTouched();
                this.UpdateRole.controls['association'].markAsTouched();
                this.updateConn = false;
            }
            else {
                this.updateConn = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                if (this.UpdateRole.value) {
                    u1 = 'id=' + this.UpdateRole.value.id;
                    u2 = '&description=' + this.UpdateRole.value.description;
                    u3 = '&freq_type=' + this.UpdateRole.value.freq_type;
                    u4 = '&frequency=' + this.UpdateRole.value.frequency;
                    u5 = '&association=' + this.UpdateRole.value.association;
                }
                this.Role = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + ' -H "accept: application/json"';
                this.Role1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '';
            }
        }
        CreateConRole(CreateRole) {
            if (this.CreateRole.invalid) {
                this.CreateRole.controls['name'].markAsTouched();
                this.CreateRole.controls['con_type'].markAsTouched();
                this.CreateRole.controls['rotation'].markAsTouched();
                this.CreateRole.controls['cred_detail'].markAsTouched();
                this.CreateRole.controls['freq_type'].markAsTouched();
                this.CreateRole.controls['frequency'].markAsTouched();
                this.CreateRole.controls['association'].markAsTouched();
                this.CRole = false;
            }
            else {
                this.CRole = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.CreateRole.value) {
                    u1 = 'name=' + this.CreateRole.value.name;
                    u2 = '&con_type=' + this.CreateRole.value.con_type;
                    u3 = '&rotation=' + this.CreateRole.value.rotation;
                    u4 = '&cred_detail=' + this.CreateRole.value.cred_detail;
                    u5 = '&freq_type=' + this.CreateRole.value.freq_type;
                    u6 = '&frequency=' + this.CreateRole.value.frequency;
                    u7 = '&association=' + this.CreateRole.value.association;
                }
                this.Role = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.Role1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        CreateConStack(Con_Stack) {
            if (this.Con_Stack.invalid) {
                this.Con_Stack.controls['name'].markAsTouched();
                this.Con_Stack.controls['con_type'].markAsTouched();
                this.Con_Stack.controls['rotation'].markAsTouched();
                this.Con_Stack.controls['cred_detail'].markAsTouched();
                this.Con_Stack.controls['freq_type'].markAsTouched();
                this.Con_Stack.controls['freq_value'].markAsTouched();
                this.Con_Stack.controls['association'].markAsTouched();
                this.ConStack = false;
            }
            else {
                this.ConStack = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.Con_Stack.value) {
                    u1 = 'name=' + this.Con_Stack.value.name;
                    u2 = '&con_type=' + this.Con_Stack.value.con_type;
                    u3 = '&rotation=' + this.Con_Stack.value.rotation;
                    u4 = '&cred_detail=' + this.Con_Stack.value.cred_detail;
                    u5 = '&freq_type=' + this.Con_Stack.value.freq_type;
                    u6 = '&freq_value=' + this.Con_Stack.value.freq_value;
                    u7 = '&association=' + this.Con_Stack.value.association;
                }
                this.stack1 = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.stack2 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        CreateConStSet(Con_StackSet) {
            if (this.Con_StackSet.invalid) {
                this.Con_StackSet.controls['name'].markAsTouched();
                this.Con_StackSet.controls['con_type'].markAsTouched();
                this.Con_StackSet.controls['rotation'].markAsTouched();
                this.Con_StackSet.controls['cred_detail'].markAsTouched();
                this.Con_StackSet.controls['freq_type'].markAsTouched();
                this.Con_StackSet.controls['freq_value'].markAsTouched();
                this.Con_StackSet.controls['association'].markAsTouched();
                this.StackSet = false;
            }
            else {
                this.StackSet = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.Con_StackSet.value) {
                    u1 = 'name=' + this.Con_StackSet.value.name;
                    u2 = '&con_type=' + this.Con_StackSet.value.con_type;
                    u3 = '&rotation=' + this.Con_StackSet.value.rotation;
                    u4 = '&cred_detail=' + this.Con_StackSet.value.cred_detail;
                    u5 = '&freq_type=' + this.Con_StackSet.value.freq_type;
                    u6 = '&freq_value=' + this.Con_StackSet.value.freq_value;
                    u7 = '&association=' + this.Con_StackSet.value.association;
                }
                this.stackset1 = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.stackset2 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        CreateConGcp(Con_GCP) {
            if (this.Con_GCP.invalid) {
                this.Con_GCP.controls['name'].markAsTouched();
                this.Con_GCP.controls['con_type'].markAsTouched();
                this.Con_GCP.controls['rotation'].markAsTouched();
                this.Con_GCP.controls['cred_detail'].markAsTouched();
                this.Con_GCP.controls['freq_type'].markAsTouched();
                this.Con_GCP.controls['freq_value'].markAsTouched();
                this.Con_GCP.controls['association'].markAsTouched();
                this.ConGCP = false;
            }
            else {
                this.ConGCP = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.Con_GCP.value) {
                    u1 = 'name=' + this.Con_GCP.value.name;
                    u2 = '&con_type=' + this.Con_GCP.value.con_type;
                    u3 = '&rotation=' + this.Con_GCP.value.rotation;
                    u4 = '&cred_detail=' + this.Con_GCP.value.cred_detail;
                    u5 = '&freq_type=' + this.Con_GCP.value.freq_type;
                    u6 = '&freq_value=' + this.Con_GCP.value.freq_value;
                    u7 = '&association=' + this.Con_GCP.value.association;
                }
                this.gcp1 = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.gcp2 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        CreateConAzure(Con_AZURE) {
            if (this.Con_AZURE.invalid) {
                this.Con_AZURE.controls['name'].markAsTouched();
                this.Con_AZURE.controls['con_type'].markAsTouched();
                this.Con_AZURE.controls['rotation'].markAsTouched();
                this.Con_AZURE.controls['cred_detail'].markAsTouched();
                this.Con_AZURE.controls['freq_type'].markAsTouched();
                this.Con_AZURE.controls['freq_value'].markAsTouched();
                this.Con_AZURE.controls['association'].markAsTouched();
                this.ConAzure = false;
            }
            else {
                this.ConAzure = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                if (this.Con_AZURE.value) {
                    u1 = 'name=' + this.Con_AZURE.value.name;
                    u2 = '&con_type=' + this.Con_AZURE.value.con_type;
                    u3 = '&rotation=' + this.Con_AZURE.value.rotation;
                    u4 = '&cred_detail=' + this.Con_AZURE.value.cred_detail;
                    u5 = '&freq_type=' + this.Con_AZURE.value.freq_type;
                    u6 = '&freq_value=' + this.Con_AZURE.value.freq_value;
                    u7 = '&association=' + this.Con_AZURE.value.association;
                }
                this.CAzure1 = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + ' -H "accept: application/json"';
                this.CAzure2 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '';
            }
        }
        CreateConFile(Con_File) {
            if (this.Con_File.invalid) {
                this.Con_File.controls['name'].markAsTouched();
                this.Con_File.controls['con_type'].markAsTouched();
                this.Con_File.controls['con_file'].markAsTouched();
                this.Con_File.controls['freq_type'].markAsTouched();
                this.Con_File.controls['freq_value'].markAsTouched();
                this.Con_File.controls['association'].markAsTouched();
                this.ConFile = false;
            }
            else {
                this.ConFile = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                if (this.Con_File.value) {
                    u1 = 'name=' + this.Con_File.value.name;
                    u2 = '&con_type=' + this.Con_File.value.con_type;
                    u3 = '&con_file=' + this.Con_File.value.con_file;
                    u4 = '&freq_type=' + this.Con_File.value.freq_type;
                    u5 = '&freq_value=' + this.Con_File.value.freq_value;
                    u6 = '&association=' + this.Con_File.value.association;
                }
                this.CFile = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + ' -H "accept: application/json"';
                this.CFile1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '';
            }
        }
    };
    ConnectorDocComponent = __decorate([
        Component({
            selector: 'app-connector-doc',
            templateUrl: './connector-doc.component.html',
            styleUrls: ['./connector-doc.component.scss']
        })
    ], ConnectorDocComponent);
    return ConnectorDocComponent;
})();
export { ConnectorDocComponent };
//# sourceMappingURL=connector-doc.component.js.map