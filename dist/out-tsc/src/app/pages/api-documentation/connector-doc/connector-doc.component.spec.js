import { async, TestBed } from '@angular/core/testing';
import { ConnectorDocComponent } from './connector-doc.component';
describe('ConnectorDocComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConnectorDocComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ConnectorDocComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=connector-doc.component.spec.js.map