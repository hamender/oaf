import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let PlatformDocComponent = /** @class */ (() => {
    let PlatformDocComponent = class PlatformDocComponent {
        constructor(fb) {
            this.fb = fb;
            this.faArrowRight = faArrowRight;
            this.getplatfrm = false;
            this.createplat = false;
            this.delPlat = false;
            this.updatePlat = false;
            this.GetPlat = false;
            this.DelPlat = false;
            this.PostPlat = false;
            this.PutPlat = false;
        }
        ngOnInit() {
            this.getP = this.fb.group({
                plat: ['', [Validators.required]],
            });
            this.delP = this.fb.group({
                plat: ['', [Validators.required]],
            });
            this.PostP = this.fb.group({
                index: ['', [Validators.required]],
                plat: ['', [Validators.required]],
                fields: ['', [Validators.required]],
            });
            this.PutP = this.fb.group({
                plat1: ['', [Validators.required]],
                field: ['', [Validators.required]],
            });
        }
        get submitP() {
            return this.getP.controls;
        }
        get DelPt() {
            return this.delP.controls;
        }
        get PostPt() {
            return this.PostP.controls;
        }
        get PutPt() {
            return this.PutP.controls;
        }
        platclick(event) {
            if (event == 1) {
                this.getplatfrm = true;
            }
            if (event == 2) {
                this.createplat = true;
            }
            if (event == 3) {
                this.delPlat = true;
            }
            if (event == 4) {
                this.updatePlat = true;
            }
        }
        cancelclick(event) {
            if (event == 1) {
                this.getplatfrm = false;
            }
            if (event == 2) {
                this.createplat = false;
            }
            if (event == 3) {
                this.delPlat = false;
            }
            if (event == 4) {
                this.updatePlat = false;
            }
        }
        /*--------------Submit Forms---------*/
        submitFormGetList(getP) {
            if (this.getP.invalid) {
                this.getP.controls['plat'].markAsTouched();
                this.GetPlat = false;
            }
            else {
                this.GetPlat = true;
                var u1 = '';
                if (this.getP.value) {
                    u1 = 'plat=' + this.getP.value.plat;
                }
                this.PGet = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + ' -H "accept: application/json"';
                this.PGet1 = 'https://api.oaf-x.com/user/?' + u1 + '';
            }
        }
        DeletePlatform(delP) {
            if (this.delP.invalid) {
                this.delP.controls['plat'].markAsTouched();
                this.DelPlat = false;
            }
            else {
                this.DelPlat = true;
                var u1 = '';
                if (this.delP.value) {
                    u1 = 'plat=' + this.delP.value.plat;
                }
                this.DelP = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + ' -H "accept: application/json"';
                this.DelP1 = 'https://api.oaf-x.com/user/?' + u1 + '';
            }
        }
        SubmitCreateForm(PostP) {
            if (this.PostP.invalid) {
                this.PostP.controls['index'].markAsTouched();
                this.PostP.controls['plat'].markAsTouched();
                this.PostP.controls['fields'].markAsTouched();
                this.PostPlat = false;
            }
            else {
                this.PostPlat = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                if (this.PostP.value) {
                    u1 = 'index=' + this.PostP.value.index;
                    u2 = '&plat=' + this.PostP.value.plat;
                    u3 = '&fields=' + this.PostP.value.fields;
                }
                this.CreateP = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + ' -H "accept: application/json"';
                this.CreateP1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '';
            }
        }
        UpdatePlateform(PutP) {
            if (this.PutP.invalid) {
                this.PutP.controls['plat1'].markAsTouched();
                this.PutP.controls['field'].markAsTouched();
                this.PutPlat = false;
            }
            else {
                this.PutPlat = true;
                var u1 = '';
                var u2 = '';
                if (this.PutP.value) {
                    u1 = 'plat1=' + this.PutP.value.plat1;
                    u2 = '&field=' + this.PutP.value.field;
                }
                this.UpdateP = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.UpdateP1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
    };
    PlatformDocComponent = __decorate([
        Component({
            selector: 'app-platform-doc',
            templateUrl: './platform-doc.component.html',
            styleUrls: ['./platform-doc.component.scss']
        })
    ], PlatformDocComponent);
    return PlatformDocComponent;
})();
export { PlatformDocComponent };
//# sourceMappingURL=platform-doc.component.js.map