import { async, TestBed } from '@angular/core/testing';
import { InstanceDocComponent } from './instance-doc.component';
describe('InstanceDocComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InstanceDocComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(InstanceDocComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=instance-doc.component.spec.js.map