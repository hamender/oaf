import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let ResourceDocComponent = /** @class */ (() => {
    let ResourceDocComponent = class ResourceDocComponent {
        constructor(fb) {
            this.fb = fb;
            this.faArrowRight = faArrowRight;
            this.getresource = false;
            this.createResouce = false;
            this.delResouce = false;
            this.updateRes = false;
            this.GetRes = false;
            this.DelRes = false;
            this.UpdRes = false;
            this.CreateRes = false;
        }
        ngOnInit() {
            this.getR = this.fb.group({
                plat: ['', [Validators.required]],
                plat_id: ['', [Validators.required]],
            });
            this.delR = this.fb.group({
                plat: ['', [Validators.required]],
                plat_id: ['', [Validators.required]],
            });
            this.postR = this.fb.group({
                plat: ['', [Validators.required]],
                sup_email: ['', [Validators.required]],
                email: ['', [Validators.required]],
                mul_email: ['', [Validators.required]],
                selector: ['', [Validators.required]],
                mul_selector: ['', [Validators.required]],
                date_time: ['', [Validators.required]],
                date: ['', [Validators.required]],
                stringN: ['', [Validators.required]],
                stringA: ['', [Validators.required]],
                StringM: ['', [Validators.required]],
                Mul_String: ['', [Validators.required]],
                mixed: ['', [Validators.required]],
            });
            this.putR = this.fb.group({
                plat: ['', [Validators.required]],
                sup_email: ['', [Validators.required]],
                email: ['', [Validators.required]],
                mul_email: ['', [Validators.required]],
                selector: ['', [Validators.required]],
                mul_selector: ['', [Validators.required]],
                date_time: ['', [Validators.required]],
                date: ['', [Validators.required]],
                stringN: ['', [Validators.required]],
                stringA: ['', [Validators.required]],
                StringM: ['', [Validators.required]],
                Mul_String: ['', [Validators.required]],
                mixed: ['', [Validators.required]],
                plat_Id: ['', [Validators.required]],
            });
        }
        get submitR() {
            return this.getR.controls;
        }
        get DelR() {
            return this.delR.controls;
        }
        get PostRes() {
            return this.postR.controls;
        }
        get PutRes() {
            return this.putR.controls;
        }
        resourceclick(event) {
            if (event == 1) {
                this.getresource = true;
            }
            if (event == 2) {
                this.createResouce = true;
            }
            if (event == 3) {
                this.delResouce = true;
            }
            if (event == 4) {
                this.updateRes = true;
            }
        }
        cancelclick(event) {
            if (event == 1) {
                this.getresource = false;
            }
            if (event == 2) {
                this.createResouce = false;
            }
            if (event == 3) {
                this.delResouce = false;
            }
            if (event == 4) {
                this.updateRes = false;
            }
        }
        /*------------Sumit Forms----------*/
        SubmitResourceData(getR) {
            if (this.getR.invalid) {
                this.getR.controls['plat_id'].markAsTouched();
                this.getR.controls['plat'].markAsTouched();
                this.GetRes = false;
            }
            else {
                this.GetRes = true;
                var u1 = '';
                var u2 = '';
                if (this.getR.value) {
                    u1 = '&plat_id=' + this.getR.value.plat_id;
                    u2 = 'plat=' + this.getR.value.plat;
                }
                this.RGet = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.RGet1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        CreResource(postR) {
            if (this.postR.invalid) {
                this.postR.controls['plat'].markAsTouched();
                this.postR.controls['sup_email'].markAsTouched();
                this.postR.controls['email'].markAsTouched();
                this.postR.controls['mul_email'].markAsTouched();
                this.postR.controls['selector'].markAsTouched();
                this.postR.controls['mul_selector'].markAsTouched();
                this.postR.controls['date_time'].markAsTouched();
                this.postR.controls['date'].markAsTouched();
                this.postR.controls['stringN'].markAsTouched();
                this.postR.controls['stringA'].markAsTouched();
                this.postR.controls['StringM'].markAsTouched();
                this.postR.controls['Mul_String'].markAsTouched();
                this.postR.controls['mixed'].markAsTouched();
                this.CreateRes = false;
            }
            else {
                this.CreateRes = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                var u8 = '';
                var u9 = '';
                var u10 = '';
                var u11 = '';
                if (this.postR.value) {
                    u1 = 'plat=' + this.postR.value.plat;
                    u2 = '&sup_email=' + this.postR.value.sup_email;
                    u3 = '&email=' + this.postR.value.email;
                    u4 = '&mul_email=' + this.postR.value.mul_email;
                    u5 = '&date_time=' + this.postR.value.date_time;
                    u6 = '&date=' + this.postR.value.date;
                    u7 = '&stringN=' + this.postR.value.stringN;
                    u8 = '&stringA=' + this.postR.value.stringA;
                    u9 = '&StringM=' + this.postR.value.StringM;
                    u10 = '&Mul_String=' + this.postR.value.Mul_String;
                    u11 = '&mixed=' + this.postR.value.mixed;
                }
                this.CreRes = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + '' + u9 + '' + u10 + '' + u11 + ' -H "accept: application/json"';
                this.CreRes1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + '' + u9 + '' + u10 + '' + u11 + '';
            }
        }
        DeleteResource(delR) {
            if (this.delR.invalid) {
                this.delR.controls['plat_id'].markAsTouched();
                this.delR.controls['plat'].markAsTouched();
                this.DelRes = false;
            }
            else {
                this.DelRes = true;
                var u1 = '';
                var u2 = '';
                if (this.delR.value) {
                    u1 = '&plat_id=' + this.delR.value.plat_id;
                    u2 = 'plat=' + this.delR.value.plat;
                }
                this.DRes = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + ' -H "accept: application/json"';
                this.DRes1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '';
            }
        }
        UpdateResoruce(putR) {
            if (this.putR.invalid) {
                this.putR.controls['plat'].markAsTouched();
                this.putR.controls['sup_email'].markAsTouched();
                this.putR.controls['email'].markAsTouched();
                this.putR.controls['mul_email'].markAsTouched();
                this.putR.controls['selector'].markAsTouched();
                this.putR.controls['mul_selector'].markAsTouched();
                this.putR.controls['date_time'].markAsTouched();
                this.putR.controls['date'].markAsTouched();
                this.putR.controls['stringN'].markAsTouched();
                this.putR.controls['stringA'].markAsTouched();
                this.putR.controls['StringM'].markAsTouched();
                this.putR.controls['Mul_String'].markAsTouched();
                this.putR.controls['mixed'].markAsTouched();
                this.putR.controls['plat_Id'].markAsTouched();
                this.UpdRes = false;
            }
            else {
                this.UpdRes = true;
                var u1 = '';
                var u2 = '';
                var u3 = '';
                var u4 = '';
                var u5 = '';
                var u6 = '';
                var u7 = '';
                var u8 = '';
                var u9 = '';
                var u10 = '';
                var u11 = '';
                var u12 = '';
                if (this.putR.value) {
                    u1 = 'plat=' + this.putR.value.plat;
                    u2 = '&sup_email=' + this.putR.value.sup_email;
                    u3 = '&email=' + this.putR.value.email;
                    u4 = '&mul_email=' + this.putR.value.mul_email;
                    u5 = '&date_time=' + this.putR.value.date_time;
                    u6 = '&date=' + this.putR.value.date;
                    u7 = '&stringN=' + this.putR.value.stringN;
                    u8 = '&stringA=' + this.putR.value.stringA;
                    u9 = '&StringM=' + this.putR.value.StringM;
                    u10 = '&Mul_String=' + this.putR.value.Mul_String;
                    u11 = '&mixed=' + this.putR.value.mixed;
                    u12 = '&plat_Id=' + this.putR.value.plat_Id;
                }
                this.UpRes = 'curl -X GET "https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + '' + u9 + '' + u10 + '' + u11 + '' + u12 + ' -H "accept: application/json"';
                this.UpRes1 = 'https://api.oaf-x.com/user/?' + u1 + '' + u2 + '' + u3 + '' + u4 + '' + u5 + '' + u6 + '' + u7 + '' + u8 + '' + u9 + '' + u10 + '' + u11 + '' + u12 + '';
            }
        }
    };
    ResourceDocComponent = __decorate([
        Component({
            selector: 'app-resource-doc',
            templateUrl: './resource-doc.component.html',
            styleUrls: ['./resource-doc.component.scss']
        })
    ], ResourceDocComponent);
    return ResourceDocComponent;
})();
export { ResourceDocComponent };
//# sourceMappingURL=resource-doc.component.js.map