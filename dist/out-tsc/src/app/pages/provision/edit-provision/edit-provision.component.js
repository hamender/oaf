import { __decorate } from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { PopupHistoryComponent } from '../../../home/popup-history/popup-history.component';
let EditProvisionComponent = /** @class */ (() => {
    let EditProvisionComponent = class EditProvisionComponent {
        constructor(activeModal, fb, elem, provisionservice, modalService, platformservice, customerservice) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.elem = elem;
            this.provisionservice = provisionservice;
            this.modalService = modalService;
            this.platformservice = platformservice;
            this.customerservice = customerservice;
            this.editP = new EventEmitter();
            this.dprovision = new EventEmitter();
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = true;
            this.compArray = [];
            this.parm = {};
            this.parms = {};
            this.delbtnDisable = true;
            this.globalName = true;
            this.indextypevalueP = '';
            this.indextypevalueS = '';
            this.showerrmsg = false;
        }
        ngOnInit() {
            if (localStorage.getItem('Attribute')) {
                let attribute = JSON.parse(localStorage.getItem('Attribute'));
                this.globall = attribute.groups[0];
                console.log(this.globall);
                if (this.globall == 'Administrator') {
                    this.globalName = false;
                    console.log(true);
                }
                else if (this.globall == 'Standard') {
                    console.log("true11");
                    this.globalName = false;
                }
                else {
                    this.globalName = true;
                }
            }
            this.userForm = this.fb.group({
                platform: [{ value: '', disabled: true }, [Validators.required]],
                service: [{ value: '', disabled: true }, [Validators.required]],
            });
            if (this.serv && this.plat) {
                this.serviceValues = this.serv.split('__');
                this.platformValues = this.plat.split('__');
                let sid = this.sid;
                this.indextypevalueP = this.platformValues[1];
                this.indextypevalueS = this.serviceValues[1];
                console.log(this.indextypevalueP);
                console.log(this.indextypevalueS);
                this.getAllServices(this.platformValues[0]);
                this.getProvision(this.serviceValues[0], this.platformValues[0], sid, this.platformValues[1]);
            }
            if (this.vserv && this.vplat) {
                this.serviceValues = this.vserv.split('__');
                this.platformValues = this.vplat.split('__');
                let sid = this.sid;
                this.indextypevalueP = this.platformValues[1];
                this.indextypevalueS = this.serviceValues[1];
                this.getAllServices(this.platformValues[0]);
                this.getProvision(this.serviceValues[0], this.platformValues[0], sid, this.platformValues[1]);
            }
            this.getPlatformList();
        }
        getProvision(serv, plat, index, indexp) {
            let param = {
                'platform': plat,
                'service': serv,
                'id': index
            };
            this.provisionservice.getProvision(param).subscribe(res => {
                this.showload = false;
                console.log(res);
                if (res.statusCode == 200) {
                    this.updateForm(plat, serv);
                    let headArray = [];
                    this.itype = index;
                    this.platname = plat;
                    this.pindexValue = res.Metadata[indexp];
                    console.log(this.itype);
                    console.log(this.pindexValue);
                    for (let i of Object.keys(res.Metadata)) {
                        if (i != 'services') {
                            headArray.push(i);
                        }
                    }
                    let valArray = [];
                    for (let val of Object.values(res.Metadata)) {
                        if (!Array.isArray(val)) {
                            valArray.push(val);
                        }
                    }
                    var result = valArray.reduce(function (result, field, index) {
                        result[headArray[index]] = field;
                        return result;
                    }, {});
                    this.compArray = result;
                }
            });
        }
        submitForm(userForm) {
            this.showload = true;
            let platformId = this.platformId.nativeElement.value;
            let serviceId = this.serviceId.nativeElement.value;
            let field = this.field.nativeElement.value;
            let elements = this.elem.nativeElement.querySelectorAll('.option_input');
            elements.forEach(element => {
                this.parms[element.name] = element.value;
            });
            let param = {
                'platform': platformId,
                'service': serviceId,
                'payload': this.parms
            };
            //console.log(param)
            this.provisionservice.update(param).subscribe(res => {
                //console.log(res)
                this.showload = false;
                var rid = 'Request Id';
                //console.log(res[rid])
                if (res.StatusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Your request has been validated and queued, you will received an email notification when fulfilled',
                        html: "Your request id is:" + ' ' + '<span data-id="' + res[rid] + '" class="SwalBtn1 customSwalBtn">' + res[rid] + '</span>',
                        showConfirmButton: true,
                        confirmButtonColor: '#707070',
                    });
                    this.activeModal.close();
                    this.editP.emit(res);
                    var self = this;
                    $(document).on('click', '.SwalBtn1', function () {
                        //Some code 1
                        var iddd = $('.SwalBtn1').data('id');
                        //console.log(iddd)
                        self.gethostiry(iddd);
                        Swal.clickConfirm();
                    });
                }
                else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Your request could not be validated and has been cancelled',
                        showConfirmButton: true,
                        confirmButtonColor: '#707070',
                    });
                }
            });
        }
        gethostiry(id) {
            console.log(id);
            const ngmodalRef = this.modalService.open(PopupHistoryComponent, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'customhistory'
            });
            ngmodalRef.componentInstance.pid = id;
            ngmodalRef.componentInstance.loc = 'provision';
        }
        updateForm(data, data1) {
            this.userForm.patchValue({ platform: data });
            this.userForm.patchValue({ service: data1 });
            this.delbtnDisable = false;
        }
        closeform() {
            this.activeModal.close();
        }
        delServprovision(servid, platid) {
            this.parm['platform'] = this.platformValues[0];
            this.parm['service'] = this.serviceValues[0];
            this.parm[this.serviceValues[1]] = servid;
            this.parm[this.platformValues[1]] = platid;
            console.log(this.parm);
            Swal.fire({
                title: 'Are you sure you want to delete this?',
                text: "This action cannot be undone",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    this.provisionservice.deleteProvision(this.parm).subscribe(res => {
                        console.log(res);
                        var rid = 'Request_Id';
                        if (res.StatusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Your request has been validated and queued, ' + '<br>' + 'you will received an email notification when fulfilled',
                                html: "Your request id is:" + ' ' + '<span data-id="' + res[rid] + '" class="SwalBtn1 customSwalBtn">' + res[rid] + '</span>',
                                showConfirmButton: true,
                                confirmButtonColor: '#707070',
                            });
                            this.dprovision.emit(res);
                            this.activeModal.close();
                            var self = this;
                            $(document).on('click', '.SwalBtn1', function () {
                                //Some code 1
                                var iddd = $('.SwalBtn1').data('id');
                                //console.log(iddd)
                                self.gethostiry(iddd);
                                Swal.clickConfirm();
                            });
                        }
                        else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Your request could not be validated and has been cancelled',
                                showConfirmButton: true,
                                confirmButtonColor: '#707070',
                            });
                        }
                    });
                }
            });
        }
        getPlatformList() {
            this.platformservice.list().subscribe(res => {
                // this.showload = false
                this.platformList = res.Metadata;
            });
        }
        getAllServices(plat) {
            let params = {
                'platform': plat
            };
            this.customerservice.getServiceListbyID(params).subscribe(res => {
                this.serviceList = res.Services;
            });
        }
        textValidation(event) {
            console.log(event.target.value);
            if (event.target.value == '') {
                this.showerrmsg = true;
            }
            else {
                this.showerrmsg = false;
            }
        }
    };
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "serv", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "plat", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "vserv", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "vplat", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "sid", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "pid", void 0);
    __decorate([
        ViewChild('platformId')
    ], EditProvisionComponent.prototype, "platformId", void 0);
    __decorate([
        ViewChild('serviceId')
    ], EditProvisionComponent.prototype, "serviceId", void 0);
    __decorate([
        ViewChild('field')
    ], EditProvisionComponent.prototype, "field", void 0);
    __decorate([
        Output()
    ], EditProvisionComponent.prototype, "editP", void 0);
    __decorate([
        Output()
    ], EditProvisionComponent.prototype, "dprovision", void 0);
    EditProvisionComponent = __decorate([
        Component({
            selector: 'app-edit-provision',
            templateUrl: './edit-provision.component.html',
            styleUrls: ['./edit-provision.component.scss']
        })
    ], EditProvisionComponent);
    return EditProvisionComponent;
})();
export { EditProvisionComponent };
//# sourceMappingURL=edit-provision.component.js.map