import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CustomValidators } from '../../../shared/validator/custom-validators';
let AddCustomerComponent = /** @class */ (() => {
    let AddCustomerComponent = class AddCustomerComponent {
        constructor(fb, customerservice, activeModal) {
            this.fb = fb;
            this.customerservice = customerservice;
            this.activeModal = activeModal;
            this.phErr = false;
            this.emErr = false;
            this.unErr = false;
            this.loadergif = '../../../assets/images/turn.gif';
            this.showload = false;
            this.aCustomer = new EventEmitter();
        }
        ngOnInit() {
            this.customerForm = this.fb.group({
                username: ['', [Validators.required]],
                password: ['', [Validators.required, Validators.minLength(14), CustomValidators.strong]],
                name: ['', [Validators.required]],
                phone: ['', [Validators.required, Validators.pattern('^\\+[1-9]\\d{1,14}$')]],
                customerEmail1: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
                mfa: ['', [Validators.required]],
                type: ['', [Validators.required]],
                callurl: [''],
            });
        }
        closeSelectModal(customerForm) {
            if (this.customerForm.invalid) {
                this.customerForm.controls['username'].markAsTouched();
                this.customerForm.controls['name'].markAsTouched();
                this.customerForm.controls['password'].markAsTouched();
                this.customerForm.controls['phone'].markAsTouched();
                this.customerForm.controls['customerEmail1'].markAsTouched();
                this.customerForm.controls['type'].markAsTouched();
                this.customerForm.controls['mfa'].markAsTouched();
            }
            else {
                this.showload = true;
                let ph = customerForm.value.phone.toString();
                this.mf = (customerForm.value.mfa == 'yes' ? true : false);
                let params = {
                    'username': customerForm.value.username,
                    'password': customerForm.value.password,
                    'name': customerForm.value.name,
                    'phone_number': ph,
                    'email': customerForm.value.customerEmail1,
                    'enable_mfa': this.mf,
                    'user_group': customerForm.value.type,
                    'callback_url': customerForm.value.callurl
                };
                console.log(params);
                this.customerservice.create(params).subscribe(res => {
                    this.showload = false;
                    if (res.statusCode == 200) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'User Created.',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        this.customerForm.reset();
                        this.activeModal.close();
                        this.aCustomer.emit(res);
                    }
                    else {
                        var err = res.data.Errors[0].split(':')[1];
                        if (err.includes('phone_number')) {
                            console.log('sss');
                            this.phErr = true;
                            this.unErr = false;
                            this.emErr = false;
                            this.showerrMsg = err;
                        }
                        else if (err.includes('User account')) {
                            console.log('sss');
                            this.unErr = true;
                            this.phErr = false;
                            this.emErr = false;
                            this.showerrMsg = err;
                        }
                        else if (err.includes('email')) {
                            console.log('sss');
                            this.emErr = true;
                            this.phErr = false;
                            this.unErr = false;
                            this.showerrMsg = err;
                        }
                        /*   Swal.fire({
                             position: 'center',
                             icon: 'error',
                             title: res.data.Errors[0].split(':')[1],
                             showConfirmButton: false,
                             timer: 1500
                           })*/
                    }
                });
            }
        }
        get customerF() {
            return this.customerForm.controls;
        }
        closeform() {
            this.activeModal.close();
        }
        genPass() {
            const ranPassword = this.randomPassword(14);
            console.log(ranPassword);
            this.customerForm.patchValue({ password: ranPassword });
        }
        randomPassword(len) {
            var length = (len) ? (len) : (10);
            var string = "abcdefghijklmnopqrstuvwxyz"; //to upper 
            var numeric = '0123456789';
            var punctuation = '!@#$%^&*()_+~`|}{[]\:;?><,./-=';
            var password = "";
            var character = "";
            var crunch = true;
            while (password.length < length) {
                var entity1 = Math.ceil(string.length * Math.random() * Math.random());
                var entity2 = Math.ceil(numeric.length * Math.random() * Math.random());
                var entity3 = Math.ceil(punctuation.length * Math.random() * Math.random());
                var hold = string.charAt(entity1);
                hold = (password.length % 2 == 0) ? (hold.toUpperCase()) : (hold);
                character += hold;
                character += numeric.charAt(entity2);
                character += punctuation.charAt(entity3);
                password = character;
            }
            password = password.split('').sort(function () { return 0.5 - Math.random(); }).join('');
            return password.substr(0, len);
        }
    };
    __decorate([
        Output()
    ], AddCustomerComponent.prototype, "aCustomer", void 0);
    AddCustomerComponent = __decorate([
        Component({
            selector: 'app-add-customer',
            templateUrl: './add-customer.component.html',
            styleUrls: ['./add-customer.component.scss']
        })
    ], AddCustomerComponent);
    return AddCustomerComponent;
})();
export { AddCustomerComponent };
//# sourceMappingURL=add-customer.component.js.map