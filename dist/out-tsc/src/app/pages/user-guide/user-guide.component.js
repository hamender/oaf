import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
let UserGuideComponent = /** @class */ (() => {
    let UserGuideComponent = class UserGuideComponent {
        constructor(utilService, router, myElement) {
            this.utilService = utilService;
            this.router = router;
            this.myElement = myElement;
            this.faArrowRight = faArrowRight;
            this.model = {};
            this.currentStep = 1;
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'User Guide' }];
            this.utilService.changeBreadcrumb(breadcrumb);
        }
        ngOnInit() {
        }
        goToLink(val) {
            console.log(val);
            $(document).ready(function () {
                $('.umanage button').click();
            });
        }
    };
    __decorate([
        ViewChild('acc')
    ], UserGuideComponent.prototype, "accordion", void 0);
    UserGuideComponent = __decorate([
        Component({
            selector: 'app-user-guide',
            templateUrl: './user-guide.component.html',
            styleUrls: ['./user-guide.component.scss']
        })
    ], UserGuideComponent);
    return UserGuideComponent;
})();
export { UserGuideComponent };
//# sourceMappingURL=user-guide.component.js.map