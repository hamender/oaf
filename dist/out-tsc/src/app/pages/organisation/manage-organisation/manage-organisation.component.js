import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ManageOrganisationComponent = /** @class */ (() => {
    let ManageOrganisationComponent = class ManageOrganisationComponent {
        constructor() { }
        ngOnInit() {
        }
    };
    ManageOrganisationComponent = __decorate([
        Component({
            selector: 'app-manage-organisation',
            templateUrl: './manage-organisation.component.html',
            styleUrls: ['./manage-organisation.component.scss']
        })
    ], ManageOrganisationComponent);
    return ManageOrganisationComponent;
})();
export { ManageOrganisationComponent };
//# sourceMappingURL=manage-organisation.component.js.map