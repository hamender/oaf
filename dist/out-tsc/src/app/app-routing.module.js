import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
const routes = [
    /*{
      path: '',
      component: LoginComponent,
      children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      ]
    },*/
    /*{
      path: 'login',
      component: LoginComponent,
    },
    {
        path : 'dashboard',
        component: DashboardComponent,
      canActivate: [AuthGuard]
    },
    {
      path : 'edit-profile',
      component: EditProfileComponent,
      canActivate: [AuthGuard]
    },
    {
      path : 'add-customer',
      component: AddCustomerComponent,
      canActivate: [AuthGuard]
    },
    {
      path : 'manage-user',
      component: ManageUserComponent,
      canActivate: [AuthGuard]
      
    },*/
    { path: '', loadChildren: () => import('./oaf/oaf.module').then(m => m.OafModule) },
    { path: '**', redirectTo: '/login' }
];
let AppRoutingModule = /** @class */ (() => {
    let AppRoutingModule = class AppRoutingModule {
    };
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
})();
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map