import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
let FooterComponent = /** @class */ (() => {
    let FooterComponent = class FooterComponent {
        constructor(modalService, auth) {
            this.modalService = modalService;
            this.auth = auth;
            this.href = 'https://app.oaf-x.com';
            this.imagePath = '../../../assets/images/blue-logo.png';
        }
        ngOnInit() {
        }
        showForm(content) {
            if (this.auth.isLoggedIn()) {
                console.log(this.auth.isLoggedIn());
                this.open(content);
            }
            else {
                console.log(this.auth.isLoggedIn());
            }
        }
        open(content) {
            this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg' }).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
        getDismissReason(reason) {
            if (reason === ModalDismissReasons.ESC) {
                return 'by pressing ESC';
            }
            else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
                return 'by clicking on a backdrop';
            }
            else {
                return `with: ${reason}`;
            }
        }
    };
    FooterComponent = __decorate([
        Component({
            selector: 'app-footer',
            templateUrl: './footer.component.html',
            styleUrls: ['./footer.component.scss']
        })
    ], FooterComponent);
    return FooterComponent;
})();
export { FooterComponent };
//# sourceMappingURL=footer.component.js.map