import { __decorate } from "tslib";
import { Component } from '@angular/core';
//import { ViewHistoryComponent } from '../../home/view-history/view-history.component';
import { PopupHistoryComponent } from '../../home/popup-history/popup-history.component';
import { faEllipsisV, faBell } from '@fortawesome/free-solid-svg-icons';
let HeaderComponent = /** @class */ (() => {
    let HeaderComponent = class HeaderComponent {
        constructor(authservice, 
        //private activeModal: NgbActiveModal,
        router, userIdle, customerservice, modalService, proservice) {
            this.authservice = authservice;
            this.router = router;
            this.userIdle = userIdle;
            this.customerservice = customerservice;
            this.modalService = modalService;
            this.proservice = proservice;
            this.faEllipsisV = faEllipsisV;
            this.faBell = faBell;
            this.texttt = false;
            this.imagePath = '../../../assets/images/myra-logo.png';
            this.globalName = true;
            this.showbell = false;
            // reqArray = []
            this.reqArray = [];
            this.searchResult = [];
            this.searchInput = '';
            this.content = new Array();
            let p = {
                'records': 'all'
            };
            this.counter = 0;
            this.proservice.provisionlist(p).subscribe(res => {
                var i = res.Records.length + 1;
                for (let items of res.Records) {
                    i--;
                    var rid = 'Request Id';
                    this.reqArray.push({ id: items[rid], len: i });
                    this.searchResult = this.reqArray;
                }
                // console.log(this.reqArray)
            });
            if (localStorage.getItem('Attribute')) {
                let attribute = JSON.parse(localStorage.getItem('Attribute'));
                //console.log(attribute)
                this.globall = attribute.groups[0];
                this.Name = attribute.name;
                if (this.globall == 'Administrator') {
                    this.globalName = false;
                }
                else if (this.globall == 'Standard') {
                    this.globalName = false;
                }
                else {
                    this.globalName = true;
                }
            }
        }
        ngOnInit() {
            this.userIdle.startWatching();
            this.logoutAcc();
            this.userIdle.onTimerStart().subscribe(count => console.log(count));
            this.userIdle.onTimeout().subscribe(() => console.log('Time is up!'));
            if (this.authservice.isAdmin()) {
                this.isAdmin = true;
            }
            else {
                this.isAdmin = false;
            }
        }
        /*  getData(){
            console.log(this.counter + 'dat size'+this.reqArray.length)
        
            for(let i=this.counter+1;i<this.reqArray.length;i++)
            {
            this.content.push(this.reqArray[i]);
            if(i%10==0) break;
            }
            this.counter+=10;
        
          }*/
        fetchSeries(value) {
            if (value === '') {
                return this.searchResult = [];
            }
            this.searchResult = this.reqArray.filter(function (series) {
                return series.id.toLowerCase().startsWith(value.target.value.toLowerCase());
            });
        }
        getSingleHistroynyID(id) {
            //console.log(id)
            const ngmodalRef = this.modalService.open(PopupHistoryComponent, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'customhistory'
            });
            ngmodalRef.componentInstance.id = id;
            ngmodalRef.componentInstance.loc = 'header';
        }
        logout() {
            let params = { "username": this.uName };
            localStorage.clear();
            localStorage.removeItem('timer');
            localStorage.removeItem('ACCESS_TOKEN');
            localStorage.removeItem('Attribute');
            localStorage.removeItem('Token');
            this.authservice.logout(params).subscribe(res => {
                //console.log(res)
                this.router.navigate(['/']);
                /*     if(res.errorMessage  == "'username'"){
                      console.log(true)
                     setInterval(function(){
                      $('.close').trigger('click');
                
                   }, 1000)
                   }else{
                    console.log(false)
                   }*/
            });
        }
        logoutAcc() {
            this.userIdle.onTimerStart().subscribe(count => {
                //console.log('no')
                if (count == 45) {
                    // console.log('yes')
                    this.logout();
                }
            });
        }
        /*closeform(){
         this.activeModal.close();
       }*/
        mUser() {
            this.router.navigate(['//manage-user']);
        }
        viewMore() {
            this.router.navigate(['//view-history']);
        }
        mConnector() {
            this.router.navigate(['//manage-connector']);
        }
        editprofile() {
            this.router.navigate(['//manage-profile']);
        }
        LogoClick() {
            this.router.navigate(['/']);
        }
        userGuide() {
            this.router.navigate(['//user-guide']);
        }
        apiDoc() {
            this.router.navigate(['//api-documentation']);
        }
        mOrgan() {
            this.router.navigate(['//manage-organisation']);
        }
    };
    HeaderComponent = __decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.scss']
        })
    ], HeaderComponent);
    return HeaderComponent;
})();
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map