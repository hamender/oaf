import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';
let AppComponent = /** @class */ (() => {
    let AppComponent = class AppComponent {
        constructor(router, auth, customerservice) {
            this.router = router;
            this.auth = auth;
            this.customerservice = customerservice;
            this.title = 'wm-oaf';
            this.sessionOut = new EventEmitter();
        }
        isHomeRoute() {
            return this.router.url === '/';
        }
        ngOnInit() {
            if (localStorage.getItem('ACCESS_TOKEN')) {
                const atoken = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
                this.val = atoken.AccessToken;
                this.username = atoken.UserAttributes.username;
                if (this.username == 'undefined' || this.username == '') {
                    console.log('dd');
                    localStorage.clear();
                    localStorage.removeItem('timer');
                    localStorage.removeItem('ACCESS_TOKEN');
                    localStorage.removeItem('Attribute');
                    localStorage.removeItem('Token');
                }
            }
            setInterval(() => {
                this.autologout();
            }, 10000);
        }
        ngOnDestroy() {
        }
        autologout() {
            const timer = JSON.parse(localStorage.getItem('timer'));
            if ((Date.now() > timer) && timer && this.val) {
                let p = {
                    'username': this.username
                };
                localStorage.clear();
                localStorage.removeItem('timer');
                localStorage.removeItem('ACCESS_TOKEN');
                localStorage.removeItem('Attribute');
                localStorage.removeItem('Token');
                Swal.fire({
                    icon: 'info',
                    title: 'Oops...',
                    text: 'Your session has expired, please log in again to continue',
                    footer: ''
                });
                this.auth.logout(p).subscribe(res => {
                    this.router.navigate(['/']);
                });
            }
        }
    };
    __decorate([
        Output()
    ], AppComponent.prototype, "sessionOut", void 0);
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        })
    ], AppComponent);
    return AppComponent;
})();
export { AppComponent };
//# sourceMappingURL=app.component.js.map