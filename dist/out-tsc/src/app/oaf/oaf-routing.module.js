import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../home/login/login.component';
import { DashboardComponent } from '../home/dashboard/dashboard.component';
import { AuthGuard } from '../shared/guard/guard.module';
import { EditProfileComponent } from '../pages/profile/edit-profile/edit-profile.component';
import { AddCustomerComponent } from '../pages/customer/add-customer/add-customer.component';
import { ManageUserComponent } from '../pages/manage-user/manage-user.component';
import { ManageConnectorComponent } from '../pages/connector/manage-connector/manage-connector.component';
import { ApiDocumentationComponent } from '../pages/api-documentation/api-documentation.component';
import { ViewHistoryComponent } from '../home/view-history/view-history.component';
/*const routes: Routes = [{ path: '', component: NyanComponent }];*/
import { UserGuideComponent } from '../pages/user-guide/user-guide.component';
import { ManageOrganisationComponent } from '../pages/organisation/manage-organisation/manage-organisation.component';
import { ChartComponent } from '../home/chart/chart.component';
const routes = [
    {
        path: '',
        component: LoginComponent,
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            },
        ]
    }, {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'manage-profile',
        component: EditProfileComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add-customer',
        component: AddCustomerComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'manage-user',
        component: ManageUserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'manage-connector',
        component: ManageConnectorComponent,
        canActivate: [AuthGuard]
    }, {
        path: 'manage-organisation',
        component: ManageOrganisationComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'api-documentation',
        component: ApiDocumentationComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'view-history',
        component: ViewHistoryComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'user-guide',
        component: UserGuideComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'charts',
        component: ChartComponent,
        canActivate: [AuthGuard]
    }
];
let OafRoutingModule = /** @class */ (() => {
    let OafRoutingModule = class OafRoutingModule {
    };
    OafRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], OafRoutingModule);
    return OafRoutingModule;
})();
export { OafRoutingModule };
//# sourceMappingURL=oaf-routing.module.js.map