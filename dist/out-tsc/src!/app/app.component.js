import { __decorate } from "tslib";
import { Component } from '@angular/core';
let AppComponent = /** @class */ (() => {
    let AppComponent = class AppComponent {
        constructor(router, auth, customerservice) {
            this.router = router;
            this.auth = auth;
            this.customerservice = customerservice;
            this.title = 'wm-oaf';
        }
        isHomeRoute() {
            return this.router.url === '/';
        }
        ngOnInit() {
            if (localStorage.getItem('ACCESS_TOKEN')) {
                const atoken = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
                this.val = atoken.AccessToken;
                this.username = atoken.UserAttributes.username;
            }
            setInterval(() => {
                this.autologout();
            }, 10000);
        }
        ngOnDestroy() {
        }
        autologout() {
            const timer = JSON.parse(localStorage.getItem('timer'));
            if ((Date.now() > timer) && timer && this.val) {
                let p = {
                    'username': this.username
                };
                this.auth.logout(p).subscribe(res => {
                    this.router.navigate(['/']);
                    localStorage.clear();
                    localStorage.removeItem('timer');
                    localStorage.removeItem('ACCESS_TOKEN');
                    localStorage.removeItem('Attribute');
                    localStorage.removeItem('Token');
                });
            }
        }
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        })
    ], AppComponent);
    return AppComponent;
})();
export { AppComponent };
//# sourceMappingURL=app.component.js.map