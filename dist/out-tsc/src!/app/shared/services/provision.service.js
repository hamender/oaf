import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ProvisionService = /** @class */ (() => {
    let ProvisionService = class ProvisionService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'provision', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        getProvision(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provision', { headers: finalHeaders, params });
            }
        }
        deleteProvision(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'provision', options);
            }
        }
        getProvisionList(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'provision/list', { headers: finalHeaders, params });
            }
        }
        update(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'provision', params, { headers: finalHeaders });
            }
        }
    };
    ProvisionService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ProvisionService);
    return ProvisionService;
})();
export { ProvisionService };
//# sourceMappingURL=provision.service.js.map