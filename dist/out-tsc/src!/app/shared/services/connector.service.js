import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ConnectorService = /** @class */ (() => {
    let ConnectorService = class ConnectorService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'connector', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'connector', params, { headers: finalHeaders });
            }
        }
        get(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'connector', { headers: finalHeaders, params });
            }
        }
        deleteConnector(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'connector', options);
            }
        }
        list() {
            if (this.IDToken1) {
                return this.http.get(this.url + 'connector/list', {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
    };
    ConnectorService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ConnectorService);
    return ConnectorService;
})();
export { ConnectorService };
//# sourceMappingURL=connector.service.js.map