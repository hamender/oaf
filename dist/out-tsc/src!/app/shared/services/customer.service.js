import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let CustomerService = /** @class */ (() => {
    let CustomerService = class CustomerService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            //let token = this.auth.getToken();
            if (this.IDToken1) {
                return this.http.post(this.url + 'user', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            if (this.IDToken1) {
                return this.http.get(this.url + 'user/list', {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        getUserbyname(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'user', { headers: finalHeaders, params });
            }
        }
        updateUser(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'user', params, { headers: finalHeaders });
            }
        }
        getCurrentUser() {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'currentuser', { headers: finalHeaders });
            }
        }
        deleteUser(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'user', options);
            }
        }
        loginHistory(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'loginhistory', { headers: finalHeaders, params });
            }
        }
        changepassword(params) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'changepassword', params, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
    };
    CustomerService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CustomerService);
    return CustomerService;
})();
export { CustomerService };
//# sourceMappingURL=customer.service.js.map