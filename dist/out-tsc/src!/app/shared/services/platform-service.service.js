import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let PlatformServiceService = /** @class */ (() => {
    let PlatformServiceService = class PlatformServiceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'platform', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            if (this.IDToken1) {
                return this.http.get(this.url + 'platform', {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        summary1() {
            if (this.IDToken1) {
                return this.http.get(this.url + 'summary_report/', {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'platform', params, { headers: finalHeaders });
            }
        }
        getPlatformbyID(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'platform', { headers: finalHeaders, params });
            }
        }
        deletePlatform(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'platform', options);
            }
        }
    };
    PlatformServiceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PlatformServiceService);
    return PlatformServiceService;
})();
export { PlatformServiceService };
//# sourceMappingURL=platform-service.service.js.map