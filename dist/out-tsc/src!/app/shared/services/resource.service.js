import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let ResourceService = /** @class */ (() => {
    let ResourceService = class ResourceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'resource', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(data) {
            if (this.IDToken1) {
                return this.http.put(this.url + 'resource', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        /*	deleteResource(params: any):Observable<any> {
          return this.http.delete(this.url+'resource/', params)
          }*/
        deleteResource(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'resource', options);
            }
        }
        getResource(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'resource', { headers: finalHeaders, params });
            }
        }
        list(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'resource/list', { headers: finalHeaders, params });
            }
        }
    };
    ResourceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], ResourceService);
    return ResourceService;
})();
export { ResourceService };
//# sourceMappingURL=resource.service.js.map