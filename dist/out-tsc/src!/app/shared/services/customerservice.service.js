import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
let CustomerserviceService = /** @class */ (() => {
    let CustomerserviceService = class CustomerserviceService {
        constructor(http, auth) {
            this.http = http;
            this.auth = auth;
            this.url = environment.url;
            this.IDToken = this.auth.getToken();
            this.IDToken1 = this.auth.getToken1();
            this.AcToken = this.auth.getAccToken();
        }
        create(data) {
            if (this.IDToken1) {
                return this.http.post(this.url + 'service', data, {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        list() {
            if (this.IDToken1) {
                return this.http.get(this.url + 'service', {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                });
            }
        }
        update(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.put(this.url + 'service', params, { headers: finalHeaders });
            }
        }
        getservicebyID(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'service', { headers: finalHeaders, params });
            }
        }
        getServiceListbyID(params) {
            if (this.IDToken1) {
                let finalHeaders = new HttpHeaders({
                    'Authorization': this.IDToken1,
                    'Accept': '*/*',
                });
                return this.http.get(this.url + 'service/list', { headers: finalHeaders, params });
            }
        }
        deleteService(data) {
            if (this.IDToken1) {
                const options = {
                    headers: new HttpHeaders({
                        'Authorization': this.IDToken1,
                        'Accept': '*/*',
                    }),
                    body: data,
                };
                return this.http.delete(this.url + 'service', options);
            }
        }
    };
    CustomerserviceService = __decorate([
        Injectable({
            providedIn: 'root'
        })
    ], CustomerserviceService);
    return CustomerserviceService;
})();
export { CustomerserviceService };
//# sourceMappingURL=customerservice.service.js.map