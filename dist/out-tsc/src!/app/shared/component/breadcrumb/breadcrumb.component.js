import { __decorate } from "tslib";
import { Component } from '@angular/core';
let BreadcrumbComponent = /** @class */ (() => {
    let BreadcrumbComponent = class BreadcrumbComponent {
        constructor(utilService) {
            this.utilService = utilService;
            this.breadcrumb = [];
        }
        ngOnInit() {
            this.utilService.currentBreadcrumb
                .subscribe(breadcrumb => {
                this.breadcrumb = breadcrumb;
            });
        }
        goback() {
            this.utilService.changeMessage('table1');
        }
    };
    BreadcrumbComponent = __decorate([
        Component({
            selector: 'app-breadcrumb',
            templateUrl: './breadcrumb.component.html',
            styleUrls: ['./breadcrumb.component.scss']
        })
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
})();
export { BreadcrumbComponent };
//# sourceMappingURL=breadcrumb.component.js.map