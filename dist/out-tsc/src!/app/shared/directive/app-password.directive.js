import { __decorate } from "tslib";
import { Directive } from '@angular/core';
let AppPasswordDirective = /** @class */ (() => {
    let AppPasswordDirective = class AppPasswordDirective {
        constructor(el) {
            this.el = el;
            this._shown = false;
            this.setup();
        }
        toggle(span) {
            this._shown = !this._shown;
            if (this._shown) {
                this.el.nativeElement.setAttribute('type', 'text');
                span.innerHTML = '<span class=""><i class="fa fa-eye-slash fa-fw"></i></span>';
            }
            else {
                this.el.nativeElement.setAttribute('type', 'password');
                span.innerHTML = '<span class=""><i class="fa fa-eye fa-fw"></i></span>';
            }
        }
        setup() {
            const parent = this.el.nativeElement.parentNode;
            const span = document.createElement('span');
            span.innerHTML = `<span class=""><i class="fa fa-eye fa-fw"></i></span>`;
            span.addEventListener('click', (event) => {
                this.toggle(span);
            });
            parent.appendChild(span);
        }
    };
    AppPasswordDirective = __decorate([
        Directive({
            selector: '[appAppPassword]'
        })
    ], AppPasswordDirective);
    return AppPasswordDirective;
})();
export { AppPasswordDirective };
//# sourceMappingURL=app-password.directive.js.map