import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { AddPlatformComponent } from '../../pages/platform/add-platform/add-platform.component';
import { AddServiceComponent } from '../../pages/services/add-service/add-service.component';
import { AddResourceComponent } from '../../pages/resource/add-resource/add-resource.component';
import { AddInstanceComponent } from '../../pages/instance/add-instance/add-instance.component';
import { ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { EditPlatformComponent } from '../../pages/platform/edit-platform/edit-platform.component';
import { EditServiceComponent } from '../../pages/services/edit-service/edit-service.component';
import { PlateformComponent } from '../../pages/platform/plateform/plateform.component';
import { EditProvisionComponent } from '../../pages/provision/edit-provision/edit-provision.component';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import 'datatables.net';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
let DashboardComponent = /** @class */ (() => {
    let DashboardComponent = class DashboardComponent {
        constructor(modalService, platformservice, customerservice, auth, utilService, resourceservice, provisionservice) {
            this.modalService = modalService;
            this.platformservice = platformservice;
            this.customerservice = customerservice;
            this.auth = auth;
            this.utilService = utilService;
            this.resourceservice = resourceservice;
            this.provisionservice = provisionservice;
            this.obj$ = of(1).pipe(delay(1500));
            this.selectedVal = '';
            this.selectedValService = '';
            this.PlatformTable = false;
            this.ServiceTable = false;
            this.tableplat = false;
            this.backbtn = false;
            this.loader = true;
            this.pdrop = false;
            this.dataArray = [];
            this.resHeading = [];
            this.columns = [];
            this.servHeading = [];
            this.servValue = [];
            this.serList = [];
            this.resData = [];
            //stelectedPlatform: any
            //anyPlat: boolean = false;
            //anyService: boolean = false;
            this.commaSep = false;
            this.plist = [];
            this.tableData = [];
            //globl:any;
            this.customize = true;
            this.initialbread = true;
            this.dtOptions = {};
            this.dtTrigger = new Subject();
            // if(this.initialbread){
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Dashboard' }];
            this.utilService.changeBreadcrumb(breadcrumb);
            // }
            this.utilService.currentMessage.subscribe(message => {
                if (message == 'table1') {
                    $('#table1').DataTable().clear().destroy();
                    $('#table2').DataTable().clear().destroy();
                    $('#table3').DataTable().clear().destroy();
                    let platform = this.selectedVal.split('__');
                    const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: platform[0] }];
                    this.utilService.changeBreadcrumb(breadcrumb);
                    this.selected();
                    this.ServiceTable = false;
                }
            });
        }
        getPlatformList() {
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            this.tableData = [];
            this.plist = [];
            this.platformservice.summary1().subscribe(res1 => {
                console.log(res1);
                if (res1.StatusCode == 200) {
                    for (let i of res1.Platforms) {
                        this.plist.push(i);
                    }
                    this.platformList = res1.Platforms;
                    this.tableData = this.plist;
                    setTimeout(() => {
                        this.dtTableLoad('table3', 'A4');
                    }, 100);
                    this.tableplat = true;
                }
            });
            /*this.platformservice.list().subscribe(res=>{
             
              for(let item of res.Metadata){
              let pp = {
                'platform': item.Platform
              }
                
                this.customerservice.getServiceListbyID(pp).subscribe(res=>{
                   
                this.plist[item.Platform+'__'+item.Index_type] = new Array();
                
                for(let itm of res.Services){
          
                  if(item.Platform == itm.Platform){
                   this.plist[item.Platform+'__'+item.Index_type].push(
                    {'platform':itm.Platform,
                    'service':itm.Service,
                    'index':itm.Index_type,
                    'indexp':item.Index_type
                  })
                  }
            
                }
                });
              }
              
              
          
              this.tableData = this.plist;
              console.log(this.tableData)
              setTimeout(()=>{
                this.dtTableLoad('table3','A4')
              },5000);
            })*/
        }
        sortArray(arr) {
            return arr.sort((a, b) => (a.Service > b.Service) ? 1 : -1);
        }
        onClick(event, type) {
            let spl = event.data.split('__');
            if (type == 'edit') {
                const ngmodalRef2 = this.modalService.open(PlateformComponent, {
                    size: 'md',
                    backdrop: 'static'
                });
                ngmodalRef2.componentInstance.platId = spl[0];
                ngmodalRef2.componentInstance.passEntry.subscribe((rdata) => {
                    if (rdata.statusCode == 200) {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                });
                ngmodalRef2.componentInstance.dplatEntry.subscribe((rdata1) => {
                    if (rdata1.statusCode == 200) {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                });
            }
            if (type == 'view') {
                const ngmodalRef2 = this.modalService.open(PlateformComponent, {
                    size: 'md',
                    backdrop: 'static'
                });
                ngmodalRef2.componentInstance.viewId = spl[0];
            }
            /*if(type == 'del'){
              this.delPlatform(spl[0])
            }*/
        }
        onClick1(event, type) {
            let spl = event.data.split('__');
            if (type == 'edit') {
                const ngmodalRef4 = this.modalService.open(EditServiceComponent, {
                    size: 'lg',
                    backdrop: 'static'
                });
                ngmodalRef4.componentInstance.servId = spl[0];
                ngmodalRef4.componentInstance.platId = spl[1];
                ngmodalRef4.componentInstance.eService.subscribe((rdata) => {
                    if (rdata.statusCode == 200) {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                });
                ngmodalRef4.componentInstance.dservEntry.subscribe((rdata1) => {
                    if (rdata1.statusCode == 200) {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                });
            }
            if (type == 'view') {
                const ngmodalRef4 = this.modalService.open(EditServiceComponent, {
                    size: 'lg',
                    backdrop: 'static'
                });
                ngmodalRef4.componentInstance.viewId = spl[0];
                ngmodalRef4.componentInstance.viewpId = spl[1];
            }
            /*if(type == 'del'){
              this.delService(spl[0], spl[1])
            }*/
        }
        openPlatform() {
            const ngmodalRef = this.modalService.open(AddPlatformComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.passEntry.subscribe((rdata) => {
                console.log(rdata);
                if (rdata.statusCode == 200) {
                    $('#table1').DataTable().clear().destroy();
                    this.tableplat = false;
                    this.getPlatformList();
                }
            });
        }
        openService() {
            const ngmodalRef2 = this.modalService.open(AddServiceComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef2.componentInstance.aService.subscribe((rdata) => {
                console.log(rdata);
                if (rdata.statusCode == 200) {
                    if (this.selectedVal) {
                        //$('#table1').DataTable().clear().destroy();
                        this.selected();
                    }
                    else if (this.selectedValService) {
                        this.selectedservice();
                    }
                    else {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                }
            });
        }
        resouce() {
            const ngmodalRef31 = this.modalService.open(AddResourceComponent, {
                size: 'lg',
                backdrop: 'static',
                windowClass: 'custom-class'
            });
            ngmodalRef31.componentInstance.aResource.subscribe((rdata) => {
                console.log(rdata);
                if (rdata.StatusCode == 200) {
                    // $('#table1').DataTable().clear().destroy();
                    if (this.selectedVal) {
                        this.selected();
                    }
                    else {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                }
            });
        }
        instance() {
            const ngmodalRef3 = this.modalService.open(AddInstanceComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef3.componentInstance.aInstance.subscribe((rdata) => {
                console.log(rdata);
                if (rdata.StatusCode == 200) {
                    if (this.selectedVal) {
                        this.selected();
                    }
                    else if (this.selectedValService) {
                        // $('#table2').DataTable().clear().destroy();
                        this.selectedservice();
                    }
                    else {
                        this.tableplat = false;
                        this.getPlatformList();
                    }
                }
            });
        }
        ngOnInit() {
            /* setTimeout(()=>{
             //this.loader = false;
               this.dtTableLoad('table3','A4')
              },5000);*/
            //
            setTimeout(() => {
                this.getPlatformList();
            }, 2000);
        }
        selected(event, indx) {
            if (event || indx) {
                this.selectedVal = event + '__' + indx;
            }
            this.PlatformTable = false;
            this.globalplabel = '';
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            let platt = this.selectedVal;
            // this.pdrop = false
            this.tableplat = false;
            let spl = platt.split('__');
            this.globalplabel = this.selectedVal;
            //this.stelectedPlatform = spl[0];
            let params = {
                'platform': spl[0]
            };
            this.PlatformTable = false;
            this.dataArray = [];
            this.resData = [];
            this.resHeading = [];
            this.columns = [];
            this.selectedValService = '';
            this.selectedColumn = [];
            const breadcrumb1 = [{ name: 'Home', link: '/', type: 'plat' }, { name: spl[0] }];
            this.utilService.changeBreadcrumb(breadcrumb1);
            this.resourceservice.list(params).subscribe(res => {
                console.log(res);
                if (res.statusCode == 200) {
                    this.pdrop = false;
                    this.ServiceTable = false;
                    this.customize = false;
                    let indexArray = [];
                    let ii = 0;
                    let countArray = [];
                    for (let item of res.Metadata) {
                        let selctindx = [];
                        for (let i of Object.keys(item)) {
                            ii++;
                            if (!this.resHeading.includes(i)) {
                                this.resHeading.push(i);
                                this.columns.push({ name: i, position: ii, tableCls: 'table1' });
                                selctindx.push(ii);
                                this.selectedColumn = selctindx;
                            }
                        }
                    }
                    for (let item of res.Metadata) {
                        let itype = item[spl[1]];
                        let resData1 = [];
                        for (let v of this.resHeading) {
                            if (item[v]) {
                                if (Array.isArray(item[v])) {
                                    let servArray = [];
                                    for (let v1 of item[v]) {
                                        servArray.push(v1.service_name + '(' + v1.count + ')');
                                    }
                                    resData1.push(servArray);
                                }
                                else {
                                    resData1.push(item[v]);
                                }
                            }
                            else {
                                resData1.push('--');
                            }
                        }
                        this.resData.push({ 'rec': resData1, 'id': itype, 'plat': spl[0], 'index': spl[1] });
                        //this.anyPlat = true;
                        let lsize = 'A4';
                        if (this.resHeading.length > 8) {
                            lsize = 'A0';
                        }
                        else {
                            lsize = 'A4';
                        }
                        setTimeout(() => {
                            $('#table1').DataTable().destroy();
                            //this.loader = false;
                            this.dtTableLoad('table1', lsize);
                            this.PlatformTable = true;
                        }, 100);
                    }
                }
                else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    if (this.pdrop == true) {
                        this.tableplat = true;
                        this.pdrop = false;
                        setTimeout(() => {
                            this.dtTableLoad('table3', 'A4');
                            this.PlatformTable = false;
                        }, 100);
                    }
                    this.customize = true;
                }
            });
            this.ServInstance(spl[0]);
        }
        counter(i) {
            return new Array(i);
        }
        platResource(plat, index) {
        }
        ServInstance(plat) {
            console.log(plat);
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            let params = {
                'platform': plat
            };
            this.customerservice.getServiceListbyID(params).subscribe(res => {
                this.serviceList = res.Services;
            });
        }
        selectedservice(event, platt, index) {
            this.pdrop = false;
            this.ServiceTable = false;
            if (event) {
                this.selectedValService = event + '__' + index;
                this.pdrop = true;
            }
            console.log(this.selectedValService);
            this.globalslabel = '';
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            this.ServiceTable = false;
            if (platt) {
                this.selectedVal = platt;
            }
            let plat = this.selectedVal;
            let spl = plat.split('__');
            this.globalslabel = this.selectedValService;
            let servSplit = this.selectedValService.split('__');
            //this.selectServ = servSplit[0];
            //this.getListService(servSplit[0], spl[0],servSplit[1])
            let params = {
                'platform': spl[0],
                'service': servSplit[0],
            };
            this.selectedColumn = [];
            this.columns = [];
            this.servHeading = [];
            const breadcrumb = [{ name: 'Home', link: '/', type: 'serv' }, { name: spl[0], link: 'plat' }, { name: servSplit[0] }];
            this.utilService.changeBreadcrumb(breadcrumb);
            this.provisionservice.getProvisionList(params).subscribe(res => {
                this.PlatformTable = false;
                this.customize = false;
                if (res.statusCode == 200) {
                    this.pdrop == false;
                    this.servValue = [];
                    for (let item of res.Metadata) {
                        let iii = 0;
                        this.selectedColumn = [];
                        this.columns = [];
                        let selctindx = [];
                        for (let i of Object.keys(item)) {
                            iii++;
                            if (!this.servHeading.includes(i)) {
                                this.servHeading.push(i);
                                this.columns.push({ name: i, position: iii, tableCls: 'table2' });
                                selctindx.push(iii);
                                this.selectedColumn = selctindx;
                            }
                        }
                    }
                    for (let item of res.Metadata) {
                        let sArray = [];
                        console.log(this.servHeading);
                        for (let v of this.servHeading) {
                            if (item[v]) {
                                sArray.push(item[v]);
                            }
                            else {
                                sArray.push('--');
                            }
                        }
                        this.indx = item[servSplit[1]];
                        this.servValue.push({ 'val': sArray, 'plat': spl[0], 'serv': servSplit[0], 'index': this.indx });
                        let lsize = 'A4';
                        if (this.servHeading.length > 8) {
                            lsize = 'A0';
                        }
                        else {
                            lsize = 'A4';
                        }
                        setTimeout(() => {
                            $('#table2').DataTable().destroy();
                            //this.loader = false;
                            this.dtTableLoad('table2', lsize);
                            this.ServiceTable = true;
                        }, 100);
                    }
                }
                else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: res.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    //this.selectServ = '';
                    //this.stelectedPlatform = '';
                    //this.anyService = false;
                    //this.anyPlat = false;
                    this.customize = true;
                    if (this.pdrop == true) {
                        this.tableplat = true;
                        this.ServiceTable = false;
                        setTimeout(() => {
                            this.dtTableLoad('table3', 'A4');
                        }, 100);
                    }
                    if (this.pdrop == false) {
                        this.tableplat = false;
                        this.ServiceTable = false;
                        /*setTimeout(()=>{
                         this.dtTableLoad('table2')
                        },500);*/
                    }
                }
            });
            //this.anyService = true;
        }
        platformSelection(event, index) {
            console.log(event);
            console.log(index);
            this.globalplabel = '';
            //this.loader = true;
            this.pdrop = true;
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            //this.stelectedPlatform = event;
            this.globalplabel = event + '__' + index;
            //this.selectedVal = event;
            this.selected(event, index);
            this.tableplat = false;
            // this.selected(this.globalplabel);
        }
        serviceSelection(event, plat, index, indexp) {
            //this.loader = true;
            //this.pdrop = true
            $('#table1').DataTable().clear().destroy();
            $('#table2').DataTable().clear().destroy();
            $('#table3').DataTable().clear().destroy();
            this.globalplabel = plat + '__' + indexp;
            this.globalslabel = event + '__' + index;
            this.ServInstance(plat);
            this.selectedservice(event, this.globalplabel, index);
            this.tableplat = false;
            //this.selected(this.globalplabel)
        }
        getListService(serv, plat, index) {
        }
        backBtn() {
            this.tableplat = true;
            this.PlatformTable = false;
            this.backbtn = false;
        }
        checkValue(values, position, tableClass) {
            const index = this.selectedColumn.findIndex(column => column === position);
            if (index >= 0) {
                this.selectedColumn.splice(index, 1);
            }
            else {
                this.selectedColumn.push(position);
            }
            const table = $('.table1').DataTable();
            const column = table.column(position);
            column.visible(!column.visible());
        }
        open(content) {
            this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg' }).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }
        getDismissReason(reason) {
            if (reason === ModalDismissReasons.ESC) {
                return 'by pressing ESC';
            }
            else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
                return 'by clicking on a backdrop';
            }
            else {
                return `with: ${reason}`;
            }
        }
        /*  delPlatform(platformValue){
            let params= {
              'platform': platformValue
            }
            Swal.fire({
          title: 'Are you sure you want to delete this?',
          text: "This action cannot be undone",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#707070',
          cancelButtonColor: '#4f9a0b',
          confirmButtonText: 'Delete'
        }).then((result) => {
          if (result.value) {
            this.platformservice.deletePlatform(params).subscribe(res=> {
              if(res.statusCode == 200){
              Swal.fire({
                  position: 'center',
                  icon: 'success',
                  title: 'Platform Deleted!!',
                  showConfirmButton: false,
                  timer: 1500
                })
              setTimeout(()=>{
                this.tableplat = false;
              this.getPlatformList();
              },1500);
              
              }
            })
            
          }
        })
            
          }*/
        /*  delService(serv,plat){
           let params= {
             'platform': plat,
             'service': serv
           }
           Swal.fire({
           title: 'Are you sure you want to delete this?',
           text: "This action cannot be undone",
           icon: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#707070',
           cancelButtonColor: '#4f9a0b',
           confirmButtonText: 'Delete'
         }).then((result) => {
         if (result.value) {
           this.customerservice.deleteService(params).subscribe(res=> {
             if(res.statusCode == 200){
             Swal.fire({
                 position: 'center',
                 icon: 'success',
                 title: res.Message,
                 showConfirmButton: false,
                 timer: 1500
               })
             setTimeout(()=>{
             this.getPlatformList();
             },1500);
             }
           })
           
         }
       })
           
         }*/
        editPlatform(index, plat, indextype) {
            const ngmodalRef3 = this.modalService.open(EditPlatformComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef3.componentInstance.platId = plat;
            ngmodalRef3.componentInstance.resId = index;
            ngmodalRef3.componentInstance.index = indextype;
            ngmodalRef3.componentInstance.passEntry.subscribe((rdata) => {
                if (rdata.StatusCode == 200) {
                    this.selected();
                }
            });
            ngmodalRef3.componentInstance.delRes.subscribe((rdata1) => {
                console.log(rdata1);
                if (rdata1.StatusCode == 200) {
                    this.selected();
                }
            });
        }
        viewPlatform(index, plat, indextype) {
            const ngmodalRef3 = this.modalService.open(EditPlatformComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef3.componentInstance.viewId = plat;
            ngmodalRef3.componentInstance.viewresId = index;
            ngmodalRef3.componentInstance.index = indextype;
        }
        editProvision(plat, serv, index) {
            const ngmodalRef4 = this.modalService.open(EditProvisionComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef4.componentInstance.plat = this.globalplabel;
            ngmodalRef4.componentInstance.serv = this.globalslabel;
            ngmodalRef4.componentInstance.sid = index;
            ngmodalRef4.componentInstance.editP.subscribe((rdata1) => {
                if (rdata1.StatusCode == 200) {
                    this.selectedservice();
                }
            });
            ngmodalRef4.componentInstance.dprovision.subscribe((rdata1) => {
                if (rdata1.StatusCode == 200) {
                    this.selectedservice();
                }
            });
        }
        viewProvision(plat, serv, index) {
            const ngmodalRef5 = this.modalService.open(EditProvisionComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef5.componentInstance.vserv = this.globalslabel;
            ngmodalRef5.componentInstance.vplat = this.globalplabel;
            ngmodalRef5.componentInstance.sid = index;
        }
        dtTableLoad(tableId = '', pageS = 'A4') {
            var pagetitle = this.selectedVal + ' ' + this.selectedValService;
            let ptitle = '';
            if (this.selectedVal == '' && this.selectedValService == '') {
                ptitle = 'All Platform & Services Data';
            }
            if (this.selectedVal && this.selectedValService == '') {
                ptitle = this.selectedVal.split('__')[0];
            }
            if (this.selectedValService && this.selectedVal == '') {
                ptitle = 'All Services';
            }
            if (this.selectedValService && this.selectedVal) {
                ptitle = this.selectedVal.split('__')[0] + ' >> ' + this.selectedValService.split('__')[0];
            }
            let dbOption = {};
            dbOption = {
                initComplete: function (settings, json) {
                    //$('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    //$('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    $('.dt-button').detach().appendTo('.download-btns ul');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    }),
                        $('body').find('.download-btns .dt-button').css({
                            'background': 'none',
                            'padding': '0.02rem 0.8rem',
                            'margin': '0px',
                            'border': '0px',
                            'font-size': '11px',
                            'line-height': '20px',
                            'width': '100%',
                            'display': 'block',
                            'text-align': 'left'
                        });
                },
                dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        title: function () {
                            return ptitle;
                        },
                        text: 'Download - PDF',
                        orientation: 'landscape',
                        pageSize: pageS,
                        titleAttr: 'PDF',
                        width: '100%'
                        /*exportOptions: {
                          columns: ':not(:first-child)',
                        },*/
                    },
                    {
                        extend: 'csv',
                        text: 'Download - CSV'
                    },
                    {
                        extend: 'excel',
                        text: 'Download - EXCEL'
                    }
                ],
                scrollCollapse: true,
                scrollX: true,
                //scrollY: 600,
                paging: true,
                select: true,
                bFilter: true,
                bInfo: true,
                ordering: true,
                lengthMenu: [10, 25, 50, 100, 500, 1000, 2000]
            };
            $('#' + tableId).DataTable(dbOption);
        }
        ngAfterViewInit() {
        }
    };
    __decorate([
        ViewChild(DataTableDirective, { static: false })
    ], DashboardComponent.prototype, "dtElement", void 0);
    DashboardComponent = __decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.scss']
        })
    ], DashboardComponent);
    return DashboardComponent;
})();
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map