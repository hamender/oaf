import { __decorate } from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
let EditServiceComponent = /** @class */ (() => {
    let EditServiceComponent = class EditServiceComponent {
        constructor(activeModal, fb, customerservice, elem) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.customerservice = customerservice;
            this.elem = elem;
            this.eService = new EventEmitter();
            this.dservEntry = new EventEmitter();
            this.dis = true;
            this.showerror = false;
            this.show_new_fields = false;
            this.delbtnDisable = true;
            this.arr = [];
            this.compArray = [];
            this.valArray = [];
            this.nARray = [];
            this.nARray1 = [];
            this.parm = {};
        }
        ngOnInit() {
            this.ServForm = this.fb.group({
                platform: ['', [Validators.required]],
                service: ['', [Validators.required]],
                services: this.fb.array([]),
            });
            if (this.servId && this.platId) {
                this.getService(this.servId, this.platId);
            }
            if (this.viewId && this.viewpId) {
                this.getService(this.viewId, this.viewpId);
            }
            this.arr = [
                { 'ftype': 'field-e', 'type': 'email', 'exacttype': 'Email' },
                { 'ftype': 'field-s', 'type': 'selector', 'exacttype': 'Selector' },
                { 'ftype': 'field-sn', 'type': 'text', 'exacttype': 'Selector + Add New' },
                { 'ftype': 'field-d', 'type': 'date', 'exacttype': 'Date' },
                { 'ftype': 'field-dt', 'type': 'datetime', 'exacttype': 'Date and Time' },
                { 'ftype': 'field-n', 'type': 'text', 'exacttype': 'String-n' },
                { 'ftype': 'field-a', 'type': 'text', 'exacttype': 'String-a' },
                { 'ftype': 'field-m', 'type': 'text', 'exacttype': 'String-m' },
                { 'ftype': 'field-me', 'type': 'text', 'exacttype': 'Emails (Separated by comma)' },
                { 'ftype': 'field-ms', 'type': 'text', 'exacttype': 'Multi selector (Separated by comma)' },
                { 'ftype': 'field-mm', 'type': 'text', 'exacttype': 'Multi Mixed (Separated by comma)' },
                { 'ftype': 'field-x', 'type': 'text', 'exacttype': 'Mixed Encrypted  (hashed)' },
                { 'ftype': 'Support Email', 'type': 'email', 'exacttype': 'Support Email' },
            ];
        }
        submitForm(ServForm) {
            if (this.addressGroup.invalid) {
                this.showerror = true;
            }
            else {
                this.showerror = false;
                let Arr1 = [];
                let Arr2 = [];
                let Arr3 = [];
                let Arr4 = [];
                let combine_new_field_array = [];
                let combine_old_field_array = [];
                for (let arr of ServForm.value.services) {
                    if (arr.serviceFields == 'field-s' || arr.serviceFields == 'field-ms' || arr.serviceFields == 'field-sn') {
                        Arr3.push({ 'name': arr.ServiceFieldValue, 'values': arr.ServiceFieldValue1 });
                    }
                    else {
                        Arr3.push({ 'name': arr.ServiceFieldValue });
                    }
                    Arr4.push({ 'type': arr.serviceFields, });
                }
                combine_new_field_array = Arr4.map((item, i) => Object.assign({}, item, Arr3[i]));
                console.log(combine_new_field_array);
                let platformId = this.platformId.nativeElement.value;
                let serviceId = this.serviceId.nativeElement.value;
                this.parm['platform'] = platformId;
                this.parm['service'] = serviceId;
                let elements = this.elem.nativeElement.querySelectorAll('.option_input1');
                elements.forEach(element => {
                    console.log(element.id);
                    if (element.name === 'field-s') {
                        let e1 = this.elem.nativeElement.querySelectorAll('.' + element.id);
                        e1.forEach(ele1 => {
                            Arr2.push({ 'type': element.name, 'name': element.value, 'values': ele1.value });
                        });
                    }
                    if (element.name === 'field-ms') {
                        let e2 = this.elem.nativeElement.querySelectorAll('.' + element.id);
                        e2.forEach(ele2 => {
                            Arr2.push({ 'type': element.name, 'name': element.value, 'values': ele2.value });
                        });
                    }
                    if (element.name === 'field-sn') {
                        let e3 = this.elem.nativeElement.querySelectorAll('.' + element.id);
                        e3.forEach(ele3 => {
                            Arr2.push({ 'type': element.name, 'name': element.value, 'values': ele3.value });
                        });
                    }
                    if (element.name != 'field-sn' && element.name != 'field-ms' && element.name != 'field-s') {
                        Arr2.push({ 'type': element.name, 'name': element.value });
                    }
                });
                //combine_old_field_array =  Arr1.map((item, i) => Object.assign({}, item, Arr2[i]))
                console.log(Arr2);
                this.parm['fields'] = Arr2.concat(combine_new_field_array);
                console.log(this.parm);
                this.customerservice.update(this.parm).subscribe(res => {
                    if (res.statusCode == 200) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: res.Message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        this.activeModal.close();
                        this.eService.emit(res);
                    }
                });
            }
        }
        closeform() {
            this.activeModal.close();
        }
        get addressGroup() {
            return this.ServForm.get('services');
        }
        updateForm(data1, data2) {
            this.ServForm.patchValue({ platform: data1 });
            this.ServForm.patchValue({ service: data2 });
            this.pname = data1;
            this.sname = data2;
            this.delbtnDisable = false;
        }
        typeChange(event, i) {
            let val = i;
            if (this.nARray.includes(val)) {
                if (event.target.value != 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    const index = this.nARray.indexOf(val);
                    if (index !== -1) {
                        this.nARray.splice(index, 1);
                    }
                }
            }
            else {
                if (event.target.value == 'field-s' || event.target.value == 'field-ms' || event.target.value == 'field-sn') {
                    this.nARray.push(val);
                    console.log(this.nARray);
                }
            }
        }
        gettype(a) {
            for (let ar of this.arr) {
                if (a === ar.ftype) {
                    return ar.type;
                }
            }
        }
        getService(serv, plat) {
            let p = {
                'service': serv,
                'platform': plat
            };
            this.customerservice.getservicebyID(p).subscribe(res => {
                if (res.statusCode == 200) {
                    this.updateForm(plat, serv);
                    for (let item of res.items) {
                        console.log(item);
                        this.compArray = [];
                        this.oldLength = item.Fields.length;
                        for (let i of item.Fields) {
                            this.valArray = [];
                            console.log(i);
                            if (i.values != null) {
                                let str = i.values.replace(/,\s*$/, "");
                                this.valArray = str.split(",");
                            }
                            this.compArray.push({ 'type': i.type, 'name': i.name, 'val': this.valArray });
                        }
                    }
                    this.show_new_fields = true;
                }
            });
        }
        addNewAddressGroup() {
            const add = this.ServForm.get('services');
            add.push(this.fb.group({
                serviceFields: ['', [Validators.required]],
                ServiceFieldValue: ['', [Validators.required]],
                ServiceFieldValue1: [''],
            }));
        }
        deleteAddressGroup(index) {
            const add = this.ServForm.get('services');
            add.removeAt(index);
            if (this.nARray.includes(index)) {
                const index1 = this.nARray.indexOf(index);
                if (index1 !== -1) {
                    this.nARray.splice(index1, 1);
                }
            }
        }
        delServ(plat, serv) {
            console.log(plat);
            console.log(serv);
            let params = {
                'platform': plat,
                'service': serv
            };
            Swal.fire({
                title: 'Are you sure you want to delete this?',
                text: "This action cannot be undone",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    this.customerservice.deleteService(params).subscribe(res => {
                        if (res.statusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: res.Message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                            this.activeModal.close();
                            this.dservEntry.emit(res);
                            /*  setTimeout(()=>{
                              this.getPlatformList();
                              },1500);*/
                        }
                    });
                }
            });
        }
    };
    __decorate([
        Input()
    ], EditServiceComponent.prototype, "servId", void 0);
    __decorate([
        Input()
    ], EditServiceComponent.prototype, "platId", void 0);
    __decorate([
        Input()
    ], EditServiceComponent.prototype, "viewId", void 0);
    __decorate([
        Input()
    ], EditServiceComponent.prototype, "viewpId", void 0);
    __decorate([
        ViewChild('platformId')
    ], EditServiceComponent.prototype, "platformId", void 0);
    __decorate([
        ViewChild('serviceId')
    ], EditServiceComponent.prototype, "serviceId", void 0);
    __decorate([
        ViewChild('field')
    ], EditServiceComponent.prototype, "field", void 0);
    __decorate([
        Output()
    ], EditServiceComponent.prototype, "eService", void 0);
    __decorate([
        Output()
    ], EditServiceComponent.prototype, "dservEntry", void 0);
    EditServiceComponent = __decorate([
        Component({
            selector: 'app-edit-service',
            templateUrl: './edit-service.component.html',
            styleUrls: ['./edit-service.component.scss']
        })
    ], EditServiceComponent);
    return EditServiceComponent;
})();
export { EditServiceComponent };
//# sourceMappingURL=edit-service.component.js.map