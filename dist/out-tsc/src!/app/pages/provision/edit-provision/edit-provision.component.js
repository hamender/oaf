import { __decorate } from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
let EditProvisionComponent = /** @class */ (() => {
    let EditProvisionComponent = class EditProvisionComponent {
        constructor(activeModal, fb, elem, provisionservice) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.elem = elem;
            this.provisionservice = provisionservice;
            this.editP = new EventEmitter();
            this.dprovision = new EventEmitter();
            this.compArray = [];
            this.parm = {};
            this.parms = {};
            this.delbtnDisable = true;
        }
        ngOnInit() {
            this.userForm = this.fb.group({
                platform: ['', [Validators.required]],
                service: ['', [Validators.required]],
            });
            if (this.serv && this.plat) {
                this.serviceValues = this.serv.split('__');
                this.platformValues = this.plat.split('__');
                let sid = this.sid;
                this.getProvision(this.serviceValues[0], this.platformValues[0], sid, this.platformValues[1]);
            }
            if (this.vserv && this.vplat) {
                this.serviceValues = this.vserv.split('__');
                this.platformValues = this.vplat.split('__');
                let sid = this.sid;
                this.getProvision(this.serviceValues[0], this.platformValues[0], sid, this.platformValues[1]);
            }
        }
        getProvision(serv, plat, index, indexp) {
            let param = {
                'platform': plat,
                'service': serv,
                'id': index
            };
            this.provisionservice.getProvision(param).subscribe(res => {
                console.log(res);
                if (res.statusCode == 200) {
                    this.updateForm(plat, serv);
                    let headArray = [];
                    this.itype = index;
                    this.platname = plat;
                    this.pindexValue = res.Metadata[indexp];
                    console.log(this.itype);
                    console.log(this.pindexValue);
                    for (let i of Object.keys(res.Metadata)) {
                        if (i != 'services') {
                            headArray.push(i);
                        }
                    }
                    let valArray = [];
                    for (let val of Object.values(res.Metadata)) {
                        if (!Array.isArray(val)) {
                            valArray.push(val);
                        }
                    }
                    var result = valArray.reduce(function (result, field, index) {
                        result[headArray[index]] = field;
                        return result;
                    }, {});
                    this.compArray = result;
                }
            });
        }
        submitForm(userForm) {
            let platformId = this.platformId.nativeElement.value;
            let serviceId = this.serviceId.nativeElement.value;
            let field = this.field.nativeElement.value;
            let elements = this.elem.nativeElement.querySelectorAll('.option_input');
            elements.forEach(element => {
                this.parms[element.name] = element.value;
            });
            let param = {
                'platform': platformId,
                'service': serviceId,
                'payload': this.parms
            };
            console.log(param);
            this.provisionservice.update(param).subscribe(res => {
                console.log(res);
                if (res.StatusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Provision Updated!!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    this.activeModal.close();
                    this.editP.emit(res);
                }
            });
        }
        updateForm(data, data1) {
            this.userForm.patchValue({ platform: data });
            this.userForm.patchValue({ service: data1 });
            this.delbtnDisable = false;
        }
        closeform() {
            this.activeModal.close();
        }
        delServprovision(servid, platid) {
            this.parm['platform'] = this.platformValues[0];
            this.parm['service'] = this.serviceValues[0];
            this.parm[this.serviceValues[1]] = servid;
            this.parm[this.platformValues[1]] = platid;
            console.log(this.parm);
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.provisionservice.deleteProvision(this.parm).subscribe(res => {
                        if (res.StatusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Instance Deleted!!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            this.dprovision.emit(res);
                            this.activeModal.close();
                        }
                    });
                }
            });
        }
    };
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "serv", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "plat", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "vserv", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "vplat", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "sid", void 0);
    __decorate([
        Input()
    ], EditProvisionComponent.prototype, "pid", void 0);
    __decorate([
        ViewChild('platformId')
    ], EditProvisionComponent.prototype, "platformId", void 0);
    __decorate([
        ViewChild('serviceId')
    ], EditProvisionComponent.prototype, "serviceId", void 0);
    __decorate([
        ViewChild('field')
    ], EditProvisionComponent.prototype, "field", void 0);
    __decorate([
        Output()
    ], EditProvisionComponent.prototype, "editP", void 0);
    __decorate([
        Output()
    ], EditProvisionComponent.prototype, "dprovision", void 0);
    EditProvisionComponent = __decorate([
        Component({
            selector: 'app-edit-provision',
            templateUrl: './edit-provision.component.html',
            styleUrls: ['./edit-provision.component.scss']
        })
    ], EditProvisionComponent);
    return EditProvisionComponent;
})();
export { EditProvisionComponent };
//# sourceMappingURL=edit-provision.component.js.map