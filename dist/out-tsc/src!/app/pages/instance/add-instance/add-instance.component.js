import { __decorate } from "tslib";
import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2';
let AddInstanceComponent = /** @class */ (() => {
    let AddInstanceComponent = class AddInstanceComponent {
        constructor(activeModal, fb, platformservice, customerservice, resourceservice, provisionservice, datepipe, elem) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.platformservice = platformservice;
            this.customerservice = customerservice;
            this.resourceservice = resourceservice;
            this.provisionservice = provisionservice;
            this.datepipe = datepipe;
            this.elem = elem;
            this.aInstance = new EventEmitter();
            this.type = 'text';
            this.arr = [];
            this.selectedVal = '';
            this.selectedValService = '';
            this.typeArr = [];
            this.show = false;
            this.getAll = [];
            this.parm = {};
            this.dropArray = [];
            this.nARray = [];
            this.nARray1 = [];
            this.nARray2 = [];
            this.nARray3 = [];
            this.nARray4 = [];
            this.nARray5 = [];
            this.nARray6 = [];
            this.nARray7 = [];
            this.nARray8 = [];
            this.nARray9 = [];
            this.erremail = true;
            this.errnum = true;
            this.erralpha = true;
            this.errmixed = true;
            this.errmultimixed = true;
            this.errmulEmail = true;
            this.selectorN = true;
            this.dateErr = true;
            this.datetimeErr = true;
            this.multisErr = true;
            this.dropdownList = [];
            this.selectedItems = [];
            this.newfieldArray = [];
            this.checki = [];
            this.getPlatformList();
        }
        ngOnInit() {
            this.arr = [
                { 'ftype': 'field-e', 'type': 'email', 'exacttype': 'Email' },
                { 'ftype': 'field-s', 'type': 'selector', 'exacttype': 'Selector' },
                { 'ftype': 'field-sn', 'type': 'text', 'exacttype': 'Selector-SN' },
                { 'ftype': 'field-d', 'type': 'date', 'exacttype': 'Date' },
                { 'ftype': 'field-dt', 'type': 'datetime', 'exacttype': 'Date and Time' },
                { 'ftype': 'field-n', 'type': 'text', 'exacttype': 'String-n' },
                { 'ftype': 'field-a', 'type': 'text', 'exacttype': 'String-a' },
                { 'ftype': 'field-m', 'type': 'text', 'exacttype': 'String-m' },
                { 'ftype': 'field-me', 'type': 'email', 'exacttype': 'String-me' },
                { 'ftype': 'field-ms', 'type': 'selector', 'exacttype': 'String-ms' },
                { 'ftype': 'field-mm', 'type': 'text', 'exacttype': 'String-mm' },
                { 'ftype': 'field-x', 'type': 'password', 'exacttype': 'String-x' },
                { 'ftype': 'Support Email', 'type': 'email', 'exacttype': 'Support Email' },
            ];
            this.dropdownList = [];
            this.selectedItems = [];
            this.dropdownSettings = {
                singleSelection: false,
                idField: 'item_id',
                textField: 'item_text',
                selectAllText: 'Select All',
                unSelectAllText: 'UnSelect All',
                itemsShowLimit: 2,
                allowSearchFilter: true
            };
        }
        onItemSelect(item) {
        }
        onSelectAll(items) {
        }
        onDeSelect(items) {
        }
        emptyfunction() {
            this.erremail = true;
            this.errnum = true;
            this.erralpha = true;
            this.errmixed = true;
            this.errmultimixed = true;
            this.errmulEmail = true;
            this.selectorN = true;
            this.dateErr = true;
            this.datetimeErr = true;
            this.multisErr = true;
            this.nARray = [];
            this.nARray1 = [];
            this.nARray2 = [];
            this.nARray3 = [];
            this.nARray4 = [];
            this.nARray5 = [];
            this.nARray6 = [];
            this.nARray7 = [];
            this.nARray8 = [];
            this.nARray9 = [];
        }
        selectedPlat() {
            this.emptyfunction();
            let plat = this.platform.nativeElement.value;
            let plt = plat.split('__');
            let param = {
                'platform': plt[0],
            };
            let pusharr = [];
            this.show = false;
            this.resourceservice.list(param).subscribe(res => {
                for (let item of res.Metadata) {
                    pusharr.push(item[plt[1]]);
                }
                this.resourceList = pusharr;
            });
            this.customerservice.getServiceListbyID(param).subscribe(res => {
                this.serviceList = res.Services;
            });
        }
        selectedRes() {
            this.emptyfunction();
            let resource = this.resourceId.nativeElement.value;
            /*let plat = this.platform.nativeElement.value;
            let resource = this.resourceId.nativeElement.value;
            let params={
              'platform':plat,
              'id':resource
            }
            this.resourceservice.getResource(params).subscribe(res=>{
              console.log(res)
            })*/
        }
        selectedServ() {
            this.emptyfunction();
            let serv = this.service.nativeElement.value;
            let plat = this.platform.nativeElement.value;
            let pval = plat.split('__');
            let params = {
                'service': serv,
                'platform': pval[0]
            };
            this.customerservice.getservicebyID(params).subscribe(res => {
                for (let item of res.items) {
                    this.typeArr = [];
                    this.dropdownList = [];
                    for (let i of item.Fields) {
                        this.dropArray = [];
                        this.dropdownList = [];
                        if (i.values != null) {
                            let str = i.values.replace(/,\s*$/, "");
                            this.dropArray = str.split(",");
                        }
                        if (i.type == "field-ms") {
                            if (i.values.indexOf(',') > -1) {
                                let aaa = i.values.split(',');
                                this.dropdownList = [];
                                for (let i = 0; i < aaa.length; i++) {
                                    this.dropdownList.push({ item_id: i, item_text: aaa[i] });
                                }
                            }
                            else {
                                this.dropdownList.push({ item_id: i, item_text: i.values });
                            }
                        }
                        this.typeArr.push({ 'type': this.gettype(i.type), 'label': i.name, 'value': this.dropArray, 'exacttype': this.getExacttype(i.type), 'multi': this.dropdownList });
                    }
                }
                this.show = true;
                this.passList = this.typeArr;
            });
        }
        Addfield(event, i, label) {
            var options = '';
            Swal.fire({
                title: 'Add New Field',
                input: 'text',
                inputPlaceholder: 'Enter new field',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    if (!this.checki.includes(i)) {
                        if (!this.newfieldArray[label])
                            this.newfieldArray[label] = new Array();
                        this.newfieldArray[label].push(result.value);
                    }
                    else {
                        this.newfieldArray.push(result.value);
                    }
                    options += '<option value="' + result.value + '__' + label + '">' + result.value + '</option>';
                    $("#selectaddnew" + i).append(options);
                }
            });
        }
        checkValidate(event, type, i) {
            if (type == 'Selector') {
                let val = event.target.value.split('__');
            }
            if (type == 'Selector-SN') {
                if (event.target.value == '') {
                    this.selectorN = true;
                    if (!this.nARray5.includes(i)) {
                        this.nARray5.push(i);
                    }
                }
                else {
                    const index = this.nARray5.indexOf(i);
                    if (index !== -1) {
                        this.nARray5.splice(index, 1);
                    }
                }
            }
            if (type == 'Date') {
                if (event.target.value == '') {
                    this.dateErr = true;
                    if (!this.nARray6.includes(i)) {
                        this.nARray6.push(i);
                    }
                }
                else {
                    const index = this.nARray6.indexOf(i);
                    if (index !== -1) {
                        this.nARray6.splice(index, 1);
                    }
                }
            }
            if (type == 'datetime') {
                if (event.target.value == '') {
                    this.datetimeErr = true;
                    if (!this.nARray7.includes(i)) {
                        this.nARray7.push(i);
                    }
                }
                else {
                    const index = this.nARray7.indexOf(i);
                    if (index !== -1) {
                        this.nARray7.splice(index, 1);
                    }
                }
            }
            if (type == 'String-ms') {
                if (event.target.value == '') {
                    this.multisErr = true;
                    if (!this.nARray8.includes(i)) {
                        this.nARray8.push(i);
                    }
                }
                else {
                    const index = this.nARray8.indexOf(i);
                    if (index !== -1) {
                        this.nARray8.splice(index, 1);
                    }
                }
            }
            if (type == "Email" || type == "Support Email") {
                var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                var serchfind = regexp.test(event.target.value);
                if (!serchfind || event.target.value == '') {
                    this.erremail = true;
                    if (!this.nARray.includes(i)) {
                        this.nARray.push(i);
                    }
                }
                else {
                    //this.erremail = false;
                    const index = this.nARray.indexOf(i);
                    if (index !== -1) {
                        this.nARray.splice(index, 1);
                    }
                }
            }
            if (type == "String-me") {
                var regexp = new RegExp(/^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}),?\s*){1,3}$/);
                var serchfind = regexp.test(event.target.value);
                if (!serchfind || event.target.value == '') {
                    this.errmulEmail = true;
                    //this.retindex = i;
                    if (!this.nARray4.includes(i)) {
                        this.nARray4.push(i);
                    }
                }
                else {
                    const index = this.nARray4.indexOf(i);
                    if (index !== -1) {
                        this.nARray4.splice(index, 1);
                    }
                }
            }
            if (type == "String-n") {
                var reg = new RegExp('^[0-9]+$');
                var sfind = reg.test(event.target.value);
                if (!sfind) {
                    this.errnum = true;
                    if (!this.nARray1.includes(i)) {
                        this.nARray1.push(i);
                    }
                }
                else {
                    const index1 = this.nARray1.indexOf(i);
                    if (index1 !== -1) {
                        this.nARray1.splice(index1, 1);
                    }
                }
            }
            if (type == "String-a") {
                var reg1 = new RegExp(/^[A-Za-z]+$/);
                var sfind1 = reg1.test(event.target.value);
                if (!sfind1) {
                    this.erralpha = true;
                    if (!this.nARray2.includes(i)) {
                        this.nARray2.push(i);
                    }
                }
                else {
                    const index2 = this.nARray2.indexOf(i);
                    if (index2 !== -1) {
                        this.nARray2.splice(index2, 1);
                    }
                }
            }
            if (type == "String-m") {
                var reg2 = new RegExp(/^[A-Za-z0-9!@#$%^&*()]+$/);
                var sfind2 = reg2.test(event.target.value);
                if (!sfind2) {
                    this.errmixed = true;
                    if (!this.nARray3.includes(i)) {
                        this.nARray3.push(i);
                    }
                }
                else {
                    const index3 = this.nARray3.indexOf(i);
                    if (index3 !== -1) {
                        this.nARray3.splice(index3, 1);
                    }
                }
            }
            if (type == "String-mm") {
                var reg2 = new RegExp(/^[A-Za-z0-9!.,@#$%^&*()]+$/);
                var sfind2 = reg2.test(event.target.value);
                if (!sfind2) {
                    this.errmultimixed = true;
                    if (!this.nARray9.includes(i)) {
                        this.nARray9.push(i);
                    }
                }
                else {
                    const index9 = this.nARray9.indexOf(i);
                    if (index9 !== -1) {
                        this.nARray9.splice(index9, 1);
                    }
                }
            }
            if (this.nARray.length == 0) {
                this.erremail = false;
            }
            if (this.nARray1.length == 0) {
                this.errnum = false;
            }
            if (this.nARray2.length == 0) {
                this.erralpha = false;
            }
            if (this.nARray3.length == 0) {
                this.errmixed = false;
            }
            if (this.nARray4.length == 0) {
                this.errmulEmail = false;
            }
            if (this.nARray5.length == 0) {
                this.selectorN = false;
            }
            if (this.nARray6.length == 0) {
                this.dateErr = false;
            }
            if (this.nARray7.length == 0) {
                this.datetimeErr = false;
            }
            if (this.nARray8.length == 0) {
                this.multisErr = false;
            }
            if (this.nARray9.length == 0) {
                this.errmultimixed = false;
            }
            /*else{
              console.log(event.target.value)
          }
          }*/
        }
        gettype(a) {
            for (let ar of this.arr) {
                if (a === ar.ftype) {
                    return ar.type;
                }
            }
        }
        getExacttype(a) {
            for (let ar of this.arr) {
                if (a === ar.ftype) {
                    return ar.exacttype;
                }
            }
        }
        getIds($evt) {
            console.log($evt);
            let platform = this.platform.nativeElement.value;
            let resourceId = this.resourceId.nativeElement.value;
            let service = this.service.nativeElement.value;
        }
        selectedDrop() {
        }
        submitForm() {
            var res = [];
            for (var x in this.getAll) {
                let spl = x.split('__');
                this.getAll.hasOwnProperty(x) && res.push({ 'label': spl[1], 'val': this.getAll[x], 'typ': spl[2] });
            }
            this.typeData = res;
            //this.parm['platform'] = this.platform.nativeElement.value;
            //this.parm['service'] = this.service.nativeElement.value;
            for (let p of res) {
                if (p.typ == 'datetime') {
                    let latest_date = this.datepipe.transform(p.val, 'd/M/y hh:mm aa');
                    this.parm[p.label] = latest_date;
                }
                else if (p.typ == 'String-ms') {
                    let pushVal = [];
                    let plabel = p.label.replace(/['"]+/g, '');
                    if (p.val) {
                        for (let v of p.val) {
                            pushVal.push(v.item_text);
                        }
                        this.parm[plabel] = pushVal.toString();
                    }
                }
                else if (p.typ == 'date') {
                    let ldate = this.datepipe.transform(p.val, 'd/M/y');
                    this.parm[p.label] = ldate;
                }
                else if (p.typ == "Selector-SN") {
                    let plabel = p.label.replace(/['"]+/g, '');
                    let spl1 = p.val.split('__');
                    var nfVal = spl1[0];
                    var nftyp = spl1[1];
                    if (this.newfieldArray[spl1[1]]) {
                        if (this.newfieldArray[spl1[1]].includes(spl1[0])) {
                            this.parm[plabel] = this.newfieldArray[spl1[1]].toString();
                        }
                        else {
                            this.parm[plabel] = this.newfieldArray[spl1[1]].toString() + ',' + nfVal;
                        }
                    }
                    else {
                        this.parm[plabel] = nfVal;
                    }
                }
                else {
                    this.parm[p.label] = p.val;
                }
            }
            let pvalue = this.platform.nativeElement.value;
            let pval = pvalue.split('__');
            //let ddown = this.dropdown.nativeElement.value;
            let elements = this.elem.nativeElement.querySelectorAll('.option_input');
            elements.forEach(element => {
                var selctor = element.value.split('__');
                let plabel = selctor[1].replace(/['"]+/g, '');
                this.parm[plabel] = selctor[0];
            });
            this.parm[pval[1]] = this.resourceId.nativeElement.value;
            let params = {
                'service': this.service.nativeElement.value,
                'platform': pval[0],
                //'payload': this.resourceId.nativeElement.value,
                'payload': this.parm
            };
            this.provisionservice.create(params).subscribe(res => {
                /* if(res.StatusCode == 200 ){
                   Swal.fire({
                         position: 'center',
                         icon: 'success',
                         title: 'Provision Created.',
                         showConfirmButton: false,
                         timer: 1500
                       })
                       this.activeModal.close();
                       this.aInstance.emit(res)
                 }*/
            });
        }
        closeform() {
            this.activeModal.close();
        }
        getPlatformList() {
            this.platformservice.list().subscribe(res => {
                this.platformList = res.Metadata;
            });
        }
    };
    __decorate([
        ViewChild('service')
    ], AddInstanceComponent.prototype, "service", void 0);
    __decorate([
        ViewChild('resourceId')
    ], AddInstanceComponent.prototype, "resourceId", void 0);
    __decorate([
        ViewChild('platform')
    ], AddInstanceComponent.prototype, "platform", void 0);
    __decorate([
        ViewChild('dropdown')
    ], AddInstanceComponent.prototype, "dropdown", void 0);
    __decorate([
        Output()
    ], AddInstanceComponent.prototype, "aInstance", void 0);
    AddInstanceComponent = __decorate([
        Component({
            selector: 'app-add-instance',
            templateUrl: './add-instance.component.html',
            styleUrls: ['./add-instance.component.scss']
        })
    ], AddInstanceComponent);
    return AddInstanceComponent;
})();
export { AddInstanceComponent };
//# sourceMappingURL=add-instance.component.js.map