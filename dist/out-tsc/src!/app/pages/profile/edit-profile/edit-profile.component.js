import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CustomValidators } from '../../../shared/validator/custom-validators';
import 'datatables.net';
let EditProfileComponent = /** @class */ (() => {
    let EditProfileComponent = class EditProfileComponent {
        constructor(authservice, fb, utilService, customerservice) {
            this.authservice = authservice;
            this.fb = fb;
            this.utilService = utilService;
            this.customerservice = customerservice;
            this.ChngPass = false;
            this.edProfile = true;
            this.showhistory = false;
            this.userRoles = [];
            this.historyList = [];
            this.parm = {};
            this.disableType = false;
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Edit Profile' }];
            this.utilService.changeBreadcrumb(breadcrumb);
            this.AcToken = this.authservice.getAccToken();
        }
        ngOnInit() {
            this.resetPass = this.fb.group({
                username: ['', [Validators.required]],
                name: ['', [Validators.required, Validators.pattern('^[A-Za-z ]+$')]],
                email: ['', [Validators.required, Validators.email]],
                phone_number: ['', [Validators.required, Validators.pattern('^\\+[1-9]\\d{1,14}$')]],
                type: ['', [Validators.required]],
            });
            this.chngPass = this.fb.group({
                usrname: ['', [Validators.required]],
                oldpass: ['', [Validators.required, Validators.minLength(14)]],
                newpass: ['', [Validators.required, Validators.minLength(14), CustomValidators.strong]],
                cnfpass: ['', [Validators.required, Validators.minLength(14), CustomValidators.strong]],
            });
            if (this.authservice.isAdmin()) {
                this.userRoles = [{ name: 'Super-Admin', value: 'Super-Admin' },
                    { name: 'Administrator', value: 'Administrator' },
                    { name: 'Standard', value: 'Standard' }];
            }
            else {
                this.userRoles = [{ name: 'Administrator', value: 'Administrator' },
                    { name: 'Standard', value: 'Standard' }];
            }
            setTimeout(() => {
                this.customerservice.getCurrentUser().subscribe(res => {
                    if (res.statusCode == 200) {
                        this.patchValue(res.data.UserProfile);
                        if (res.data.UserProfile.groups[0] && res.data.UserProfile.groups[0] == "Standard") {
                            this.disableType = true;
                            this.roletype = res.data.UserProfile.groups[0];
                        }
                    }
                });
            }, 1000);
        }
        patchValue(data) {
            this.resetPass.patchValue({ username: data.username });
            this.resetPass.patchValue({ name: data.name });
            this.resetPass.patchValue({ email: data.email });
            this.resetPass.patchValue({ phone_number: data.phone_number });
            this.resetPass.patchValue({ mfa: data.mfa_enabled == true ? this.item = true : this.item = false });
            this.resetPass.patchValue({ type: data.groups[0] ? data.groups[0] : "" });
            this.chngPass.patchValue({ usrname: data.username });
            this.uName = data.username;
            this.e_mail = data.email;
            this.p_number = data.phone_number;
            // this.getLoginHistory(this.uName);
        }
        get resetF() {
            return this.chngPass.controls;
        }
        get submitF() {
            return this.resetPass.controls;
        }
        chngePasswrd() {
            this.ChngPass = true;
            this.edProfile = false;
            this.showhistory = false;
        }
        editProfile() {
            this.ChngPass = false;
            this.showhistory = false;
            this.edProfile = true;
        }
        showHistory() {
            this.ChngPass = false;
            this.edProfile = false;
            //this.showhistory = true;
            this.historyList = [];
            this.getLoginHistory(this.uName);
        }
        SubmitChngPass(chngPass) {
            if (this.chngPass.invalid) {
                this.chngPass.controls['newpass'].markAsTouched();
                this.chngPass.controls['oldpass'].markAsTouched();
                this.chngPass.controls['cnfpass'].markAsTouched();
            }
            else {
                let x = this.chngPass.value;
                if (this.PasswordMatch == true) {
                    let npass = this.chngPass.value.newpass;
                    let oldpass = this.chngPass.value.oldpass;
                    let params = {
                        'previous_password': oldpass,
                        'new_password': npass,
                        'access_token': this.AcToken
                    };
                    this.customerservice.changepassword(params).subscribe(res => {
                        if (res.statusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Password Changed!!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    });
                }
            }
        }
        SubmitPass(resetPass) {
            if (this.resetPass.invalid) {
                this.resetPass.controls['name'].markAsTouched();
                this.resetPass.controls['email'].markAsTouched();
                this.resetPass.controls['phone_number'].markAsTouched();
            }
            else {
                let arr = [
                    { 'Name': 'phone_number', 'Value': this.resetPass.value.phone_number },
                    { 'Name': 'email', 'Value': this.resetPass.value.email },
                    { 'Name': 'name', 'Value': this.resetPass.value.name }
                ];
                this.parm['username'] = this.resetPass.value.username;
                this.parm['user_group'] = this.resetPass.value.type;
                /*if(this.resetPass.value.password != ''){
                this.parm['password']= this.resetPass.value.password;
                }*/
                this.parm['attributes'] = arr;
                this.customerservice.updateUser(this.parm).subscribe(res => {
                    // console.log(res)
                    if (res.statusCode == 200) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Updated!!',
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                });
            }
        }
        CheckPassword(event) {
            let confirmPass = event.target.value;
            let passWrd = this.chngPass.value.newpass;
            if (passWrd == confirmPass) {
                // console.log('wd')
                this.PasswordMatch = true;
            }
            else {
                // console.log('wdd')
                this.PasswordMatch = false;
            }
        }
        genPass() {
            const ranPassword = this.randomPassword(14);
            this.chngPass.patchValue({ newpass: ranPassword });
            this.chngPass.patchValue({ cnfpass: ranPassword });
            this.PasswordMatch = true;
        }
        randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }
        getLoginHistory(username) {
            let p = {
                "username": username
            };
            this.customerservice.loginHistory(p).subscribe(res => {
                if (res.statusCode == 200) {
                    this.showhistory = true;
                    for (let item of res.data.login_history) {
                        //console.log(item)
                        this.historyList.push(item);
                    }
                    setTimeout(() => {
                        $('#table2').DataTable().destroy();
                        this.dtTableLoad('table2');
                    }, 1000);
                }
            });
        }
        dtTableLoad(tableId = '') {
            let dbOption = {};
            dbOption = {
                initComplete: function (settings, json) {
                    $('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    //$('#table3_filter').detach().appendTo('#sbar');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    });
                },
                scrollCollapse: true,
                paging: false,
                select: false,
                bFilter: false,
                bInfo: false,
                ordering: true,
                bLengthChange: false
                // lengthMenu: [10, 25, 50,100,500,1000,2000]
            };
            $('#' + tableId).DataTable(dbOption);
        }
    };
    EditProfileComponent = __decorate([
        Component({
            selector: 'app-edit-profile',
            templateUrl: './edit-profile.component.html',
            styleUrls: ['./edit-profile.component.scss']
        })
    ], EditProfileComponent);
    return EditProfileComponent;
})();
export { EditProfileComponent };
//# sourceMappingURL=edit-profile.component.js.map