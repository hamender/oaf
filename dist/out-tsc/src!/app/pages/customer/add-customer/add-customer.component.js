import { __decorate } from "tslib";
import { Component, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CustomValidators } from '../../../shared/validator/custom-validators';
let AddCustomerComponent = /** @class */ (() => {
    let AddCustomerComponent = class AddCustomerComponent {
        constructor(fb, customerservice, activeModal) {
            this.fb = fb;
            this.customerservice = customerservice;
            this.activeModal = activeModal;
            this.aCustomer = new EventEmitter();
        }
        ngOnInit() {
            this.customerForm = this.fb.group({
                username: ['', [Validators.required]],
                password: ['', [Validators.required, Validators.minLength(14), CustomValidators.strong]],
                name: ['', [Validators.required]],
                phone: ['', [Validators.required, Validators.pattern('^\\+[1-9]\\d{1,14}$')]],
                customerEmail: ['', [Validators.required, Validators.email]],
                mfa: ['', [Validators.required]],
                type: ['', [Validators.required]],
            });
        }
        closeSelectModal(customerForm) {
            let ph = customerForm.value.phone.toString();
            this.mf = (customerForm.value.mfa == 'yes' ? true : false);
            let params = {
                'username': customerForm.value.username,
                'password': customerForm.value.password,
                'name': customerForm.value.name,
                'phone_number': ph,
                'email': customerForm.value.customerEmail,
                'enable_mfa': this.mf,
                'user_group': customerForm.value.type
            };
            console.log(params);
            this.customerservice.create(params).subscribe(res => {
                console.log(res.data.Error);
                if (res.statusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'User Created.',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    this.customerForm.reset();
                    this.activeModal.close();
                    this.aCustomer.emit(res);
                }
                else {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Something went wrong!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            });
        }
        get customerF() {
            return this.customerForm.controls;
        }
        closeform() {
            this.activeModal.close();
        }
        genPass() {
            const ranPassword = this.randomPassword(14);
            console.log(ranPassword);
            this.customerForm.patchValue({ password: ranPassword });
        }
        randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }
    };
    __decorate([
        Output()
    ], AddCustomerComponent.prototype, "aCustomer", void 0);
    AddCustomerComponent = __decorate([
        Component({
            selector: 'app-add-customer',
            templateUrl: './add-customer.component.html',
            styleUrls: ['./add-customer.component.scss']
        })
    ], AddCustomerComponent);
    return AddCustomerComponent;
})();
export { AddCustomerComponent };
//# sourceMappingURL=add-customer.component.js.map