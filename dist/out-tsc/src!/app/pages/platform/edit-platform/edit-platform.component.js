import { __decorate } from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
let EditPlatformComponent = /** @class */ (() => {
    let EditPlatformComponent = class EditPlatformComponent {
        constructor(activeModal, fb, platformservice, resourceservice, elem) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.platformservice = platformservice;
            this.resourceservice = resourceservice;
            this.elem = elem;
            this.passEntry = new EventEmitter();
            this.delRes = new EventEmitter();
            this.typeArr = [];
            this.dropArray = [];
            this.arr = [];
            this.show = false;
            this.getAll = [];
            this.compArray = [];
            this.param = {};
            this.parms = {};
            this.delbtnDisable = true;
        }
        ngOnInit() {
            this.userForm = this.fb.group({
                platform: ['', [Validators.required]],
            });
            if (this.platId && this.resId) {
                this.getPlatform(this.platId, this.resId, this.index);
            }
            if (this.viewId) {
                this.getPlatform(this.viewId, this.viewresId, this.index);
            }
            this.arr = [
                { 'ftype': 'field-e', 'type': 'email', 'exacttype': 'Email' },
                { 'ftype': 'field-s', 'type': 'selector', 'exacttype': 'Selector' },
                { 'ftype': 'field-sn', 'type': 'text', 'exacttype': 'Selector + Add New' },
                { 'ftype': 'field-d', 'type': 'date', 'exacttype': 'Date' },
                { 'ftype': 'field-dt', 'type': 'datetime', 'exacttype': 'Date and Time' },
                { 'ftype': 'field-n', 'type': 'text', 'exacttype': 'String-n' },
                { 'ftype': 'field-a', 'type': 'text', 'exacttype': 'String-a' },
                { 'ftype': 'field-m', 'type': 'text', 'exacttype': 'String-m' },
                { 'ftype': 'field-me', 'type': 'text', 'exacttype': 'Emails (Separated by comma)' },
                { 'ftype': 'field-ms', 'type': 'text', 'exacttype': 'Multi selector (Separated by comma)' },
                { 'ftype': 'field-mm', 'type': 'text', 'exacttype': 'Multi Mixed (Separated by comma)' },
                { 'ftype': 'field-x', 'type': 'text', 'exacttype': 'Mixed Encrypted  (hashed)' },
                { 'ftype': 'Support Email', 'type': 'email', 'exacttype': 'Support Email' },
            ];
        }
        updateForm(data) {
            this.userForm.patchValue({ platform: data });
            this.delbtnDisable = false;
        }
        getPlatform(value, resVal, index) {
            let p = {
                'platform': value,
                'id': resVal
            };
            this.resourceservice.getResource(p).subscribe(res => {
                console.log(res);
                if (res.statusCode == 200) {
                    this.updateForm(value);
                    let headArray = [];
                    this.itype = res.Metadata[index];
                    this.platname = value;
                    console.log(this.itype);
                    for (let i of Object.keys(res.Metadata)) {
                        if (i != 'services') {
                            headArray.push(i);
                        }
                    }
                    let valArray = [];
                    for (let val of Object.values(res.Metadata)) {
                        if (!Array.isArray(val)) {
                            valArray.push(val);
                        }
                    }
                    var result = valArray.reduce(function (result, field, index) {
                        result[headArray[index]] = field;
                        return result;
                    }, {});
                    this.compArray = result;
                }
            });
        }
        gettype(a) {
            for (let ar of this.arr) {
                if (a === ar.ftype) {
                    return ar.type;
                }
            }
        }
        fieldChange() {
        }
        selectedDrop() {
        }
        closeform() {
            this.activeModal.close();
        }
        /*  getIds($event){
            let platformId = this.platformId.nativeElement.value;
            let field = this.field.nativeElement.value;
        
          }*/
        submitForm(userForm) {
            // let updateArray= []
            let platformId = this.platformId.nativeElement.value;
            let field = this.field.nativeElement.value;
            let elements = this.elem.nativeElement.querySelectorAll('.option_input');
            elements.forEach(element => {
                this.param[element.name] = element.value;
            });
            this.param['platform'] = platformId;
            console.log(this.param);
            this.resourceservice.update(this.param).subscribe(res => {
                console.log(res);
                if (res.StatusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Resource Updated!!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    this.activeModal.close();
                }
                this.passEntry.emit(res);
            });
        }
        delPlatformRes(idextype, plat) {
            /* let params= {
               this.index: idextype,
               'platform':plat
             }*/
            this.parms[this.index] = idextype;
            this.parms['platform'] = plat;
            //console.log(this.parms)
            Swal.fire({
                title: 'Are you sure you want to delete?',
                text: "This action cannot be undone",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    this.resourceservice.deleteResource(this.parms).subscribe(res => {
                        if (res.StatusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Resource Deleted!!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                        this.activeModal.close();
                        this.delRes.emit(res);
                    });
                }
            });
        }
    };
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "platId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "resId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "index", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "viewId", void 0);
    __decorate([
        Input()
    ], EditPlatformComponent.prototype, "viewresId", void 0);
    __decorate([
        ViewChild('platformId')
    ], EditPlatformComponent.prototype, "platformId", void 0);
    __decorate([
        ViewChild('dropdown')
    ], EditPlatformComponent.prototype, "dropdown", void 0);
    __decorate([
        ViewChild('field')
    ], EditPlatformComponent.prototype, "field", void 0);
    __decorate([
        ViewChild('field1')
    ], EditPlatformComponent.prototype, "field1", void 0);
    __decorate([
        Output()
    ], EditPlatformComponent.prototype, "passEntry", void 0);
    __decorate([
        Output()
    ], EditPlatformComponent.prototype, "delRes", void 0);
    EditPlatformComponent = __decorate([
        Component({
            selector: 'app-edit-platform',
            templateUrl: './edit-platform.component.html',
            styleUrls: ['./edit-platform.component.scss']
        })
    ], EditPlatformComponent);
    return EditPlatformComponent;
})();
export { EditPlatformComponent };
//# sourceMappingURL=edit-platform.component.js.map