import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { AddConnectorComponent } from '../add-connector/add-connector.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { EditConnectorComponent } from '../edit-connector/edit-connector.component';
let ManageConnectorComponent = /** @class */ (() => {
    let ManageConnectorComponent = class ManageConnectorComponent {
        constructor(utilService, modalService, connectorservice) {
            this.utilService = utilService;
            this.modalService = modalService;
            this.connectorservice = connectorservice;
            this.dtOptions = {};
            this.dtTrigger = new Subject();
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Manage Connector' }];
            this.utilService.changeBreadcrumb(breadcrumb);
        }
        ngOnInit() {
            this.dtOptions = {
                initComplete: function (settings, json) {
                    $('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    });
                },
                scrollCollapse: true,
                paging: true,
                select: true,
                bFilter: true,
                bInfo: true,
                ordering: true,
                lengthMenu: [10, 25, 50, 100, 500, 1000, 2000],
            };
            setTimeout(() => {
                this.getList();
            }, 1000);
        }
        addConnector() {
            const ngmodalRef = this.modalService.open(AddConnectorComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.Aconnector.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        editConnector(event) {
            const ngmodalRef = this.modalService.open(EditConnectorComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.eID = event;
            ngmodalRef.componentInstance.editCon.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            ngmodalRef.componentInstance.delCon.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        viewConnector(event) {
            const ngmodalRef = this.modalService.open(EditConnectorComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.vID = event;
        }
        getList(noUpdate) {
            this.connectorservice.list().subscribe(res => {
                if (res.statusCode == 200) {
                    if (res.data.Items.length > 0) {
                        this.tableData = res.data.Items;
                    }
                    if (noUpdate) {
                        this.dtElement.dtInstance.then((dtInstance) => {
                            dtInstance.destroy();
                            this.dtTrigger.next();
                        });
                    }
                    else {
                        this.dtTrigger.next();
                    }
                }
            });
        }
    };
    __decorate([
        ViewChild(DataTableDirective, { static: false })
    ], ManageConnectorComponent.prototype, "dtElement", void 0);
    ManageConnectorComponent = __decorate([
        Component({
            selector: 'app-manage-connector',
            templateUrl: './manage-connector.component.html',
            styleUrls: ['./manage-connector.component.scss']
        })
    ], ManageConnectorComponent);
    return ManageConnectorComponent;
})();
export { ManageConnectorComponent };
//# sourceMappingURL=manage-connector.component.js.map