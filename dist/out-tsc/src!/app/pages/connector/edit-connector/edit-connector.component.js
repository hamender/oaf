import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import Swal from 'sweetalert2';
let EditConnectorComponent = /** @class */ (() => {
    let EditConnectorComponent = class EditConnectorComponent {
        constructor(activeModal, fb, connectorservice, platformservice, customerservice) {
            this.activeModal = activeModal;
            this.fb = fb;
            this.connectorservice = connectorservice;
            this.platformservice = platformservice;
            this.customerservice = customerservice;
            this.fretypeArray = [];
            this.editCon = new EventEmitter();
            this.delCon = new EventEmitter();
        }
        ngOnInit() {
            this.formConnector = this.fb.group({
                connector_name: [{ value: '', disabled: true }, [Validators.required]],
                description: ['', [Validators.required]],
                connector_type: ['', [Validators.required]],
                frequency_type: ['', [Validators.required]],
                frequency_value: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
                connector_association: ['', [Validators.required]],
                assoc_value: [''],
                id: ['', [Validators.required]],
            });
            setTimeout(() => {
                this.getplatform();
            }, 1000);
            if (this.eID) {
                this.get_current_connector(this.eID);
            }
            if (this.vID) {
                this.get_current_connector(this.vID);
                this.formConnector.get('frequency_type').disable();
                this.formConnector.get('connector_association').disable();
                this.formConnector.get('assoc_value').disable();
                this.formConnector.get('connector_type').disable();
                this.formConnector.get('frequency_value').disable();
                this.formConnector.get('description').disable();
            }
            this.fretypeArray = [{ 'val': 'MINUTES', 'name': 'MINUTES' },
                { 'val': 'HOURS', 'name': 'HOURS' },
                { 'val': 'DAYS', 'name': 'DAYS' }];
        }
        closeform() {
            this.activeModal.close();
        }
        get submitF() {
            return this.formConnector.controls;
        }
        submit(formConnector) {
            let params = {
                'id': this.formConnector.value.id,
                'connector_description': this.formConnector.value.description,
                'frequency_type': this.formConnector.value.frequency_type,
                'frequency_value': this.formConnector.value.frequency_value,
                "connector_association": [{
                        "type": this.formConnector.value.assoc_value ? 'SERVICE' : 'PLATFORM',
                        "value": this.formConnector.value.assoc_value ? this.formConnector.value.assoc_value : this.formConnector.value.connector_association
                    }],
            };
            this.connectorservice.update(params).subscribe(res => {
                console.log(res);
                if (res.statusCode == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Connector Updated!!',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    this.activeModal.close();
                    this.editCon.emit(res);
                }
            });
        }
        get_current_connector(id) {
            let params = {
                'id': id
            };
            this.connectorservice.get(params).subscribe(res => {
                console.log(res);
                if (res.statusCode == 200) {
                    this.updateValue(res.data.item);
                }
            });
        }
        getplatform() {
            this.platformservice.summary1().subscribe(res => {
                if (res.StatusCode == 200) {
                    this.platformList = res.Platforms;
                }
            });
        }
        get_platform_service(plat) {
            let params = {
                'platform': plat
            };
            this.customerservice.getServiceListbyID(params).subscribe(res => {
                this.serviceList = res.Services;
            });
        }
        selectAssoc(event) {
            let selPlatform = event.target.value;
            console.log(selPlatform);
            this.get_platform_service(selPlatform);
        }
        selectConnectorType(event) {
        }
        delConnector(id) {
            let params = {
                'id': id
            };
            Swal.fire({
                title: 'Are you sure you want to delete?',
                text: "This action cannot be undone",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0181AC',
                cancelButtonColor: '#d5d5d5',
                confirmButtonText: 'Delete'
            }).then((result) => {
                if (result.value) {
                    this.connectorservice.deleteConnector(params).subscribe(res => {
                        if (res.statusCode == 200) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Connector Deleted!!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                        this.activeModal.close();
                        this.delCon.emit(res);
                    });
                }
            });
        }
        updateValue(data) {
            this.formConnector.patchValue({ connector_name: data.connector_name });
            this.formConnector.patchValue({ id: data.id });
            this.formConnector.patchValue({ description: data.connector_description });
            this.formConnector.patchValue({ connector_type: data.connector_type });
            this.formConnector.patchValue({ frequency_type: data.frequency_type });
            this.formConnector.patchValue({ frequency_value: data.frequency_value });
            for (let i of data.connector_association) {
                if (i.type == "PLATFORM") {
                    this.formConnector.patchValue({ connector_association: i.value });
                    this.get_platform_service(i.value);
                }
                else {
                }
            }
            this.Cid = data.id;
            /*this.formConnector.patchValue({assoc_value: data.assoc_value})*/
        }
    };
    __decorate([
        Input()
    ], EditConnectorComponent.prototype, "eID", void 0);
    __decorate([
        Input()
    ], EditConnectorComponent.prototype, "vID", void 0);
    __decorate([
        Output()
    ], EditConnectorComponent.prototype, "editCon", void 0);
    __decorate([
        Output()
    ], EditConnectorComponent.prototype, "delCon", void 0);
    EditConnectorComponent = __decorate([
        Component({
            selector: 'app-edit-connector',
            templateUrl: './edit-connector.component.html',
            styleUrls: ['./edit-connector.component.scss']
        })
    ], EditConnectorComponent);
    return EditConnectorComponent;
})();
export { EditConnectorComponent };
//# sourceMappingURL=edit-connector.component.js.map