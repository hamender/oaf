import { __decorate } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { EditUserComponent } from '../../pages/manage/edit-user/edit-user.component';
import { AddCustomerComponent } from '../../pages/customer/add-customer/add-customer.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
let ManageUserComponent = /** @class */ (() => {
    let ManageUserComponent = class ManageUserComponent {
        constructor(router, modalService, customerservice, utilService) {
            this.router = router;
            this.modalService = modalService;
            this.customerservice = customerservice;
            this.utilService = utilService;
            this.loader = true;
            this.dtOptions = {};
            this.dtTrigger = new Subject();
            this.show = false;
            const breadcrumb = [{ name: 'Home', link: '/', type: 'base' }, { name: 'Manage User' }];
            this.utilService.changeBreadcrumb(breadcrumb);
        }
        getList(noUpdate) {
            this.customerservice.list().subscribe(res => {
                if (res.statusCode == 200) {
                    this.userList = res.data.Users;
                    this.show = true;
                    if (noUpdate) {
                        this.dtElement.dtInstance.then((dtInstance) => {
                            dtInstance.destroy();
                            this.dtTrigger.next();
                        });
                    }
                    else {
                        this.dtTrigger.next();
                    }
                }
            });
        }
        ngOnInit() {
            setTimeout(() => {
                this.getList();
            }, 1000);
            this.dtOptions = {
                initComplete: function (settings, json) {
                    $('body').find('.dataTables_scroll').css('border-bottom', '0px');
                    $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
                    $('body').find('.dataTables_filter input').css({
                        "border-width": "0px",
                        "border-bottom": "1px solid #b1b8bb",
                        "outline": "none",
                        "width": "150px",
                        "margin-bottom": "0px",
                        "margin-right": "0px !important",
                        "margin-left": "0px !important"
                    });
                },
                scrollCollapse: true,
                paging: true,
                select: true,
                bFilter: true,
                bInfo: true,
                ordering: true,
                lengthMenu: [10, 25, 50, 100, 500, 1000, 2000],
            };
        }
        viewUser(index) {
            const ngmodalRef = this.modalService.open(EditUserComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.viewId = index;
            ngmodalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
                console.log(receivedEntry);
            });
        }
        editUser(index) {
            const ngmodalRef = this.modalService.open(EditUserComponent, {
                size: 'md',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.username = index;
            ngmodalRef.componentInstance.passEntry.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            ngmodalRef.componentInstance.dEntry.subscribe((rdata1) => {
                if (rdata1.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
        }
        addCustomer() {
            const ngmodalRef = this.modalService.open(AddCustomerComponent, {
                size: 'lg',
                backdrop: 'static'
            });
            ngmodalRef.componentInstance.aCustomer.subscribe((rdata) => {
                if (rdata.statusCode == 200) {
                    this.getList('noUpdate');
                }
            });
            //this.router.navigate(['//add-customer'])
        }
        ngOnDestroy() {
            this.dtTrigger.unsubscribe();
        }
    };
    __decorate([
        ViewChild(DataTableDirective, { static: false })
    ], ManageUserComponent.prototype, "dtElement", void 0);
    ManageUserComponent = __decorate([
        Component({
            selector: 'app-manage-user',
            templateUrl: './manage-user.component.html',
            styleUrls: ['./manage-user.component.scss']
        })
    ], ManageUserComponent);
    return ManageUserComponent;
})();
export { ManageUserComponent };
//# sourceMappingURL=manage-user.component.js.map