import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OafRoutingModule } from './oaf-routing.module';
import { OafComponent } from './oaf.component';
let OafModule = /** @class */ (() => {
    let OafModule = class OafModule {
    };
    OafModule = __decorate([
        NgModule({
            declarations: [OafComponent],
            imports: [
                CommonModule,
                OafRoutingModule
            ]
        })
    ], OafModule);
    return OafModule;
})();
export { OafModule };
//# sourceMappingURL=oaf.module.js.map