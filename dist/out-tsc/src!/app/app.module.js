import { __decorate } from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './shared/guard/guard.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './home/login/login.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { AddPlatformComponent } from './pages/platform/add-platform/add-platform.component';
import { EditPlatformComponent } from './pages/platform/edit-platform/edit-platform.component';
import { AddServiceComponent } from './pages/services/add-service/add-service.component';
import { EditServiceComponent } from './pages/services/edit-service/edit-service.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { AddCustomerComponent } from './pages/customer/add-customer/add-customer.component';
import { AddProvisionComponent } from './pages/provision/add-provision/add-provision.component';
import { PlateformComponent } from './pages/platform/plateform/plateform.component';
import { AddInstanceComponent } from './pages/instance/add-instance/add-instance.component';
import { AddResourceComponent } from './pages/resource/add-resource/add-resource.component';
import { ManageUserComponent } from './pages/manage-user/manage-user.component';
//import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditUserComponent } from './pages/manage/edit-user/edit-user.component';
import { BreadcrumbComponent } from './shared/component/breadcrumb/breadcrumb.component';
import { UserIdleModule } from 'angular-user-idle';
import { AppPasswordDirective } from './shared/directive/app-password.directive';
import { DatePipe } from '@angular/common';
import { ShContextMenuModule } from 'ng2-right-click-menu';
import { EditProvisionComponent } from './pages/provision/edit-provision/edit-provision.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AddConnectorComponent } from './pages/connector/add-connector/add-connector.component';
import { ManageConnectorComponent } from './pages/connector/manage-connector/manage-connector.component';
import { EditConnectorComponent } from './pages/connector/edit-connector/edit-connector.component';
let AppModule = /** @class */ (() => {
    let AppModule = class AppModule {
    };
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                LoginComponent,
                DashboardComponent,
                AddPlatformComponent,
                EditPlatformComponent,
                AddServiceComponent,
                EditServiceComponent,
                HeaderComponent,
                FooterComponent,
                EditProfileComponent,
                AddCustomerComponent,
                AddProvisionComponent,
                PlateformComponent,
                AddInstanceComponent,
                AddResourceComponent,
                ManageUserComponent,
                EditUserComponent,
                BreadcrumbComponent,
                AppPasswordDirective,
                EditProvisionComponent,
                AddConnectorComponent,
                ManageConnectorComponent,
                EditConnectorComponent
            ],
            imports: [
                BrowserModule,
                NgbModule,
                AppRoutingModule,
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                OwlDateTimeModule,
                OwlNativeDateTimeModule,
                //ToastrModule.forRoot() // ToastrModule added, 
                BrowserAnimationsModule,
                ShContextMenuModule,
                UserIdleModule.forRoot({ idle: 3600, timeout: 1800, ping: 870 }),
                DataTablesModule,
                NgMultiSelectDropDownModule.forRoot(),
            ],
            providers: [
                AuthGuard, DatePipe,
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
})();
export { AppModule };
//# sourceMappingURL=app.module.js.map