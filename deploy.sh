#!/bin/bash -xe 
apt-get update -y
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - 
apt-get install jq nodejs -y
#ln -sf /usr/bin/nodejs /usr/bin/node

echo $1
echo $2

CREDENTIALS=`aws sts assume-role --role-arn "$1" --role-session-name oaf-app-deploy --output=json` 
export AWS_DEFAULT_REGION=us-east-1 
export AWS_ACCESS_KEY_ID=`echo ${CREDENTIALS} | jq -r '.Credentials.AccessKeyId'` 
export AWS_SECRET_ACCESS_KEY=`echo ${CREDENTIALS} | jq -r '.Credentials.SecretAccessKey'` 
export AWS_SESSION_TOKEN=`echo ${CREDENTIALS} | jq -r '.Credentials.SessionToken'` 
 
aws sts get-caller-identity 
 
#cd "$CODEBUILD_SRC_DIR/web-content/app/config" 
 
#if [ "$ENV" == "PRE-PROD" ] 
#then 
#    mv settings.PRE-PROD.js settings.js 
#    rm -f settings.PROD.js 
 
#elif [ "$ENV" == "PROD" ] 
#then 
#    mv settings.PROD.js settings.js 
#    rm -f settings.PRE-PROD.js 
 
# else 
    # rm -f settings.PROD.js 
    # rm -f settings.PRE-PROD.js 
 
#fi

sudo npm install
sudo npm install npm@latest -g
sudo npm cache clean -f
sudo npm update -g nvm
#sudo npm install -g angular cli

sudo npm i -g @angular/cli


npm config set strict-ssl false
 
#rm -rf $CODEBUILD_SRC_DIR/deploy 
#mkdir $CODEBUILD_SRC_DIR/deploy 

#npm run prod

ng build --prod --aot=true

echo "AFTER NPM"
ls

cd "$CODEBUILD_SRC_DIR"/

echo "AFTER CD"
ls

#for FOLDER in "dist" "src" "vendor"
#do 
#    aws s3 sync ${FOLDER} s3://$2/${FOLDER} --acl private  
#done 

aws s3 sync ./dist/wm-oaf/ s3://$2/ --acl private