import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from "@angular/router";
import { UserIdleService } from 'angular-user-idle';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  uName: any;
  texttt: boolean = false;
  isAdmin:boolean;
  imagePath = '../../../assets/images/myra-logo.png';
  constructor(public authservice: AuthenticationService,
    private router: Router,
    private userIdle: UserIdleService,
    ) {

    if(localStorage.getItem('Attribute')){
      let attribute = JSON.parse(localStorage.getItem('Attribute'));
     // console.log(attribute)
      this.uName = attribute.username;
    }
     }



  ngOnInit(): void {
      this.userIdle.startWatching();
    this.logoutAcc();

    this.userIdle.onTimerStart().subscribe(count => console.log(count));
    
    this.userIdle.onTimeout().subscribe(() => console.log('Time is up!'));
    if(this.authservice.isAdmin()){
      this.isAdmin = true
    }
    else{
      this.isAdmin = false
    }
  }
  logout(){

     
  let params = {"username": this.uName}
    this.authservice.logout(params).subscribe(res=>{
      console.log(res)
    
     this.router.navigate(['/']);
     localStorage.clear();
      localStorage.removeItem('timer');
      localStorage.removeItem('ACCESS_TOKEN');
      localStorage.removeItem('Attribute');
      localStorage.removeItem('Token');
    })
    
  }
   logoutAcc(){
 
    this.userIdle.onTimerStart().subscribe(count => {
            console.log('no')
            if(count == 45){
              console.log('yes')
              this.logout();
            }
          });
  }
  mUser(){
    this.router.navigate(['//manage-user'])
  }
  mConnector(){
    
    this.router.navigate(['//manage-connector'])
  }
  
   editprofile(){
    this.router.navigate(['//edit-profile'])
  }
  
  LogoClick(){
    this.router.navigate(['//dashboard'])
  }


}
