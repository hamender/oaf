import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAppPassword]'
})
export class AppPasswordDirective {
 private _shown = false;
  constructor(private el: ElementRef) { 
this.setup();
  }
toggle(span: HTMLElement) {
    this._shown = !this._shown;
    if (this._shown) {
      this.el.nativeElement.setAttribute('type', 'text');
      span.innerHTML = '<span class=""><i class="fa fa-eye-slash fa-fw"></i></span>';
    } else {
      this.el.nativeElement.setAttribute('type', 'password');
      span.innerHTML = '<span class=""><i class="fa fa-eye fa-fw"></i></span>';
    }
  }
setup() {
    const parent = this.el.nativeElement.parentNode;
    const span = document.createElement('span');
    span.innerHTML = `<span class=""><i class="fa fa-eye fa-fw"></i></span>`;
    span.addEventListener('click', (event) => {
      this.toggle(span);
    });
    parent.appendChild(span);
  }
}
