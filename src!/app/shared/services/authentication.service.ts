import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  url: string;
  constructor (@Inject(DOCUMENT) private document: Document,
    private http: HttpClient) { 
    this.url = environment.url;
  
	}

	signin(user:any): Observable<any> {
    return this.http.post(this.url+'signin', user).pipe(
      map((auth: { data: any }) => {
        localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
       return auth;
      })
      );
  }

  getHostname() {
    return this.document.location.hostname;
  }
  isAdmin() {
    if(localStorage.getItem('ACCESS_TOKEN')){
      let Attribute = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
     let grp = Attribute.UserAttributes.groups;
     
      if(grp.includes('Super-Admin')){
      return true;
      }
      else{
        return false;
      }
    }
  }

  public isLoggedIn() {
    if(localStorage.getItem('Token')){
      const token = localStorage.getItem('Token');
      return token ?  true : false;
    } else {
      return false;
    }
  }
 /* public logout() {
    localStorage.clear();
  }*/
  logout(params:any):Observable<any>{
    
    return this.http.post(this.url+'signout',params)
  }

  resetPassword(params:any):Observable<any>{
    return this.http.post(this.url+'resetpassword',params).pipe(
      map((auth: { data: any }) => {
        localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
       return auth;
      })
      );
  }
  resetUserPassword(params:any):Observable<any>{
    return this.http.put(this.url+'resetpassword',params)
  }

  forgotPassword(param:any):Observable <any> {
    return this.http.post(this.url+'forgotpassword',param)
  }

  forgottonusername(param:any):Observable <any> {
    return this.http.post(this.url+'forgotusername',param)
  }

  mfa(params:any):Observable <any> {
    return this.http.post(this.url+'mfa',params).pipe(
      map((auth: { data: any }) => {
        localStorage.setItem('ACCESS_TOKEN', JSON.stringify(auth.data));
       return auth;
      })
      );
  }
  public getToken(){
    if(localStorage.getItem('Token')){
        const token = JSON.parse(localStorage.getItem('Token'));
        
        return token.IdToken;
      } else {
        return false;
      }
  }
  public getToken1(){
    if(localStorage.getItem('ACCESS_TOKEN')){
        const token1 = JSON.parse(localStorage.getItem('ACCESS_TOKEN'));
        
        return token1.IdToken;
      } else {
        return false;
      }
  }
  public getAccToken(){
    if(localStorage.getItem('Token')){
        const token = JSON.parse(localStorage.getItem('Token'));
        return token.AccessToken;
      } else {
        return false;
      }
  }

 
  
}
