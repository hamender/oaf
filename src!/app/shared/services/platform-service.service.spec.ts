import { TestBed } from '@angular/core/testing';

import { Service } from './platform-service.service';

describe('PlatformServiceService', () => {
  let service: PlatformServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlatformServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
