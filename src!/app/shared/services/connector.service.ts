import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
 
export class ConnectorService {
	url: string;
  IDToken1: any;
  AcToken: any;
  constructor(private http: HttpClient,
  	private auth:AuthenticationService) {

  		this.url = environment.url;
	    this.IDToken1 = this.auth.getToken1();
	    this.AcToken = this.auth.getAccToken();
  	}

  	create(data: any): Observable<any> {
  		if(this.IDToken1){
  			return this.http.post<Blob>(this.url+'connector', data, {
  				headers: new HttpHeaders(
  					{
  						'Authorization': this.IDToken1,
  						'Accept': '*/*',
  					}),
  			});
  		}
  	}

    update(params: any): Observable<any> {
      if(this.IDToken1){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': this.IDToken1,
                'Accept': '*/*',
              });
     return this.http.put(this.url+'connector', params, { headers: finalHeaders});  
        
        
      }
  }

  get(params: any): Observable<any> { 
      if(this.IDToken1){
        let finalHeaders = new HttpHeaders(
              {
                'Authorization': this.IDToken1,       
                'Accept': '*/*',
              });
        
        return this.http.get(
          this.url+'connector',
          { headers: finalHeaders, params }
        );
      }
  }

  deleteConnector(data: any): Observable<any> {
      if(this.IDToken1){
        const options = {
        headers: new HttpHeaders({
            'Authorization': this.IDToken1,
            'Accept': '*/*',
        }),
        body: data,
        }
        return this.http.delete( this.url+'connector', options);
      }
  }

  list():Observable<any> {
      if(this.IDToken1){
      return this.http.get<Blob>(this.url+'connector/list', { //  <-----  notice the null  *****
        headers: new HttpHeaders(
          {
            'Authorization': this.IDToken1,
            'Accept': '*/*',
          }),  
        });
      }
  }

}
