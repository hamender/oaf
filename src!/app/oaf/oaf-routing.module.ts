import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OafComponent } from './oaf.component';
import { LoginComponent } from '../home/login/login.component';
import { DashboardComponent } from '../home/dashboard/dashboard.component';
import { AuthGuard } from '../shared/guard/guard.module';
import { EditProfileComponent } from '../pages/profile/edit-profile/edit-profile.component'
import { AddCustomerComponent } from '../pages/customer/add-customer/add-customer.component';
import { ManageUserComponent } from '../pages/manage-user/manage-user.component';
import { ManageConnectorComponent } from '../pages/connector/manage-connector/manage-connector.component'
/*const routes: Routes = [{ path: '', component: NyanComponent }];*/
const routes: Routes = [
{
  path: '',
  component: LoginComponent,
  children: [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  ]
},{
  path: 'login',
  component: LoginComponent,
},
{
	path : 'dashboard',
	component: DashboardComponent,
  canActivate: [AuthGuard]
},
{
  path : 'edit-profile',
  component: EditProfileComponent,
  canActivate: [AuthGuard]
},
{
  path : 'add-customer',
  component: AddCustomerComponent,
  canActivate: [AuthGuard]
},
{
  path : 'manage-user',
  component: ManageUserComponent,
  canActivate: [AuthGuard]
  
},
{
  path : 'manage-connector',
  component: ManageConnectorComponent,
  canActivate: [AuthGuard]
  
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OafRoutingModule { }
