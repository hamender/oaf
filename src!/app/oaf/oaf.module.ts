import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OafRoutingModule } from './oaf-routing.module';
import { OafComponent } from './oaf.component';


@NgModule({
  declarations: [OafComponent],
  imports: [
    CommonModule,
    OafRoutingModule
  ]
})
export class OafModule { }
