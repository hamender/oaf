import { Component, OnInit, AfterViewInit, ElementRef, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PlatformServiceService } from '../../../shared/services/platform-service.service';
import { CustomerserviceService } from '../../../shared/services/customerservice.service'


@Component({
  selector: 'app-add-provision',
  templateUrl: './add-provision.component.html',
  styleUrls: ['./add-provision.component.scss']
})
export class AddProvisionComponent implements OnInit {
	provision : FormGroup;
  type: string = 'text';
  arr = [];
  platformList: any;
  serviceList: any;
  selectedVal:string='';
  selectedValService:string='';
  loopnumber: any;
  typeArr = [];
  passList: any;
  PassPlatformValue: any;
  show: boolean = false;



  constructor(public activeModal: NgbActiveModal,
  				private fb: FormBuilder,
          private platformservice: PlatformServiceService,
          private customerservice: CustomerserviceService) { 

        this.getPlatformList();
        
  }

  ngOnInit(): void {
  	this.provision = this.fb.group({
      platform: [''],
      service: [''],
      resourceId: [''],
      instanceId: [''],
      platformfields: this.fb.array([]),
    });
    const add = this.provision.get('platformfields') as FormArray;
          add.push(this.fb.group({
            platField: [],
          }))
    this.arr = [
    {'ftype':'field-e','type':'email'},
    {'ftype':'field-s','type':'selector'},
    {'ftype':'field-sn','type':'selector'},
    {'ftype':'field-d','type':'date'},
    {'ftype':'field-dt','type':'datetime'},
    {'ftype':'field-n','type':'text'},
    {'ftype':'field-a','type':'text'},
    {'ftype':'field-m','type':'text'},
    {'ftype':'field-me','type':'text'},
    {'ftype':'field-ms','type':'text'},
    {'ftype':'field-mm','type':'text'},
    {'ftype':'field-x','type':'text'},
    ]
  
  }

  selected(){
    let plat = this.selectedVal;
    this.passList=[];
    this.PassPlatformValue = plat;
    let params= {
      'platform': plat
    }
    this.customerservice.getServiceListbyID(params).subscribe(res=>{
      console.log(res)
    this.serviceList = res.Services;
    })
  
  }

  selectedServ(){
    let serv = this.selectedValService;
    console.log(serv)
    console.log(this.PassPlatformValue)
    let params = {
      'platform': this.PassPlatformValue,
      'service': serv
    }

    this.customerservice.getservicebyID(params).subscribe(res=>{
      console.log(res)

       for(let item of res.items){
        this.typeArr= []; 

        for(let i of item.Fields){
          this.typeArr.push({'type':this.gettype(i.type),'label':i.name});
          
        } 
      }
      this.show = true;
      this.passList = this.typeArr;
    })
  }

  gettype(a){
    for(let ar of this.arr){
      if(a === ar.ftype){
        return ar.type;
      }
    }
  }

  submitForm(provision){
  	console.log(provision)
    let newArray=[];
    for(let arr of provision.value.platformfields){
      let newObj={};
      newObj['type']=arr.platField;
      newArray.push(newObj)
    }
    console.log(provision)
    console.log(newArray)
  }
  closeform(){
      this.activeModal.close();
  }

  getPlatformList(){
    this.platformservice.list().subscribe(res=>{
      console.log(res)
      this.platformList = res.Metadata;
    })
  }
 

}
