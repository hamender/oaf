import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProvisionComponent } from './add-provision.component';

describe('AddProvisionComponent', () => {
  let component: AddProvisionComponent;
  let fixture: ComponentFixture<AddProvisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProvisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProvisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
