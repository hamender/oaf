import { UtilService } from '../../shared/services/util.service';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray} from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from "@angular/router";
import { EditUserComponent } from '../../pages/manage/edit-user/edit-user.component'
import Swal from 'sweetalert2';
import { CustomerService } from '../../shared/services/customer.service';
import { AddCustomerComponent } from '../../pages/customer/add-customer/add-customer.component';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss']
})
export class ManageUserComponent implements OnInit {
  userList: any;
  loader:boolean= true;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  show:boolean = false;

  constructor(private router: Router,
    private modalService: NgbModal,
    private customerservice: CustomerService,
    public utilService: UtilService) { 

    const breadcrumb = [{ name: 'Home', link: '/',type :'base'}, { name: 'Manage User' }]
    this.utilService.changeBreadcrumb(breadcrumb);

    
      
    
  }
 getList(noUpdate?){
      this.customerservice.list().subscribe(res=>{
        if(res.statusCode == 200){


        this.userList = res.data.Users
        this.show = true
         if (noUpdate) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.dtTrigger.next();
          
        })
      } 
      else {
        this.dtTrigger.next();
      }
         
        }
      })
    }
    ngOnInit(): void {

      setTimeout(()=>{
          this.getList();
         },1000);
        this.dtOptions = {
        initComplete: function(settings, json) {
          $('body').find('.dataTables_scroll').css('border-bottom', '0px');
          $('body').find('.dataTables_scrollBody').css('border-bottom', '0px');
          $('body').find('.dataTables_filter input').css({
            "border-width": "0px",
            "border-bottom": "1px solid #b1b8bb",
            "outline": "none",
            "width": "150px",
            "margin-bottom": "0px",
            "margin-right": "0px !important",
            "margin-left": "0px !important"
        } )
          },
          scrollCollapse: true,
          paging:true,
          select:true,
          bFilter: true, 
          bInfo: true,
          ordering: true,
          lengthMenu: [10, 25, 50,100,500,1000,2000],
          
      }
    }

   
    viewUser(index){

    const ngmodalRef = this.modalService.open(EditUserComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef.componentInstance.viewId = index;
    ngmodalRef.componentInstance.passEntry.subscribe((receivedEntry) => {
    console.log(receivedEntry);
    })
    }
    editUser(index){
      const ngmodalRef = this.modalService.open(EditUserComponent, {
      size: 'md',
      backdrop: 'static'
    });
    ngmodalRef.componentInstance.username = index;
    ngmodalRef.componentInstance.passEntry.subscribe((rdata) => {
      if(rdata.statusCode == 200){
        this.getList('noUpdate');
      }
    })
    ngmodalRef.componentInstance.dEntry.subscribe((rdata1) => {
      if(rdata1.statusCode == 200){
        this.getList('noUpdate');
      }
    })
    }
   
    addCustomer(){
      const ngmodalRef = this.modalService.open(AddCustomerComponent, {
      size: 'lg',
      backdrop: 'static'
    });
      ngmodalRef.componentInstance.aCustomer.subscribe((rdata) => {

  if(rdata.statusCode == 200){
    
     this.getList('noUpdate');
  }
  })
    //this.router.navigate(['//add-customer'])
  }


    ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
