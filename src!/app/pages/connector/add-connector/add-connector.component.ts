import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Output, EventEmitter,ChangeDetectorRef  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ConnectorService } from '../../../shared/services/connector.service'
import { PlatformServiceService } from '../../../shared/services/platform-service.service';
import { CustomerserviceService } from '../../../shared/services/customerservice.service';
import 'datatables.net';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-add-connector',
  templateUrl: './add-connector.component.html',
  styleUrls: ['./add-connector.component.scss']
})
export class AddConnectorComponent implements OnInit {
  @Output() Aconnector: EventEmitter<any> = new EventEmitter();
  addConnector: FormGroup;
  show_assoc_value: boolean = false;
  show_credential_User: boolean = false;
  show_credential_Role: boolean = false;
  show_credential_GCP: boolean = false;
  showfile: boolean = false;
  showjson: boolean = false;
  platformList: any;
  serviceList: any;
  globalFileUpload: any;
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();

  constructor(public activeModal: NgbActiveModal,
  				private fb: FormBuilder,
  				private cd: ChangeDetectorRef,
          private connectorservice: ConnectorService,
          private platformservice: PlatformServiceService,
          private customerservice: CustomerserviceService) {


  				 }

  ngOnInit(): void {
  	this.addConnector = this.fb.group({
      connector_name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      connector_type: ['', [Validators.required]],
      frequency_type: ['', [Validators.required]],
      frequency_value: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      connector_association: ['', [Validators.required]],
      assoc_value: [''],
      credential_detail_type: [''],
      file: [''],
      access_key_id: [''],
      secret_access_key: [''],
      iam_user_arn: [''],
      external_id: [''],
      iam_role_arn: [''],
      project_id: [''],
      private_key_id: [''],
      private_key: [''],
      client_id: [''],
      client_x509_cert_url: [''],
      client_email: ['']
      

    });

    setTimeout(()=>{
      this.getplatform();
    },1000)

    
  }

  closeform(){
  	 this.activeModal.close();
  }
  get submitF() {
    return this.addConnector.controls;
  }
  selectAssoc(event){
    let selPlatform = event.target.value;
    console.log(selPlatform)
    this.get_platform_service(selPlatform)
  
  }
  selectConnectorType(event){
 
    setTimeout(()=>{
      if(event.target.value === 'AWS_IAM_ROLE'){
      this.addConnector.get('iam_role_arn').setValidators([Validators.required]);
      this.addConnector.get('iam_role_arn').updateValueAndValidity();

      this.addConnector.get('external_id').setValidators([Validators.required]);
      this.addConnector.get('external_id').updateValueAndValidity();

      this.userClearvalidator();
      this.gcpClearValidator();

      
    }
    else if(event.target.value === 'GCP_SERVICE_ACCOUNT'){
      this.addConnector.get('project_id').setValidators([Validators.required]);
      this.addConnector.get('project_id').updateValueAndValidity();

      this.addConnector.get('private_key_id').setValidators([Validators.required]);
      this.addConnector.get('private_key_id').updateValueAndValidity();

      this.addConnector.get('private_key').setValidators([Validators.required]);
      this.addConnector.get('private_key').updateValueAndValidity();

      this.addConnector.get('client_id').setValidators([Validators.required]);
      this.addConnector.get('client_id').updateValueAndValidity();

      this.addConnector.get('client_x509_cert_url').setValidators([Validators.required]);
      this.addConnector.get('client_x509_cert_url').updateValueAndValidity();

      this.addConnector.get('client_email').setValidators([Validators.required]);
      this.addConnector.get('client_email').updateValueAndValidity();

      this.userClearvalidator()
      this.roleClearValidtor();
    }
    else{
      this.roleClearValidtor()
      this.gcpClearValidator();
    }
  },1000)


    if(event.target.value == 'AWS_IAM_USER'){
      this.show_credential_User = true
      this.show_credential_Role = false
      this.show_credential_GCP = false
    }

    if(event.target.value == 'AWS_IAM_ROLE'){
      this.show_credential_Role = true      
      this.show_credential_User = false      
      this.show_credential_GCP = false      
    }

    if(event.target.value == 'GCP_SERVICE_ACCOUNT'){
      this.show_credential_GCP = true
      this.show_credential_Role = false      
      this.show_credential_User = false
    }

    
  }
  selectCredType(event){
    console.log(event.target.value)
    if(event.target.value == 'FILE'){
      this.showfile = true
      this.showjson = false;
      this.addConnector.get('file').setValidators([Validators.required]);
      this.addConnector.get('file').updateValueAndValidity();

      this.addConnector.get('access_key_id').clearValidators();
      this.addConnector.get('access_key_id').updateValueAndValidity();

      this.addConnector.get('secret_access_key').clearValidators();
      this.addConnector.get('secret_access_key').updateValueAndValidity();

      this.addConnector.get('iam_user_arn').clearValidators();
      this.addConnector.get('iam_user_arn').updateValueAndValidity();

       //this.setconditionalValidators('USER','FILE');
    }
    else if(event.target.value == 'JSON'){
      this.showjson = true;
      this.showfile =  false;
      this.addConnector.get('access_key_id').setValidators([Validators.required]);
      this.addConnector.get('access_key_id').updateValueAndValidity();

      this.addConnector.get('secret_access_key').setValidators([Validators.required]);
      this.addConnector.get('secret_access_key').updateValueAndValidity();

      this.addConnector.get('iam_user_arn').setValidators([Validators.required]);
      this.addConnector.get('iam_user_arn').updateValueAndValidity();

      this.addConnector.get('file').clearValidators();
      this.addConnector.get('file').updateValueAndValidity();


    }
  }

  userClearvalidator(){
    this.addConnector.get('access_key_id').clearValidators();
    this.addConnector.get('access_key_id').updateValueAndValidity();

    this.addConnector.get('secret_access_key').clearValidators();
    this.addConnector.get('secret_access_key').updateValueAndValidity();

    this.addConnector.get('iam_user_arn').clearValidators();
    this.addConnector.get('iam_user_arn').updateValueAndValidity();

    this.addConnector.get('file').clearValidators();
    this.addConnector.get('file').updateValueAndValidity();
  }
  roleClearValidtor(){
    this.addConnector.get('iam_role_arn').clearValidators();
    this.addConnector.get('iam_role_arn').updateValueAndValidity();
    
    this.addConnector.get('external_id').clearValidators();
    this.addConnector.get('external_id').updateValueAndValidity();
  }
  gcpClearValidator(){
    this.addConnector.get('project_id').clearValidators();
    this.addConnector.get('project_id').updateValueAndValidity();

    this.addConnector.get('private_key_id').clearValidators();
    this.addConnector.get('private_key_id').updateValueAndValidity();

    this.addConnector.get('private_key').clearValidators();
    this.addConnector.get('private_key').updateValueAndValidity();

    this.addConnector.get('client_id').clearValidators();
    this.addConnector.get('client_id').updateValueAndValidity();

    this.addConnector.get('client_x509_cert_url').clearValidators();
    this.addConnector.get('client_x509_cert_url').updateValueAndValidity();

    this.addConnector.get('client_email').clearValidators();
    this.addConnector.get('client_email').updateValueAndValidity();
  }

  submit(addConnector){
    if (this.addConnector.invalid) {
       this.addConnector.controls['connector_name'].markAsTouched();
       this.addConnector.controls['description'].markAsTouched();
       this.addConnector.controls['connector_type'].markAsTouched();
       this.addConnector.controls['frequency_type'].markAsTouched();
       this.addConnector.controls['frequency_value'].markAsTouched();
       this.addConnector.controls['connector_association'].markAsTouched();
       this.addConnector.controls['assoc_value'].markAsTouched();
       this.addConnector.controls['access_key_id'].markAsTouched();
       this.addConnector.controls['secret_access_key'].markAsTouched();
       this.addConnector.controls['iam_role_arn'].markAsTouched();
       this.addConnector.controls['external_id'].markAsTouched();
       this.addConnector.controls['access_key_id'].markAsTouched();
       this.addConnector.controls['iam_user_arn'].markAsTouched();
       this.addConnector.controls['file'].markAsTouched();
       this.addConnector.controls['project_id'].markAsTouched();
       this.addConnector.controls['private_key_id'].markAsTouched();
       this.addConnector.controls['private_key'].markAsTouched();
       this.addConnector.controls['client_id'].markAsTouched();
       this.addConnector.controls['client_x509_cert_url'].markAsTouched();
       this.addConnector.controls['client_email'].markAsTouched();

    }
    else{
  	//console.log(addConnector.value)
     let params = {}

    if(this.addConnector.value.connector_type == 'AWS_IAM_USER'){
      if(this.addConnector.value.credential_detail_type =='FILE'){
        params = {
        "connector_name": this.addConnector.value.connector_name,
        "connector_description": this.addConnector.value.description,
        "connector_type": this.addConnector.value.connector_type,
        "credential_details": {
          "type": "FILE",
          "contents": this.globalFileUpload
        },
        "frequency_type": this.addConnector.value.frequency_type,
        "frequency_value": this.addConnector.value.frequency_value,
        "connector_association": [{
          "type": this.addConnector.value.assoc_value?'SERVICE':'PLATFORM',
          "value": this.addConnector.value.assoc_value?this.addConnector.value.assoc_value:this.addConnector.value.connector_association
          }]
        }
      }
      else{
        params = {
        "connector_name": this.addConnector.value.connector_name,
        "connector_description": this.addConnector.value.description,
        "connector_type": this.addConnector.value.connector_type,
        "frequency_type": this.addConnector.value.frequency_type,
        "frequency_value": this.addConnector.value.frequency_value,
        "connector_association": [{
          "type": this.addConnector.value.assoc_value?'SERVICE':'PLATFORM',
          "value": this.addConnector.value.assoc_value?this.addConnector.value.assoc_value:this.addConnector.value.connector_association
          }],
        "credential_details": {
          "type": "JSON",
          "contents": {
          "access_key_id": this.addConnector.value.access_key_id,
          "secret_access_key": this.addConnector.value.secret_access_key,
          "iam_user_arn": this.addConnector.value.iam_user_arn
          }
        }
        }
      }
      //console.log(params)
    }
    else if(this.addConnector.value.connector_type == 'AWS_IAM_ROLE'){
      params = {
        "connector_name": this.addConnector.value.connector_name,
        "connector_description": this.addConnector.value.description,
        "connector_type": this.addConnector.value.connector_type,
        "frequency_type": this.addConnector.value.frequency_type,
        "frequency_value": this.addConnector.value.frequency_value,
        "connector_association": [{
          "type": this.addConnector.value.assoc_value?'SERVICE':'PLATFORM',
          "value": this.addConnector.value.assoc_value?this.addConnector.value.assoc_value:this.addConnector.value.connector_association
          }],
          "credential_details": {
            "type": "JSON",
            "contents": {
              "iam_role_arn": this.addConnector.value.iam_role_arn,
              "external_id": this.addConnector.value.external_id
            }
          }
        }
       //console.log(params)
    }
    else if(this.addConnector.value.connector_type == 'GCP_SERVICE_ACCOUNT'){
      params = {
        "connector_name": this.addConnector.value.connector_name,
        "connector_description": this.addConnector.value.description,
        "connector_type": this.addConnector.value.connector_type,
        "frequency_type": this.addConnector.value.frequency_type,
        "frequency_value": this.addConnector.value.frequency_value,
        "connector_association": [{
          "type": this.addConnector.value.assoc_value?'SERVICE':'PLATFORM',
          "value": this.addConnector.value.assoc_value?this.addConnector.value.assoc_value:this.addConnector.value.connector_association
          }],
        "credential_details": {
          "type": "JSON",
          "contents": {
            "type": "service_account",
          "project_id": this.addConnector.value.project_id,
          "private_key_id": this.addConnector.value.private_key_id,
          "private_key": this.addConnector.value.private_key,
          "client_id": this.addConnector.value.client_id,
          "client_x509_cert_url": this.addConnector.value.client_x509_cert_url,
          "client_email": this.addConnector.value.client_email,
          }
        }

        }
        //console.log(params)
    }



    this.connectorservice.create(params).subscribe(res => {
    console.log(res)
    if(res.statusCode == 200){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Connector Updated!!',
          showConfirmButton: false,
          timer: 1500
        })
        this.activeModal.close();
        this.Aconnector.emit(res)
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.data.Errors,
          showConfirmButton: false,
          timer: 1500
        })
      }
    })

    }
  }

  onFileChange(event) {
    const file = event.target.files[0];
    let reader: FileReader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      let base64String = (reader.result as string).split(',').pop();
      this.globalFileUpload =  base64String;
    };
  
  }

  getplatform()
  {
    this.platformservice.summary1().subscribe(res=>{
       if(res.StatusCode == 200){
        this.platformList = res.Platforms;
      }
    })
  }

  get_platform_service(plat){
    let params = {
      'platform': plat
    }
    this.customerservice.getServiceListbyID(params).subscribe(res=>{
    this.serviceList = res.Services;
    });
  }

  


}
