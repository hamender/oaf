import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Input,Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PlatformServiceService } from '../../../shared/services/platform-service.service'
import {ResourceService } from '../../../shared/services/resource.service';
@Component({
  selector: 'app-edit-platform',
  templateUrl: './edit-platform.component.html',
  styleUrls: ['./edit-platform.component.scss']
})
export class EditPlatformComponent implements OnInit {

  @Input() platId: any;
  @Input() resId: any;
  @Input() index: any;
  @Input() viewId: any;
  @Input() viewresId: any;
  @ViewChild('platformId') platformId : ElementRef;
  @ViewChild('dropdown') dropdown : ElementRef;
  @ViewChild('field') field : ElementRef;
  @ViewChild('field1') field1 : ElementRef;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  @Output() delRes: EventEmitter<any> = new EventEmitter();
  
  userForm: FormGroup
  typeArr = [];
  dropArray = [];
  arr= [];
  passList: any;
  show: boolean = false;
  getAll:any[]=[];
  data: any;
  compArray= [];
  param= {}
  parms= {}
  itype: any;
  platname: any;
  delbtnDisable: boolean = true;

  constructor(public activeModal: NgbActiveModal,
          private fb: FormBuilder,
          private platformservice: PlatformServiceService,
          private resourceservice: ResourceService,
          private elem: ElementRef) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      platform: ['',[Validators.required]],
});

    if(this.platId && this.resId){
      this.getPlatform(this.platId,this.resId,this.index)
    }
    if(this.viewId){
      this.getPlatform(this.viewId,this.viewresId,this.index)
    }

     this.arr = [
    {'ftype':'field-e','type':'email', 'exacttype':'Email'},
    {'ftype':'field-s','type':'selector', 'exacttype':'Selector'},
    {'ftype':'field-sn','type':'text', 'exacttype':'Selector + Add New'},
    {'ftype':'field-d','type':'date', 'exacttype':'Date'},
    {'ftype':'field-dt','type':'datetime', 'exacttype':'Date and Time'},
    {'ftype':'field-n','type':'text', 'exacttype':'String-n'},
    {'ftype':'field-a','type':'text', 'exacttype':'String-a'},
    {'ftype':'field-m','type':'text', 'exacttype':'String-m'},
    {'ftype':'field-me','type':'text', 'exacttype':'Emails (Separated by comma)'},
    {'ftype':'field-ms','type':'text', 'exacttype':'Multi selector (Separated by comma)'},
    {'ftype':'field-mm','type':'text', 'exacttype':'Multi Mixed (Separated by comma)'},
    {'ftype':'field-x','type':'text', 'exacttype':'Mixed Encrypted  (hashed)'},
    {'ftype':'Support Email','type':'email', 'exacttype':'Support Email'},

    ]
  }

    updateForm(data){
    this.userForm.patchValue({platform: data})
    this.delbtnDisable = false;
    }

  getPlatform(value,resVal,index){
      let p= {
        'platform':value,
        'id': resVal
      }
      this.resourceservice.getResource(p).subscribe(res=>{
        console.log(res)
        
     if(res.statusCode == 200){
      this.updateForm(value)
      let headArray = []
      this.itype = res.Metadata[index];
      this.platname = value;
      console.log( this.itype )
      for(let i of Object.keys(res.Metadata)){
          if(i != 'services'){
            headArray.push(i)
          }
        }
        let valArray=[]
        for(let val of Object.values(res.Metadata)){
          if(!Array.isArray(val)){
            valArray.push(val)
          }

        }
        var result =  valArray.reduce(function(result, field, index) {
          result[headArray[index]] = field;
          return result;
        }, {})
        
        this.compArray = result;

          
        }
      })
  }
    gettype(a){

    for(let ar of this.arr){
      if(a === ar.ftype){
        return ar.type;
      }
    }
  }

  fieldChange(){
    
  }

  selectedDrop(){

  }
  closeform(){
    
    this.activeModal.close();
  }
/*  getIds($event){
    let platformId = this.platformId.nativeElement.value;
    let field = this.field.nativeElement.value;

  }*/
  submitForm(userForm){   
   // let updateArray= []
   
    let platformId = this.platformId.nativeElement.value;
    let field = this.field.nativeElement.value;
 
    let elements = this.elem.nativeElement.querySelectorAll('.option_input');
    
    elements.forEach(element => {
      
        this.param[element.name] = element.value
      
    });
    this.param['platform'] = platformId;
    console.log(this.param)
    this.resourceservice.update(this.param).subscribe(res => {
      console.log(res)
      if(res.StatusCode == 200){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Resource Updated!!',
          showConfirmButton: false,
          timer: 1500
        })
        this.activeModal.close();
      }
      this.passEntry.emit(res);
    })
    
  }
   
  delPlatformRes(idextype, plat){

   /* let params= {
      this.index: idextype,
      'platform':plat
    }*/
    this.parms[this.index] = idextype;
    this.parms['platform'] = plat;
    //console.log(this.parms)
    
    Swal.fire({
  title: 'Are you sure you want to delete?',
  text: "This action cannot be undone",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#707070',
  cancelButtonColor: '#0181AC',
  confirmButtonText: 'Delete'
}).then((result) => {
  if (result.value) {
    this.resourceservice.deleteResource(this.parms).subscribe(res=> {
      if(res.StatusCode == 200){  
      Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Resource Deleted!!',
          showConfirmButton: false,
          timer: 1500
        })
      }
      this.activeModal.close();
       this.delRes.emit(res);
    })
    
  }
})
  }

}
