import { Component, OnInit,AfterViewInit, TemplateRef, ViewChild,Input, ElementRef,Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PlatformServiceService } from '../../../shared/services/platform-service.service'


@Component({
  selector: 'app-plateform',
  templateUrl: './plateform.component.html',
  styleUrls: ['./plateform.component.scss']
})
export class PlateformComponent implements OnInit {
  @Input() platId: any;
  @Input() viewId: any;
  @ViewChild('platformId') platformId : ElementRef;
  @ViewChild('field') field : ElementRef;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  @Output() dplatEntry: EventEmitter<any> = new EventEmitter();
  dis: boolean = true;
  show_new_fields: boolean = false;
  showerror: boolean = false;
  delbtnDisable: boolean = true;
  PlatForm: FormGroup;
  arr= [];
  compArray= [];
  valArray = [];
  nARray = [];
  nARray1 = [];
  oldLength:number;
  parm={}
  pname: any;


  constructor(public activeModal: NgbActiveModal,
          private fb: FormBuilder,
          private platformservice: PlatformServiceService,
          private elem: ElementRef) { }

  ngOnInit() {

    this.PlatForm = this.fb.group({
      platform: ['',[Validators.required]],
      services: this.fb.array([]),
    });

    if(this.platId){
      this.getPlatform(this.platId)
    }
    if(this.viewId){
      this.getPlatform(this.viewId)
    }

    this.arr = [
    {'ftype':'field-e','type':'email', 'exacttype':'Email'},
    {'ftype':'field-s','type':'selector', 'exacttype':'Selector'},
    {'ftype':'field-sn','type':'text', 'exacttype':'Selector + Add New'},
    {'ftype':'field-d','type':'date', 'exacttype':'Date'},
    {'ftype':'field-dt','type':'datetime', 'exacttype':'Date and Time'},
    {'ftype':'field-n','type':'text', 'exacttype':'String-n'},
    {'ftype':'field-a','type':'text', 'exacttype':'String-a'},
    {'ftype':'field-m','type':'text', 'exacttype':'String-m'},
    {'ftype':'field-me','type':'text', 'exacttype':'Emails (Separated by comma)'},
    {'ftype':'field-ms','type':'text', 'exacttype':'Multi selector (Separated by comma)'},
    {'ftype':'field-mm','type':'text', 'exacttype':'Multi Mixed (Separated by comma)'},
    {'ftype':'field-x','type':'text', 'exacttype':'Mixed Encrypted  (hashed)'},
    {'ftype':'Support Email','type':'email', 'exacttype':'Support Email'},
    ]


  }

  submitForm(PlatForm){
      if (this.addressGroup.invalid) {

        this.showerror = true;

        }else{
          this.showerror = false;
     let Arr1= []
     let Arr2= []
     let Arr3= []
     let Arr4= []
     let combine_new_field_array = []
     let combine_old_field_array = []

    for(let arr of PlatForm.value.services){
      if(arr.serviceFields == 'field-s' || arr.serviceFields == 'field-ms' || arr.serviceFields == 'field-sn'){
         Arr3.push({'name':arr.ServiceFieldValue,'values':arr.ServiceFieldValue1})
      }
      else{
        Arr3.push({'name':arr.ServiceFieldValue})
      }
      Arr4.push({'type':arr.serviceFields,})
    }
    
    combine_new_field_array = Arr4.map((item, i) => Object.assign({}, item, Arr3[i]))
    console.log(combine_new_field_array)



    let platformId = this.platformId.nativeElement.value;

    this.parm['platform']=platformId;

    let elements = this.elem.nativeElement.querySelectorAll('.option_input1');
   /* elements.forEach(element => {
      Arr1.push({'type':element.name})
    });*/

    //let elements1 = this.elem.nativeElement.querySelectorAll('.option_input1');
   
    elements.forEach(element => {
      console.log(element.id)
      if(element.name === 'field-s'){
      let e1 = this.elem.nativeElement.querySelectorAll('.'+element.id);
        e1.forEach(ele1 => {

           Arr2.push({'type': element.name,'name':element.value,'values':ele1.value})
        });
      }
      if( element.name === 'field-ms'){
      let e2 = this.elem.nativeElement.querySelectorAll('.'+element.id);
        e2.forEach(ele2 => {
           Arr2.push({'type': element.name,'name':element.value,'values':ele2.value})
        });
      }
      if( element.name === 'field-sn'){
      let e3 = this.elem.nativeElement.querySelectorAll('.'+element.id);
        e3.forEach(ele3 => {
           Arr2.push({'type': element.name,'name':element.value,'values':ele3.value})
        });
      }
      if( element.name != 'field-sn' && element.name != 'field-ms' && element.name != 'field-s'){
        Arr2.push({'type': element.name,'name':element.value})
      }
    });
   //combine_old_field_array =  Arr1.map((item, i) => Object.assign({}, item, Arr2[i]))
   console.log(Arr2)
   this.parm['fields'] = Arr2.concat(combine_new_field_array)
   console.log(this.parm)
   this.platformservice.update(this.parm).subscribe(res=>{

    if(res.statusCode == 200){
      Swal.fire({
          position: 'center',
          icon: 'success',
          title: res.Message,
          showConfirmButton: false,
          timer: 1500
        })
       this.activeModal.close();
       this.passEntry.emit(res);
    }
   })
 }
  }

  closeform(){
    this.activeModal.close()
  }

  get addressGroup(): FormArray {
    return this.PlatForm.get('services') as FormArray;
  }
 
   typeChange(event,i){
    let val = i;
     if(this.nARray.includes(val)){
        if(event.target.value != 'field-s' || event.target.value == 'field-ms'|| event.target.value == 'field-sn'){
          const index: number = this.nARray.indexOf(val);
            if (index !== -1) {
                this.nARray.splice(index, 1);
            }
        } 
     }
     else{
      if(event.target.value == 'field-s' || event.target.value == 'field-ms'|| event.target.value == 'field-sn'){
      this.nARray.push(val)
      console.log(this.nARray)
      }
     }
  }

  updateForm(data){
    this.PlatForm.patchValue({platform: data})
    this.pname = data;
    this.delbtnDisable = false;
  }
  getPlatform(value){
      let p= {
        'platform':value
      }
      this.platformservice.getPlatformbyID(p).subscribe(res=>{
        console.log(res)
     if(res.statusCode == 200){
      this.updateForm(value);
      for(let item of res.Metadata){
      this.compArray= [];  
        this.oldLength = item.Fields.length;

        for(let i of item.Fields){
          this.valArray= [];
          //console.log(i)
          if(i.values != null){
            let str = i.values.replace(/,\s*$/, "");
            this.valArray = str.split(",");
          }
          this.compArray.push({'type':i.type,'name':i.name,'val':this.valArray})
        }

        }
        console.log(this.oldLength)
        this.show_new_fields = true;

        
        }
      })
  }
    gettype(a){
    for(let ar of this.arr){
      if(a === ar.ftype){
        return ar.type;
      }
    }
  }

  addNewAddressGroup() {

    const add = this.PlatForm.get('services') as FormArray;
    add.push(this.fb.group({
      serviceFields: ['',[Validators.required]],
      ServiceFieldValue: ['',[Validators.required]],
      ServiceFieldValue1: [''],
    }))
  }

  deleteAddressGroup(index: number) {
    const add = this.PlatForm.get('services') as FormArray;
    add.removeAt(index)
    if(this.nARray.includes(index)){
        
          const index1: number = this.nARray.indexOf(index);
            if (index1 !== -1) {
                this.nARray.splice(index1, 1);
            }
        
     }
  }

  delPlatformRes(platname){
    console.log(platname)
    let params= {
      'platform': platname
    }
    Swal.fire({
  title: 'Are you sure you want to delete this?',
  text: "This action cannot be undone",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#707070',
  cancelButtonColor: '#4f9a0b',
  confirmButtonText: 'Delete'
}).then((result) => {
  if (result.value) {
    this.platformservice.deletePlatform(params).subscribe(res=> {
      if(res.statusCode == 200){  
      Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Platform Deleted!!',
          showConfirmButton: false,
          timer: 1500
        })

      this.dplatEntry.emit(res)
       this.activeModal.close();
      /*setTimeout(()=>{
        this.tableplat = false;
      this.getPlatformList();
      },1500);*/
      
      }
    })
    
  }
})
  }


}
