import { Component, OnInit, AfterViewInit, ElementRef, ViewChild,Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder,  FormArray, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PlatformServiceService } from '../../../shared/services/platform-service.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-add-platform',
  templateUrl: './add-platform.component.html',
  styleUrls: ['./add-platform.component.scss']
})
export class AddPlatformComponent implements OnInit {
@ViewChild('addserviceClick') addserviceClick: ElementRef;
@Output() passEntry: EventEmitter<any> = new EventEmitter();
platForm: FormGroup;
showField:any;
slcVal: string= '';
nARray=[];
loader:boolean = false
showerror:boolean = false

  constructor(public activeModal: NgbActiveModal,
          private fb: FormBuilder,
          private platformservice: PlatformServiceService
        ) { }

  ngOnInit(): void {
    this.platForm = this.fb.group({
      platformName: ['' ,[Validators.required]],
      serviceIndex: ['' ,[Validators.required]],
      ServiceIndexValue: ['' ,[Validators.required]],
      services: this.fb.array([]),
    });
    setTimeout(() => {
    this.addserviceClick.nativeElement.click();
    }, 200);
    this.addNewAddressGroup();
  }
 closeform(){
      this.activeModal.close();
  }

  typeChange(event,i){
    let val = i;
     if(this.nARray.includes(val)){
        if(event.target.value != 'field-s' || event.target.value == 'field-ms'|| event.target.value == 'field-sn'){
          const index: number = this.nARray.indexOf(val);
            if (index !== -1) {
                this.nARray.splice(index, 1);
            }
        } 
     }
     else{
      if(event.target.value == 'field-s' || event.target.value == 'field-ms'|| event.target.value == 'field-sn'){
      this.nARray.push(val)
      console.log(this.nARray)
      }
     }
  }

submitplatForm(platForm){
   if (this.platForm.invalid || this.addressGroup.invalid) {
    this.showerror = true;
    }
    else{
      this.showerror = false;
      this.loader = true;
      let newArray=[];
    for(let arr of platForm.value.services){
      let newObj={};
      newObj['type']=arr.serviceFields;
      newObj['name']=arr.ServiceFieldValue;
      if(arr.serviceFields == 'field-s'){ 
      newObj['values']=arr.ServiceFieldValue1;
      }
      if(arr.serviceFields == 'field-ms'){ 
      newObj['values']=arr.ServiceFieldValue1;
      }
      if(arr.serviceFields == 'field-sn'){ 
      newObj['values']=arr.ServiceFieldValue1;
      }
      newArray.push(newObj)
    }
    let params = {
      'index': platForm.value.ServiceIndexValue,
      'platform': platForm.value.platformName,
      'fields': newArray
    }
    console.log(params)
   this.platformservice.create(params).subscribe(res => {
      if(res.statusCode == 200){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Platform Created.',
          showConfirmButton: false,
          timer: 1500
        })
        this.activeModal.close();
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'warning',
          title: res.Message,
          showConfirmButton: false,
          timer: 1500
        })
      }
      this.loader = false
      this.passEntry.emit(res);
    })
  }
  }
 
addNewAddressGroup() {
    const add = this.platForm.get('services') as FormArray;
    add.push(this.fb.group({
      serviceFields: ['',[Validators.required]],
      ServiceFieldValue: ['',[Validators.required]],
      ServiceFieldValue1: [''],
    }))
  }
  deleteAddressGroup(index: number) {
    const add = this.platForm.get('services') as FormArray;
    add.removeAt(index)
    if(this.nARray.includes(index)){
        
          const index1: number = this.nARray.indexOf(index);
            if (index1 !== -1) {
                this.nARray.splice(index1, 1);
            }
        
     }
  }

  get forgetF() {
        return this.platForm.controls;
  }
  get addressGroup(): FormArray {
    return this.platForm.get('services') as FormArray;
  }

}
