import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from "@angular/router";
import Swal from 'sweetalert2';
import { CustomValidators } from '../../shared/validator/custom-validators';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  href = 'https://app.oaf-x.com';
  closeResult: string;
  loginForm: FormGroup;
  codeForm: FormGroup;
  forgotForm: FormGroup;
  resetPass: FormGroup;
  resetUserPass: FormGroup;
  forgotUser: FormGroup;
  fcontrol: boolean = false;
  step:number = 1;
  show:boolean = false;
  code:boolean = false;
  PasswordMatch: boolean ;
  PasswordMatch1: boolean ;
  usrname : any;
  phone: any;
  uemail: any;
  sess: any;
  mfaError:boolean = false;
  imagePath = '../../../assets/images/myra-logo.png';
  legalImgPath = '../../../assets/images/blue-logo.png';
  gifpath = '../../../assets/images/marketing-gif.gif';

  constructor(private authservice: AuthenticationService,
    private fb: FormBuilder,
    private router: Router,
    private modalService: NgbModal) {
     
    if (this.authservice.isLoggedIn()) {
      this.router.navigate(["dashboard"]);
    }

    }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      username: ['',[Validators.required]],
      password: ['',[Validators.required]],
      
    })
    this.forgotUser = this.fb.group({
      uemail: ['',[Validators.required]],
      pnumber: ['',[Validators.required]],
      
    })
    this.codeForm = this.fb.group({
      mfcode: ['',[Validators.required]]
    })
    this.forgotForm = this.fb.group({
      uname: ['',[Validators.required]]
    })
    this.resetPass = this.fb.group({
      newpass: ['',[Validators.required,Validators.minLength(14),CustomValidators.strong]],
      cnfnewpass:['',[Validators.required]],

    })
    this.resetUserPass = this.fb.group({
      newpass: ['',[Validators.required,Validators.minLength(14),CustomValidators.strong]],
      cnfnewpass:['',[Validators.required]],
      code: ['',[Validators.required]],

    })
    if(localStorage.getItem('Attribute')){
    let userAttr = JSON.parse(localStorage.getItem('Attribute'));
    this.phone = userAttr.phone_number;
    this.uemail = userAttr.email; 
    }
    if(localStorage.getItem('Username')){
    let uname = localStorage.getItem('Username');
    this.usrname = uname;
    }
    if(localStorage.getItem('Session')){  
    let session = localStorage.getItem('Session');
    this.sess = session;
    }
    
  }

     get logF() {
        return this.loginForm.controls;
      }

     get forgetF() {
        return this.forgotForm.controls;
      }
    
     get resetF() {
        return this.resetUserPass.controls;
      }
      get forgetU() {
        return this.forgotUser.controls;
      }
     
     get PassF() {
        return this.resetPass.controls;
      }

     get oneF() {
        return this.resetPass.controls;
      }
      

  submitLogin(loginForm){
    //console.log(this.loginForm.valid)
    if (this.loginForm.invalid) {
        }else{
    const uname = this.loginForm.get("username").value;
    const pass = this.loginForm.get("password").value;
    let params = {
      'username': uname,
      'password': pass
      }
    
    this.authservice.signin(params).subscribe(res => {
      console.log(res)
      if(res.statusCode == 200){
        if(res.data.hasOwnProperty('ChallengeName')){
          if(res.data.ChallengeName == 'NEW_PASSWORD_REQUIRED'){
            this.step = 4;
            localStorage.setItem(
                "Session", res.data.Session
              );
      localStorage.setItem(
                "Attribute", res.data.ChallengeParameters.userAttributes
              );
      localStorage.setItem(
                "Username", res.data.ChallengeParameters.USER_ID_FOR_SRP
              );
          }
          if(res.data.ChallengeName == 'SMS_MFA'){
            console.log(res.data.Session)
            this.step = 5;
             localStorage.setItem(
                "Session", res.data.Session
              );
             this.sess = res.data.Session;

          }
        }
        else{
          console.log(res.data)
        localStorage.setItem("Token",JSON.stringify(res.data ));
        const time_to_login = Date.now() + res.data.ExpiresIn*1000;
        
        localStorage.setItem('timer', JSON.stringify(time_to_login));
        localStorage.setItem("Attribute",JSON.stringify(res.data.UserAttributes ));
        this.router.navigate(["dashboard"]);

        }
      
     
      }
      else{
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.message,
          showConfirmButton: false,
          timer: 1500
        })
       
      }
      
    });
  }
  }
  submitCode(codeForm){
    if (this.codeForm.invalid) {
        }else{
    const uname = this.loginForm.get("username").value;
    console.log(this.sess)
    if(localStorage.getItem('Session')){  
    let session = localStorage.getItem('Session');
    console.log(session)
    this.sess = session;
    }
    let params = {
      'username': uname,
      'mfa_code': this.codeForm.value.mfcode,
      'session': this.sess
      }
      this.authservice.mfa(params).subscribe(res => {
      console.log(res)
      if(res.statusCode == 200){
        localStorage.setItem("Token",JSON.stringify(res.data ));
        const time_to_login = Date.now() + res.data.ExpiresIn*1000;
        localStorage.setItem('timer', JSON.stringify(time_to_login));
        localStorage.setItem("Attribute",JSON.stringify(res.data.UserAttributes ));
        this.router.navigate(["dashboard"]);
      }
    })
  }
}
  SubmitPass(resetPass){
    if (this.resetPass.invalid) {
        }else{
          console.log(resetPass)
          if(localStorage.getItem('Username')){
          let uname = localStorage.getItem('Username');
          this.usrname = uname;
          console.log(this.usrname)
          }
           if(localStorage.getItem('Session')){  
          let session = localStorage.getItem('Session');
          this.sess = session;
          }
          if(this.PasswordMatch == true){
            let npass=  this.resetPass.value.newpass;
              let params = {
              'username': this.usrname,
              'password': npass,
              'session': this.sess
              }
              this.authservice.resetPassword(params).subscribe(res => {
                if(res.statusCode== 200){
                  if(res.data.ChallengeName == 'SMS_MFA'){
                    this.step = 5;
                     localStorage.setItem(
                        "Session", res.data.Session
                      );
                  }
                  else{  
                  console.log(res)
                  //this.step=1
                  const time_to_login = Date.now() + res.data.ExpiresIn*1000;
                  console.log(time_to_login)
                  localStorage.setItem('timer', JSON.stringify(time_to_login));
                  localStorage.setItem("Token",JSON.stringify(res.data ));
                  this.router.navigate(["dashboard"]);
                  }
                }
              })
              
          }
  
  }
}


  ResetUserPass(resetUserPass){

    if (this.resetUserPass.invalid) {
        }else{
     if(this.PasswordMatch1 == true){
      let nuser=  this.usrname;
        let params = {
        'username': nuser,
        'password': this.resetUserPass.value.newpass,
        'mfa_code': this.resetUserPass.value.code,
        }

        this.authservice.resetUserPassword(params).subscribe(res => {

          if(res.message == "Invalid Verification code"){
           this.mfaError = true;
          }
   else{
           this.mfaError = false;

        if(res.statusCode ==200){
          Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Password Successfully Changed.',
          showConfirmButton: false,
          timer: 1500
        })
          this.step=1
        }}
        })
      }
     }
  }
  CheckPassword(event) {
    console.log('d')
    let confirmPass = event.target.value;
    let passWrd = this.resetPass.value.newpass;

    if (passWrd == confirmPass) {
      this.PasswordMatch = true;
    } else {
      this.PasswordMatch = false;
    }
  }
  CheckPassword1(event) {
console.log('dd')
    let confirmPass = event.target.value;
    let passWrd = this.resetUserPass.value.newpass;

    if (passWrd == confirmPass) {
      this.PasswordMatch1 = true;
    } else {
      this.PasswordMatch1 = false;
    }
  }

  forgotPassword(){
    this.step = 2;
  }
  logIn(){
    this.step = 1;
  }

  forgotuser(){
    this.step = 6;
    this.loginForm.reset();

  }
  submitForgotUser(forgotUser){
    let u_email = this.forgotUser.value.uemail;
    let p_number = this.forgotUser.value.pnumber.toString();;
    let p ={
      'email':u_email,
      'phone_number':p_number
    }
    this.authservice.forgottonusername(p).subscribe(res=>{
      console.log(res)
      if(res.statusCode == 200){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Username sent on email/phone number',
          showConfirmButton: false,
          timer: 1500
        })
         this.step = 1;
         this.forgotUser.reset()
      }
      else{
         Swal.fire({
          position: 'center',
          icon: 'error',
          title: res.data.Error,
          showConfirmButton: false,
          timer: 1500
        })
      }
    })
  }
  submitForgot(forgotForm){
    console.log(this.forgotForm.valid)
     if (this.forgotForm.invalid) {
        }
        else{
    const uname = this.forgotForm.get("uname").value;
    let params = {
      'username': uname
    }
    this.authservice.forgotPassword(params).subscribe(res => {

      if(res.statusCode==200){
        this.step = 3;
        this.fcontrol= false;
        this.usrname = uname;
      }
    });
  }
}


showForm(content){
    if(this.authservice.isLoggedIn()){
      console.log(this.authservice.isLoggedIn())
    }
    else{
      console.log(this.authservice.isLoggedIn())
      this.open(content);
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
